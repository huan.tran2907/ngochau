﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models
{
    public class Company
    {
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Url { get; set; }
        public string? Phone1 { get; set; }
        public string? Phone2 { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
        public string? Description { get; set; }
        public string? Content { get; set; }
        public int? Status { get; set; }
        public string? SeoTitle { get; set; }
        public string? SeoKeywords { get; set; }
        public string? SeoDescription { set; get; }
        public int? Type { get; set; }
        public string? LogoImage { get; set; }
        public string? IconImage { get; set; }
        public int? Order { get; set; }
        public string? Website { get; set; }
        public string? TimeWork { get; set; }
        public string? FileRobots { get; set; }
        public string? FileSiteMap { get; set; }
        public string? CopyRightFooter { get; set; }
    }
}
