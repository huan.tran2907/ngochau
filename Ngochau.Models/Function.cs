﻿using Ngochau.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models
{
    public class Function
    {
       
        [Key]
        [StringLength(128)]
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Code { set; get; }

        [Required]
        [StringLength(128)]
        public string Name { set; get; }

        [Required]
        [StringLength(250)]
        public string URL { set; get; }


        [StringLength(128)]
        public string? ParentId { set; get; }

        public string IconCss { get; set; }
        public int SortOrder { set; get; }
        public int Status { set; get; }
    }
}
