﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Ngochau.Models.NgoChauModel
{
    public class PI
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? POId { get; set; }
        [Required]
        [ForeignKey("CustomerId")]
        public Guid CustomerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? Desc { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? PIDate { get; set; }
        public virtual Customer Customer { set; get; }
        public virtual ICollection<PIDetail> PIDetails { set; get; }
    }
    public class PIDetail
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [ForeignKey("PIId")]
        public Guid? PIId { get; set; }
        [ForeignKey("ProductId")]
        public Guid? ProductId { get; set; }
        [ForeignKey("ItemId")]
        public Guid? ItemId { get; set; }
        [ForeignKey("ColorId")]
        public Guid? ColorId { get; set; }
        public decimal? CBM { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public int? QuantityExport { get; set; }
        public decimal? Money { get; set; }
        public decimal? Deposit { get; set; }
        public string? Desc { get; set; }
        public string? DescCodeColor { get; set; }
        public string? DescColorFails { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual PI PI { set; get; }
        public virtual Product Product { set; get; }
        public virtual Item Item { set; get; }
        public virtual Color Color { set; get; }
    }
}
