﻿using Ngochau.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models.NgoChauModel
{
    public class Plan
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [ForeignKey("CustomerId")]
        public Guid CustomerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? Desc { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual Customer Customer { set; get; }
        public virtual ICollection<PlanDetail> PlanDetails { set; get; }
    }
    public class PlanDetail
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("PlanId")]
        public Guid? PlanId { get; set; }

        [ForeignKey("POId")]
        public Guid? POId { get; set; }
        //public string? POCode { get; set; }

        [ForeignKey("ProductId")]
        public Guid? ProductId { get; set; }

        [ForeignKey("ItemId")]
        public Guid? ItemId { get; set; }
        [ForeignKey("ColorId")]
        public Guid? ColorId { get; set; }
        public decimal? CBM { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public int? QuantityExport { get; set; }
        public decimal? Money { get; set; }
        public decimal? Deposit { get; set; }
        public string? Desc { get; set; }
        public string? DescCodeColor { get; set; }
        public string? DescColorFails { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual Plan Plan { set; get; }
        public virtual PO PO { set; get; }
        public virtual Product Product { set; get; }
        public virtual Item Item { set; get; }
        public virtual Color Color { set; get; }
    }
}
