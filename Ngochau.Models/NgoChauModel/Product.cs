﻿using Ngochau.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models.NgoChauModel
{
    public class Product
    {
        [Key]
        public Guid Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public string? Image { get; set; }
        public string? Desc { get; set; }
        public DateTime? DateCreated { set; get; }
    }
}
