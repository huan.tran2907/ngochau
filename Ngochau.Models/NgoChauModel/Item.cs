﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ngochau.Models;

namespace Ngochau.Models.NgoChauModel
{
    public class Item
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("ProductId")]
        public Guid ProductId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? CBM { get; set; }
        public string? MPN { get; set; }
        public string? CodeY { get; set; }
        public string? Desc { get; set; }
        public decimal? PriceNew { get; set; }
        public decimal? PriceOld { get; set; }
        public DateTime? CreateDate { get; set; }
        //public string? BoxVol { get; set; }
        public string? BoxDimension { get; set; }
        public string? BoxWeight { get; set; }
        public virtual Product Product { set; get; }
    }
}
