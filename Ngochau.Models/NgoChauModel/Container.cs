﻿using Ngochau.Models.NgoChauModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models.NgoChauModel
{
    public class Container
    {
        [Key]
        public Guid Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public int? IsActive { get; set; }
        public virtual ICollection<CIPL> CIPLs { set; get; }
    }
}
