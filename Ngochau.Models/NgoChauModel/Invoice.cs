﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ngochau.Models.NgoChauModel
{
    public class Invoice
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [ForeignKey("CustomerId")]
        public Guid? CustomerId { get; set; }

        [ForeignKey("ContainerId")]
        public Guid? ContainerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? Desc { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public virtual Customer Customer { set; get; }
        public virtual Container Container { set; get; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { set; get; }
    }
    public class InvoiceDetail
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("InvoiceId")]
        public Guid? InvoiceId { get; set; }
        [ForeignKey("CIPLId")]
        public Guid? CIPLId { get; set; }

        [ForeignKey("ProductId")]
        public Guid? ProductId { get; set; }

        [ForeignKey("ItemId")]
        public Guid? ItemId { get; set; }
        [ForeignKey("ColorId")]
        public Guid? ColorId { get; set; }
        public decimal? CBM { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public int? QuantityExport { get; set; }
        public decimal? Money { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? RemainDeposit { get; set; }
        public string? Desc { get; set; }
        public string? DescCodeColor { get; set; }
        public string? DescColorFails { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual Invoice Invoice { set; get; }
        public virtual Product Product { set; get; }
        public virtual Item Item { set; get; }
        public virtual Color Color { set; get; }
        public virtual CIPL CIPL { set; get; }
    }
}
