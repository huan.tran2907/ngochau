﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Models
{
    public class Payment
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey("CustomerId")]
        public Guid? CustomerId { get; set; }
        public Guid? ContainerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public string? Desc { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }        
        public int? TotalQuantity { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual Customer Customer { set; get; }
        public virtual ICollection<PaymentDetail> PaymentDetails { set; get; }
    }

    public class PaymentDetail
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey("PaymentId")]
        public Guid? PaymentId { get; set; }
        public Guid? MasterOrderId { get; set; }
        public Guid? MasterOrderDetailId { get; set; }
        public Guid? ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? SKU { get; set; }
        public Guid? ItemId { get; set; }
        public string? ItemCode { get; set; }
        public string? ItemName { get; set; }
        public Guid? ColorId { get; set; }
        public string? ColorCode { get; set; }
        public string? ColorName { get; set; }
        public string? Dimension { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? CBM { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? Quantity { get; set; }
        public decimal? BoxWeight { get; set; } = 0;
        public string? ExtraCharge { get; set; }
        public string? BatchVolume { get; set; }
        public string? Finish { get; set; }
        public decimal UnitPrice { get; set; }

        public virtual Payment Payment { set; get; }
    }
}
