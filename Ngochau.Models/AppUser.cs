﻿

using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ngochau.Models
{
    public class AppUser : IdentityUser<Guid>
    {
        public string? Discriminator { get; set; }

        public string? FullName { get; set; }

        public DateTime? BirthDay { set; get; }

        public string? Address { set; get; }

        public decimal? Balance { get; set; }

        public string? Avatar { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? Status { get; set; }
    }
}
