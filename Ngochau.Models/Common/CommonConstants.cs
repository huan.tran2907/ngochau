﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models.Common
{
    public class CommonConstants
    {
        public const string DefaultFooterId = "DefaultFooterId";



        public const string ProductTag = "Product";
        public const string BlogTag = "Blog";
        public class AppRole
        {
            public const string AdminRole = "Admin";
        }
        public class UserClaims
        {
            public const string Roles = "Roles";
        }
    }
}
