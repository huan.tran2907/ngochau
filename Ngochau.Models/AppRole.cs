﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.Models
{
    public class AppRole : IdentityRole<Guid>
    {
        [StringLength(250)]
        public string? Code { get; set; }
        [StringLength(250)]
        public string? Description { get; set; }
    }
}
