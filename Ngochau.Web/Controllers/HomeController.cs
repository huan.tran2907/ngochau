﻿
using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Ngochau.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
       
        private readonly ICompanySerivce _companySerivce;
        public HomeController(ILogger<HomeController> logger, ICompanySerivce companySerivce)
        {
            _logger = logger;
            _companySerivce = companySerivce;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login", "Authen", new { area = "Cpanel" });
        }
    }
}