﻿using Ngochau.Web.Areas.Admin.Service.IService;
using Ngochau.Web.Model;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using IProductService = Ngochau.Web.Areas.Admin.Service.IService.IProductService;

namespace Ngochau.Web.Controllers
{
    public class ViewDefaultController : Controller
    {

        IProductService _productService;
        IConfiguration _configuration;
        //private ISystemService _systemService;
        public ViewDefaultController(IProductService productService, IConfiguration configuration)
        {
            _productService = productService;
            _configuration = configuration;
        }

        [Route("{alias}.html")]
        public IActionResult ViewDefault(string alias, int? pageSize, string shortBy, int page = 1)
        {
            ViewDefaultModel model = new ViewDefaultModel();
                model.pageSize = pageSize == null ? _configuration.GetValue<int>("PageSize") : (int)pageSize;
                model.SortBy = shortBy;
                return View(model);
        }

    }
}
