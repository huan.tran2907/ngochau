﻿using Ngochau.Web.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class CpanelController : BaseController
    {
        public CpanelController(IAuthorizationService authorizationService) : base(authorizationService)
        {

        }
        public IActionResult Index()
        {
            var resultDashBoard = this.CheckPermission("Dashboards", Operations.Read);
            if (!string.IsNullOrEmpty(resultDashBoard.Result))
                return new RedirectResult(resultDashBoard.Result);
            return View();
        }


    }
}
