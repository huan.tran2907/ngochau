﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class ReportController : BaseController
    {
        public ReportController(IAuthorizationService authorizationService) : base(authorizationService)
        {

        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
