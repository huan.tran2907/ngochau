﻿using Ngochau.Models;
using Ngochau.Web.Authorization;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService, IAuthorizationService authorizationService) : base(authorizationService)
        {
            _roleService = roleService;
        }
        public IActionResult Role()
        {
            var resSettigRoleRead = this.CheckPermission("SettingRole", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigRoleRead))
            {
                return new RedirectResult(resSettigRoleRead);
            }
            return View();
        }
        public IActionResult Roles()
        {
            var resSettigRoleRead = this.CheckPermission("SettingRole", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigRoleRead))
            {
                return new RedirectResult(resSettigRoleRead);
            }
            return View();
        }

        #region API ROLE

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var model = _roleService.GetAllPagingAsync(keyword, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _roleService.GetAllAsync();

            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            var model = await _roleService.GetById(id);

            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(AppRole roleVm)
        {
            try
            {
                if (roleVm.Id == Guid.Empty)
                {
                    var resSettigRoleCreate = this.CheckPermission("SettingRole", Operations.Create).Result;
                    if (!string.IsNullOrEmpty(resSettigRoleCreate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigRoleCreate });
                    }
                    else
                    {
                        await _roleService.AddAsync(roleVm);
                        return new OkObjectResult(new { StatusCode = 200, Message = "Thêm mới vai trò " + roleVm.Code + " thành công", Data = roleVm.Id });
                    }
                }
                else
                {
                    var resSettigRoleUpdate = this.CheckPermission("SettingRole", Operations.Update).Result;
                    if (!string.IsNullOrEmpty(resSettigRoleUpdate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigRoleUpdate });
                    }
                    else
                    {
                        await _roleService.UpdateAsync(roleVm);
                        return new OkObjectResult(new { StatusCode = 200, Message = "Thêm mới vai trò " + roleVm.Code + " thành công", Data = roleVm.Id });
                    }
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }

        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var resSettigRoleDelete = this.CheckPermission("SettingRole", Operations.Delete).Result;
            if (!string.IsNullOrEmpty(resSettigRoleDelete))
            {
                return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigRoleDelete });
            }
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            await _roleService.DeleteAsync(id);
            return new OkObjectResult(new { StatusCode = 200, Message = "Xóa vai trò thành công", Data = id });
        }


        [HttpPost]
        public IActionResult ListAllFunction(Guid roleId)
        {
            var functions = _roleService.GetListFunctionWithRole(roleId);
            return new OkObjectResult(functions);
        }

        [HttpPost]
        public IActionResult SavePermission(List<Permission> listPermmission, Guid roleId)
        {
            _roleService.SavePermission(listPermmission, roleId);
            return new OkObjectResult(new { StatusCode = 200, Message = "Cập nhật phân quyền cho chức năng thành công", Data = roleId });
        }
        #endregion
    }
}
