﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class CIPLController : BaseController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        ICIPLService _iCIPLService;

        public CIPLController(IWebHostEnvironment hostingEnvironment, IAuthorizationService authorizationService, ICIPLService iCIPLService) : base(authorizationService)
        {
            _hostingEnvironment = hostingEnvironment;
            _iCIPLService = iCIPLService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CIPL()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetPlanForCIPL(Guid planId)
        {
            var model = await _iCIPLService.GetPlanForCIPL(planId);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            CIPLResultModel.CIPLGetByIdResultModel model = await _iCIPLService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _iCIPLService.GetAll().Select(p => new CIPL() { Id = p.Id, Name = p.Code + " - " + p.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            return new OkObjectResult(_iCIPLService.GetAllPaging(keyword, status, page, pageSize));
        }

        [HttpPost]
        public async Task<IActionResult> SaveCIPL(CIPLParamModel.CIPLInsertParamModel model)
        {
            try
            {
                if (model.CIPLId == Guid.Empty || model.CIPLId == null)
                {
                    var result = await _iCIPLService.InsertCIPL(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "CIPL đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "CIPL đã thêm thành công vào hệ thống", Data = result.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var result = await _iCIPLService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Kế hoạch " + model.Name + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Kế hoạch " + model.Name + " đã cập nhật thành công", Data = model.CIPLId });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteCIPL(Guid ciplId)
        {
            CreateUpdateDeleteResultModel<Guid> result = await _iCIPLService.DeleteCIPL(ciplId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        public async Task<IActionResult> ExportCIPL(Guid ciplId)
        {
            var dataFile = await _iCIPLService.ExportCIPL(ciplId);
            return File(dataFile.Bytes, dataFile.ContentType, dataFile.Name);
        }
    }
}
