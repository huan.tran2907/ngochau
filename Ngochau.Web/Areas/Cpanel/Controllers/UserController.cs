﻿using Ngochau.Models;
using Ngochau.Web.Authorization;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService, IAuthorizationService authorizationService) : base(authorizationService)
        {
            _userService = userService;
        }
        public async Task<IActionResult> Index()
        {
            var resSettigUserRead = this.CheckPermission("SettingUser", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigUserRead))
            {
                return new RedirectResult(resSettigUserRead);
            }
            return View();
        }

        public async Task<IActionResult> Users()
        {
            var resSettigUserRead = this.CheckPermission("SettingUser", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigUserRead))
            {
                return new RedirectResult(resSettigUserRead);
            }
            return View();
        }

        #region API USERS

        public IActionResult GetAll()
        {
            var model = _userService.GetAllAsync();

            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            var model = await _userService.GetById(id);

            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var model = _userService.GetAllPagingAsync(keyword, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(UserParamModel userVm)
        {
            try
            {
                CreateUpdateDeleteResultModel<Guid> result = new CreateUpdateDeleteResultModel<Guid>();
                if (userVm.Id == null)
                {
                    var resSettigUserCreate = this.CheckPermission("SettingUser", Operations.Create).Result;
                    if (!string.IsNullOrEmpty(resSettigUserCreate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserCreate });
                    }
                    result = await _userService.AddAsync(userVm);
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = 200, Message = "Thêm mới người dùng " + userVm.FullName + " thành công", Data = result.Id });
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = 416, Message = "Tài khoản " + userVm.Email + " đã tồn tại trong hệ thống.", Data = "" });
                    else
                        return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var resSettigUserUpdate = this.CheckPermission("SettingUser", Operations.Update).Result;
                    if (!string.IsNullOrEmpty(resSettigUserUpdate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserUpdate });
                    }
                    result = await _userService.UpdateAsync(userVm);
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = 200, Message = "Cập nhật người dùng " + userVm.FullName + " thành công", Data = result.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteEntity(Guid id)
        {
            var resSettigUserDelete = this.CheckPermission("SettingUser", Operations.Delete).Result;
            if (!string.IsNullOrEmpty(resSettigUserDelete))
            {
                return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserDelete });
            }
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                await _userService.DeleteAsync(id);

                return new OkObjectResult(new { StatusCode = 200, Message = "Xóa vai trò thành công", Data = id });
            }
        }
        #endregion
    }
}
