﻿using Ngochau.Models;
using Ngochau.Models.Common;
using Ngochau.Web.Authorization;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Linq;

namespace Ngochau.Web.Areas.Cpanel.Controllers.Components
{
    public class CpanelSideBarViewComponent : ViewComponent
    {
        private IFunctionService _functionService;
        public CpanelSideBarViewComponent(IFunctionService functionService)
        {
            _functionService = functionService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var roles = ((ClaimsPrincipal)User).GetSpecificClaim("Roles");
            List<Function> functions = new List<Function>();
            if (roles.Split(";").Contains(CommonConstants.AppRole.AdminRole))
            {
                functions = _functionService.GetAll(string.Empty).Result.ToList();
            }
            else
            {
                string userId = ((ClaimsPrincipal)User).GetSpecificClaim("UserId");
                //TODO: Get by permission
                functions = _functionService.GetFunctionWithRoles(userId).Result.ToList();
            }
            return View(functions);
        }
    }
}
