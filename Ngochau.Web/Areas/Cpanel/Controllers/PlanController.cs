﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Ngochau.Models;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class PlanController : BaseController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        IPlanService _planService;

        public PlanController(IWebHostEnvironment hostingEnvironment, IAuthorizationService authorizationService, IPlanService planService) : base(authorizationService)
        {
            _hostingEnvironment = hostingEnvironment;
            _planService = planService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Plan()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetPOForPlan(Guid poId)
        {
            var model = await _planService.GetPOForPlan(poId);
            return new OkObjectResult(model);
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _planService.GetAll().Select(p => new Plan() { Id = p.Id, Name = p.Code + " - " + p.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            return new OkObjectResult(_planService.GetAllPaging(keyword, status, page, pageSize));
        }

        #region API
        [HttpPost]
        public async Task<IActionResult> SavePlan(PlanParamModel.PlanInsertParamModel model)
        {
            try
            {
                if (model.PlanId == Guid.Empty || model.PlanId == null)
                {
                    var result = await _planService.InsertPlan(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Plan đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Plan đã thêm thành công vào hệ thống", Data = result.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var result = await _planService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Kế hoạch " + model.Name + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Kế hoạch " + model.Name + " đã cập nhật thành công", Data = model.PlanId });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeletePlan(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> result = await _planService.DeletePlan(id);
            return new OkObjectResult(result);
        }


        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            PlanResultModel.PlanGetByIdResultModel model = await _planService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetListPlanByCustomerId(Guid customerId)
        {
            List<Ngochau.Models.NgoChauModel.Plan> model = new List<Ngochau.Models.NgoChauModel.Plan>();
            model = _planService.GetListPlanByCustomerId(customerId).Select(x => new Ngochau.Models.NgoChauModel.Plan() { Id = x.Id, Name = x.Code + " - " + x.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult ExportPlans(Guid[] ids)
        {
            var fileName = "Plan-" + DateTime.Now.ToString("HH-MM-yyyy") + ".xlsx";
            List<Plan> lstPlans = new List<Plan>();
            lstPlans = _planService.GetListPlanByIds(ids);
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);

            // Tạo một package Excel mới
            using (var workbook = new XLWorkbook())
            {
                // Tạo một worksheet mới
                var ws = workbook.Worksheets.Add("FEB");

                // Thêm tiêu đề cho các cột
                ws.Cell(1, 1).Value = "IDX#";
                ws.Cell(1, 2).Value = "PO#";
                ws.Cell(1, 3).Value = "Product ID";
                ws.Cell(1, 4).Value = "Item ID";
                ws.Cell(1, 5).Value = "Product Description";
                ws.Cell(1, 6).Value = "Color";
                ws.Cell(1, 7).Value = "Finish";
                ws.Cell(1, 8).Value = "Box Vol";
                ws.Cell(1, 9).Value = "QTY";
                ws.Cell(1, 10).Value = "Unit Price";
                ws.Cell(1, 11).Value = "Extra charge";
                ws.Cell(1, 12).Value = "Total amount (FOB)";
                ws.Cell(1, 13).Value = "Batch Volume";
                ws.Cell(1, 14).Value = "PO ETD soonest";
                ws.Cell(1, 15).Value = "PO ETD latest";
                ws.Cell(1, 16).Value = "Note";
                ws.FirstRow().Style.Font.Bold = true;
                // ...
                int i = 0;
                // Gán dữ liệu cho các ô trong ws
                foreach (var plan in lstPlans)
                {
                    int stt = 0;
                    foreach (var item in plan.PlanDetails)
                    {
                        ws.Cell(i + 2, 1).Value = stt++;
                        ws.Cell(i + 2, 2).Value = plan.Code;
                        //ws.Cell(i + 2, 3).Value = item.SKU;
                        //ws.Cell(i + 2, 4).Value = item.ItemCode;
                        //ws.Cell(i + 2, 5).Value = item.ProductName;
                        //ws.Cell(i + 2, 6).Value = item.ColorCode;
                        //ws.Cell(i + 2, 7).Value = item.Finish;
                        //ws.Cell(i + 2, 8).Value = item.BatchVolume;

                        ws.Cell(i + 2, 9).Value = item.Quantity;
                        ws.Cell(i + 2, 10).Value = item.UnitPrice;
                        //ws.Cell(i + 2, 11).Value = item.ExtraCharge;
                        //ws.Cell(i + 2, 12).Value = item.TotalMoney;
                        //ws.Cell(i + 2, 13).Value = item.BatchVolume;
                        ws.Cell(i + 2, 14).Value = "TBC";
                        ws.Cell(i + 2, 15).Value = "TBC";
                        ws.Cell(i + 2, 16).Value = "";
                        i++;
                    }
                    //ws.Cell(i + 3, 12).Value = "TOTAL AMOUNT : $" + plan.PlanDetails.Sum(x => x.TotalMoney);
                    //ws.Cell(i + 4, 12).Value = "DEPOSIT AMOUNT : $" + plan.PlanDetails.Sum(x => x.Deposit);
                    //ws.Cell(i + 5, 12).Value = "BALANCE AMOUNT : $" + (plan.PlanDetails.Sum(x => x.TotalMoney) - plan.PlanDetails.Sum(x => x.Deposit));
                    i += 4;
                }



                ws.Columns().AdjustToContents();
                workbook.SaveAs(filePath);
                // Lưu trữ tệp Excel
                var filePathLocal = $@"\uploaded\po\export";

                string folder = _hostingEnvironment.WebRootPath + filePathLocal;

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    // Lưu tệp Excel từ luồng dữ liệu vào ổ đĩa
                    using (var fileStream = new FileStream(folder + "/" + fileName, FileMode.Create))
                    {
                        stream.Position = 0;
                        stream.CopyTo(fileStream);
                    }
                }

                // Thực hiện hành động tiếp theo (ví dụ: trả về tệp Excel để tải xuống)
                string path = filePathLocal + "\\" + fileName;
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", data = path });
            }
        }

        [HttpGet]
        public async Task<IActionResult> ExportPlan(Guid planId)
        {
            var dataFile = await _planService.ExportPlan(planId);
            return File(dataFile.Bytes, dataFile.ContentType, dataFile.Name);
        }
        #endregion
    }
}
