﻿using Ngochau.Models;
using Ngochau.Utility;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    [Area("Cpanel")]
    public class AuthenController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        public AuthenController(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult ResetPassword()
        {
            return View();
        }
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region API
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Authen(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(model.Email);
                    //var password = await _userManager.CheckPasswordAsync(user, model.Password);
                    //This doesn't count login failures towards account lockout
                    //To enable password failures to trigger account lockout, set lockoutOnFailure: true
                    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        return new OkObjectResult(new GenericResult(true));
                    }
                    if (result.IsLockedOut)
                    {
                        return new ObjectResult(new GenericResult(false, "Tài khoản đã bị khoá"));
                    }
                    else
                    {
                        return new ObjectResult(new GenericResult(false, "Tên đăng nhập hoặc mất khẩu không đúng, xin kiểm tra lại."));
                    }
                }
                return new ObjectResult(new GenericResult(false, model));
            }
            catch (Exception ex)
            {
                return new ObjectResult(new GenericResult(false, "Lỗi hệ thống, không thể login vào hệ thống vào lúc này. Xin cảm ơn.!"));
            }
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/cpanel/authen/login");
        }
        #endregion
    }
}
