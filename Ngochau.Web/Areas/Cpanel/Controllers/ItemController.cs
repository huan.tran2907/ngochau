﻿using Ngochau.Models;
using Ngochau.Web.Authorization;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class ItemController : BaseController
    {
        IItemService _itemService;
        public ItemController(IAuthorizationService authorizationService, IItemService itemService) : base(authorizationService)
        {
            _itemService = itemService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ItemDetail()
        {
            return View();
        }

        #region API

        [HttpGet]
        public IActionResult GetById(Guid id)
        {
            Item model = new Item();
            model = _itemService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetItems()
        {
            List<Item> model = new List<Item>();
            model = _itemService.GetItems().Select(x => new Item() { Id = x.Id, Name = x.Code + " - " + x.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetItemBySearch(string keyword, int take)
        {
            List<Item> model = new List<Item>();
            model = _itemService.GetItems(keyword, take).Select(x => new Item() { Id = x.Id, Code = x.Code, Name = x.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(Item model)
        {
            try
            {
                if (model.Id == Guid.Empty || model.Id == null)
                {
                    var result = await _itemService.Insert(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Thêm mới thất bại. Mã " + model.Code + " đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã item " + model.Code + " đã thêm vào hệ thống thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var result = await _itemService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã " + model.Code + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã " + model.Code + " đã cập nhật thành công thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }


        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var model = _itemService.GetAllPagingAsync(keyword, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var resSettigUserDelete = this.CheckPermission("SettingUser", Operations.Delete).Result;
            if (!string.IsNullOrEmpty(resSettigUserDelete))
            {
                return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserDelete });
            }
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                await _itemService.Delete(id);

                return new OkObjectResult(new { StatusCode = 200, Message = "Xóa vai trò thành công", Data = id });
            }
        }
        #endregion
    }
}
