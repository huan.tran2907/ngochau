﻿using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class InvoiceController : BaseController
    {
        IInvoiceService _invoiceService;
        public InvoiceController(IAuthorizationService authorizationService, IInvoiceService invoiceService) : base(authorizationService)
        {
            _invoiceService = invoiceService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Invoices()
        {
            return View();
        }

        public IActionResult Invoice()
        {
            return View();
        }

        public IActionResult Deposits()
        {
            return View();
        }

        public IActionResult Deposit()
        {
            return View();
        }

        public IActionResult DepositDetail()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            return new OkObjectResult(_invoiceService.GetAllPaging(keyword, status, page, pageSize));
        }

        [HttpPost]
        public async Task<IActionResult> SaveInvoice(InvoiceParamModel.InvoiceInsertParamModel model)
        {
            try
            {
                if (model.InvoiceId == Guid.Empty || model.InvoiceId == null)
                {
                    var result = await _invoiceService.InsertInvoice(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Hóa đơn đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Hóa đơn đã thêm thành công vào hệ thống", Data = result.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var result = await _invoiceService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Hóa đơn " + model.Name + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Hóa đơn " + model.Name + " đã cập nhật thành công", Data = model.InvoiceId });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }

            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            InvoiceResultModel.InvoiceGetByIdResultModel model = await _invoiceService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteInvoice(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> result = await _invoiceService.DeleteInvoice(id);
            return new OkObjectResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> ApplyPayment(Guid invoiceId)
        {
            return new OkObjectResult(await _invoiceService.ApplyPayment(invoiceId));
        }

        public async Task<IActionResult> ExportInvoice(Guid invoiceId)
        {
            var dataFile = await _invoiceService.ExportInvoice(invoiceId);
            return File(dataFile.Bytes, dataFile.ContentType, dataFile.Name);
        }
    }
}
