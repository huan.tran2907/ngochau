﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Authorization;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class FunctionController : BaseController
    {
        private IFunctionService _functionService;
        public FunctionController(IFunctionService functionService, IAuthorizationService authorizationService) : base(authorizationService)
        {
            _functionService = functionService;
        }
        public async Task<IActionResult> Function()
        {
            var resSettigRead = this.CheckPermission("SettingMenu", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigRead))
            {
                return new RedirectResult(resSettigRead);
            }
            return View();
        }

        public async Task<IActionResult> Functions()
        {
            var resSettigRead = this.CheckPermission("SettingMenu", Operations.Read).Result;
            if (!string.IsNullOrEmpty(resSettigRead))
            {
                return new RedirectResult(resSettigRead);
            }
            return View();
        }

        #region API Function
        /// <summary>
        /// Lấy danh sách function theo dạng cây
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _functionService.GetAll(string.Empty);
            var rootFunctions = model.Where(c => c.ParentId == null);
            var items = new List<Function>();
            foreach (var function in rootFunctions)
            {
                //add the parent category to the item list
                items.Add(function);
                //now get all its children (separate Category in case you need recursion)
                GetByParentId(model.ToList(), function, items);
            }
            return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", Data = items });
        }

        private void GetByParentId(IEnumerable<Function> allFunctions,
            Function parent, IList<Function> items)
        {
            var functionsEntities = allFunctions as Function[] ?? allFunctions.ToArray();
            var subFunctions = functionsEntities.Where(c => c.ParentId == parent.Id).OrderBy(x=>x.SortOrder);
            foreach (var cat in subFunctions)
            {
                //add this category
                items.Add(cat);
                //recursive call in case your have a hierarchy more than 1 level deep
                GetByParentId(functionsEntities, cat, items);
            }
        }

        public async Task<IActionResult> GetAll(string filter)
        {
            try
            {
                Function userVm = new Function();
                var item = await _functionService.GetAll(filter);
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", Data = item });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        public async Task<IActionResult> GetById(string id)
        {
            try
            {
                Function userVm = new Function();
                var item = await _functionService.GetById(id);
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", Data = item });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(Function model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Id))
                {
                    var resSettigUserCreate = this.CheckPermission("SettingMenu", Operations.Create).Result;
                    if (!string.IsNullOrEmpty(resSettigUserCreate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserCreate });
                    }
                    model.Id = model.Code;
                    var result = await _functionService.Insert(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Thêm mới thất bại. Chức năng " + model.Name + " đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Thêm mới chức năng " + model.Code + " thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var resSettigUpdate = this.CheckPermission("SettingMenu", Operations.Update).Result;
                    if (!string.IsNullOrEmpty(resSettigUpdate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUpdate });
                    }
                    var result = await _functionService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Chức năng " + model.Name + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Cập nhật chức năng " + model.Name + " thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            var resSettigRoleDelete = this.CheckPermission("SettingMenu", Operations.Delete).Result;
            if (!string.IsNullOrEmpty(resSettigRoleDelete))
            {
                return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigRoleDelete });
            }
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            var result = await _functionService.Delete(id);
            if (result.ResultType == 200)
            {
                return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Xóa chức năng thành công", Data = id });
            }    
            else
            {
                if (result.ResultType == 404)
                    return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Chức năng không tồn tại trong hệ thống. Xin kiểm tra lại", Data = id });
                else
                    return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
            }
        }
        #endregion

    }
}
