﻿using ClosedXML.Excel;
using ClosedXML.Excel.Drawings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using System;
using ClosedXML.Excel.Drawings;
using static Ngochau.Web.Model.Cpanel.ParamModels.PurchaseOrderParamModel;
using Microsoft.Win32;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class MasterOrderController : BaseController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        IPOService _poService;
        IProductService _productService;
        public MasterOrderController(IWebHostEnvironment hostingEnvironment, IProductService productService, IAuthorizationService authorizationService, IPOService poService) : base(authorizationService)
        {
            _poService = poService;
            _hostingEnvironment = hostingEnvironment;
            _productService = productService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Pis()
        {
            return View();
        }

        public IActionResult PiDetail()
        {
            return View();
        }
        public IActionResult Detail()
        {
            return View();
        }

        public IActionResult MasterDetail()
        {
            return View();
        }

        public IActionResult Master()
        {
            return View();
        }

        public IActionResult MasterOrderDetail()
        {
            return View();
        }

        //#region MASTER ORDER API

        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            MasterOrderItemResultModel model = new MasterOrderItemResultModel();
            model = await _poService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetByPIId(Guid id)
        {
            PIResultModel model = new PIResultModel();
            model = await _poService.GetByPIId(id);
            return new OkObjectResult(model);
        }


        [HttpGet]
        public IActionResult GetPOs()
        {
            List<Ngochau.Models.NgoChauModel.PO> model = new List<Ngochau.Models.NgoChauModel.PO>();
            model = _poService.GetPOs().Select(x => new Ngochau.Models.NgoChauModel.PO() { Id = x.Id, Name = x.Code + " - " + x.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int status, int page, int pageSize)
        {
            var model = _poService.GetAllPagingAsync(keyword, status, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPisPaging(string keyword, int status, int page, int pageSize)
        {
            var model = _poService.GetAllPisPagingAsync(keyword, status, page, pageSize);
            return new OkObjectResult(model);
        }
        [HttpPost]
        public IActionResult ExportPOs(Guid[] ids)
        {
            var fileName = "PO-" + DateTime.Now.ToString("HH-MM-yyyy") + ".xlsx";
            List<Ngochau.Models.NgoChauModel.PO> masterOrders = new List<Ngochau.Models.NgoChauModel.PO>();
            masterOrders = _poService.GetListPOByIds(ids);
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);
            // Tạo một package Excel mới
            using (var workbook = new XLWorkbook())
            {
                // Tạo một worksheet mới
                var worksheet = workbook.Worksheets.Add("PO");

                // Đường dẫn đến file hình ảnh logo
                string logoPath = _hostingEnvironment.WebRootPath + "/uploaded/images/logo/logo.jpg";
                // Chuyển đổi hình ảnh thành Stream
                using (var logoImage = Image.FromFile(logoPath))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        logoImage.Save(memoryStream, logoImage.RawFormat);
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        IXLRange rangeLogo = worksheet.Range("A1:B4");
                        rangeLogo.Merge();
                        // Chèn hình ảnh vào file
                        IXLPicture excelPicture = worksheet.AddPicture(memoryStream)
                            .MoveTo(rangeLogo.FirstCell())
                            .WithSize(70, 70); // Đặt kích thước hình ảnh
                        excelPicture.MoveTo(15, 5);
                    }
                }

                var title = "NGO CHAU TRADING COMPANY LIMITED";
                var address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
                var contact = "Tel: (+84 8) 3730 9465 | Fax: (+84 8) 3730 8465";
                var titlePI = "PURCHASE ORDER";
                IXLRange rangeTitle = worksheet.Range("C1:H1");
                rangeTitle.Merge();
                IXLRange rangeAddress = worksheet.Range("C2:H2");
                rangeAddress.Merge();
                IXLRange rangeContact = worksheet.Range("C3:H3");
                rangeContact.Merge();
                IXLRange rangetitlePI = worksheet.Range("C4:H4");
                rangetitlePI.Merge();
                rangeTitle.FirstCell().Value = title;
                rangeAddress.FirstCell().Value = address;
                rangeContact.FirstCell().Value = contact;
                rangetitlePI.FirstCell().Value = titlePI;

                // Thêm tiêu đề cho các cột
                worksheet.Cell(6, 1).Value = "Số lô SX";
                worksheet.Cell(6, 2).Value = "Chú ý";
                worksheet.Cell(6, 3).Value = "PI date";
                worksheet.Cell(6, 4).Value = "Khách hàng";
                worksheet.Cell(6, 5).Value = "Số PO";
                worksheet.Cell(6, 6).Value = "SKU#";
                worksheet.Cell(6, 7).Value = "Sample ref";
                worksheet.Cell(6, 8).Value = "Item #";
                worksheet.Cell(6, 9).Value = "Tên Hàng";
                worksheet.Cell(6, 10).Value = "Màu";
                worksheet.Cell(6, 11).Value = "Số lượng";
                worksheet.Cell(6, 12).Value = "Đã xuất";
                worksheet.Cell(6, 13).Value = "Còn lại";
                worksheet.Cell(6, 14).Value = "Trạng thái";
                worksheet.Cell(6, 15).Value = "Actual ETD";
                worksheet.Cell(6, 16).Value = "CTR#";
                worksheet.Cell(6, 17).Value = "CBM";
                worksheet.Cell(6, 18).Value = "UnitPrice";
                worksheet.Cell(6, 19).Value = "UnitPriceOld";
                worksheet.Cell(6, 20).Value = "Total CBM";
                worksheet.Cell(6, 21).Value = "Total value";
                worksheet.Cell(6, 22).Value = "Deposit";
                worksheet.Cell(6, 23).Value = "DEPOSIT DATE";
                worksheet.Cell(6, 24).Value = "Payment date";
                worksheet.Cell(6, 25).Value = "Projected ETD";
                worksheet.Cell(6, 26).Value = "Deadline ETD";
                worksheet.Cell(6, 27).Value = "Priority";
                worksheet.Cell(6, 28).Value = "Ghi chú nội bộ NC";
                // ...
                IXLRow row = worksheet.Row(6);
                row.Style.Font.Bold = true;
                row.Style.Fill.SetBackgroundColor(XLColor.Gray);
                row.Style.Fill.SetPatternColor(XLColor.Red);
                // Gán dữ liệu cho các ô trong worksheet
                int i = 5;
                foreach (var po in masterOrders)
                {

                    foreach (var item in po.PODetails)
                    {
                        worksheet.Cell(i + 2, 4).Value = po.Customer.Code;
                        worksheet.Cell(i + 2, 5).Value = po.Code;
                        worksheet.Cell(i + 2, 6).Value = item.Product.Code;
                        worksheet.Cell(i + 2, 7).Value = item.Item.MPN;
                        worksheet.Cell(i + 2, 8).Value = item.Item.Code;
                        worksheet.Cell(i + 2, 9).Value = item.Product.Name;
                        worksheet.Cell(i + 2, 10).Value = item.Color == null ? "" : item.Color.Code;
                        worksheet.Cell(i + 2, 11).Value = item.Quantity;
                        worksheet.Cell(i + 2, 12).Value = item.QuantityExport;
                        //worksheet.Cell(i + 2, 14).Value = item.Status == 1 ? "Mới" : item.Status == 2 ? "Đang xử lý" : item.Status == 3 ? "Đóng" : "";
                        worksheet.Cell(i + 2, 17).Value = item.CBM;
                        worksheet.Cell(i + 2, 18).Value = item.UnitPrice;
                        worksheet.Cell(i + 2, 19).Value = item.UnitPriceOld;
                        worksheet.Cell(i + 2, 20).Value = 0;
                        worksheet.Cell(i + 2, 21).Value = item.Quantity * item.UnitPrice;
                        worksheet.Cell(i + 2, 22).Value = item.Deposit;
                        i++;
                    }
                }

                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(filePath);
                // Lưu trữ tệp Excel
                var filePathLocal = $@"\uploaded\po\export";

                string folder = _hostingEnvironment.WebRootPath + filePathLocal;

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    // Lưu tệp Excel từ luồng dữ liệu vào ổ đĩa
                    using (var fileStream = new FileStream(folder + "/" + fileName, FileMode.Create))
                    {
                        stream.Position = 0;
                        stream.CopyTo(fileStream);
                    }
                }

                // Thực hiện hành động tiếp theo (ví dụ: trả về tệp Excel để tải xuống)
                string path = filePathLocal + "\\" + fileName;
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", data = path });
            }
        }

        [HttpPost]
        public IActionResult ExportPIs(Guid[] ids)
        {
            var fileName = "PO-" + DateTime.Now.ToString("HH-MM-yyyy") + ".xlsx";
            List<PI> lstPIs = new List<PI>();
            lstPIs = _poService.GetListPIByIds(ids);
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);

            // Tạo một package Excel mới
            using (var workbook = new XLWorkbook())
            {
                // Tạo một worksheet mới
                var worksheet = workbook.Worksheets.Add("PI");

                // Đường dẫn đến file hình ảnh logo
                string logoPath = _hostingEnvironment.WebRootPath + "/uploaded/images/logo/logo.jpg";
                // Chuyển đổi hình ảnh thành Stream
                using (var logoImage = Image.FromFile(logoPath))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        logoImage.Save(memoryStream, logoImage.RawFormat);
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        IXLRange rangeLogo = worksheet.Range("A1:B4");
                        rangeLogo.Merge();
                        // Chèn hình ảnh vào file
                        IXLPicture excelPicture = worksheet.AddPicture(memoryStream)
                            .MoveTo(rangeLogo.FirstCell())
                            .WithSize(70, 70); // Đặt kích thước hình ảnh
                        excelPicture.MoveTo(15, 5);
                    }
                }

                var title = "NGO CHAU TRADING COMPANY LIMITED";
                var address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
                var contact = "Tel: (+84 8) 3730 9465 | Fax: (+84 8) 3730 8465";
                var titlePI = "PRO-FORMA INVOICE";
                IXLRange rangeTitle = worksheet.Range("C1:H1");
                rangeTitle.Merge();
                IXLRange rangeAddress = worksheet.Range("C2:H2");
                rangeAddress.Merge();
                IXLRange rangeContact = worksheet.Range("C3:H3");
                rangeContact.Merge();
                IXLRange rangetitlePI = worksheet.Range("C4:H4");
                rangetitlePI.Merge();
                rangeTitle.FirstCell().Value = title;
                rangeAddress.FirstCell().Value = address;
                rangeContact.FirstCell().Value = contact;
                rangetitlePI.FirstCell().Value = titlePI;

                // Thêm tiêu đề cho các cột
                worksheet.Cell(6, 1).Value = "IDX#";
                worksheet.Cell(6, 2).Value = "PO#";
                worksheet.Cell(6, 3).Value = "Product ID";
                worksheet.Cell(6, 4).Value = "Item ID";
                worksheet.Cell(6, 5).Value = "Product Description";
                //worksheet.Cell(6, 6).Value = "Color";
                worksheet.Cell(6, 7).Value = "Finish";
                worksheet.Cell(6, 8).Value = "Box Vol";
                worksheet.Cell(6, 9).Value = "QTY";
                worksheet.Cell(6, 10).Value = "Unit Price";
                worksheet.Cell(6, 11).Value = "Extra charge";
                worksheet.Cell(6, 12).Value = "Total amount (FOB)";
                worksheet.Cell(6, 13).Value = "Batch Volume";
                worksheet.Cell(6, 14).Value = "PO ETD soonest";
                worksheet.Cell(6, 15).Value = "PO ETD latest";
                worksheet.Cell(6, 16).Value = "Note";
                worksheet.FirstRow().Style.Font.Bold = true;
                IXLRow row = worksheet.Row(6);
                row.Style.Font.Bold = true;
                // ...
                int i = 5;
                // Gán dữ liệu cho các ô trong worksheet
                decimal? totalMoney = 0;
                decimal? totalDeposit = 0;
                decimal? totalMoneyDeposi = 0;
                
                foreach (var po in lstPIs)
                {
                    int stt = 0;
                    foreach (var item in po.PIDetails)
                    {
                        var poItem = _poService.GetById(Guid.Parse(po.POId.ToString()));
                        worksheet.Cell(i + 2, 1).Value = stt++;
                        worksheet.Cell(i + 2, 2).Value = poItem == null ? "" : poItem.Result.Code;
                        worksheet.Cell(i + 2, 3).Value = item.Product.Code;
                        worksheet.Cell(i + 2, 4).Value = item.Item.Code;
                        worksheet.Cell(i + 2, 5).Value = item.Product.Name;
                        worksheet.Cell(i + 2, 7).Value = item.Color.Code;
                        worksheet.Cell(i + 2, 8).Value = item.Item.CBM;// Chính là VolBox;
                        worksheet.Cell(i + 2, 9).Value = item.Quantity;
                        worksheet.Cell(i + 2, 10).Value = item.UnitPrice;
                        worksheet.Cell(i + 2, 11).Value = item.Item.BoxWeight;
                        worksheet.Cell(i + 2, 12).Value = item.Quantity * item.UnitPrice;
                        worksheet.Cell(i + 2, 13).Value = item.Item.BoxDimension;
                        worksheet.Cell(i + 2, 14).Value = "TBC";
                        worksheet.Cell(i + 2, 15).Value = "TBC";
                        worksheet.Cell(i + 2, 16).Value = "";
                        //Tính tiền : 
                        totalMoney += item.Quantity * item.UnitPrice;
                        i++;
                    }
                    //totalMoney += po.PIDetails.Sum(x => x.TotalMoney);
                    totalDeposit += po.PIDetails.Sum(x => x.Deposit);
                    totalMoneyDeposi += totalMoney - po.PIDetails.Sum(x => x.Deposit);
                }

                worksheet.Cell(i + 3, 12).Value = "TOTAL AMOUNT : $" + totalMoney;
                worksheet.Cell(i + 4, 12).Value = "DEPOSIT AMOUNT : $" + totalDeposit;
                worksheet.Cell(i + 5, 12).Value = "BALANCE AMOUNT : $" + totalMoneyDeposi;

                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(filePath);
                // Lưu trữ tệp Excel
                var filePathLocal = $@"\uploaded\po\export";

                string folder = _hostingEnvironment.WebRootPath + filePathLocal;

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    // Lưu tệp Excel từ luồng dữ liệu vào ổ đĩa
                    using (var fileStream = new FileStream(folder + "/" + fileName, FileMode.Create))
                    {
                        stream.Position = 0;
                        stream.CopyTo(fileStream);
                    }
                }

                // Thực hiện hành động tiếp theo (ví dụ: trả về tệp Excel để tải xuống)
                string path = filePathLocal + "\\" + fileName;
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công", data = path });
            }
        }

        void SetMergedHeaderCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isCenter = false)
        {
            var titleCell = worksheet.Cells[cellRange];
            titleCell.Merge = true;
            titleCell.Value = text;
            titleCell.Style.Font.Size = fontSize;
            titleCell.Style.Font.Bold = isBold;
            titleCell.Style.HorizontalAlignment = isCenter ? ExcelHorizontalAlignment.Center : ExcelHorizontalAlignment.Left;
            titleCell.AutoFitColumns();
        }

        [HttpPost]
        public async Task<IActionResult> Save(MasterOrderParamModel model)
        {
            try
            {
                var duplicateCodes = model.MasterOrderDetailParamModel
                .GroupBy(p => p.StrProduct)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key)
                .ToList();

                if (duplicateCodes.Any())
                {
                    Console.WriteLine("Danh sách sản phẩm có mã code trùng lặp:");
                    foreach (var code in duplicateCodes)
                    {
                        return new OkObjectResult(new { StatusCode = 416, Message = "Sản phẩm : " + code + " đang trùng, xin kiểm tra lại" });
                    }
                }

                if (model.Id == Guid.Empty || model.Id == null)
                {
                    //var resSettigUserCreate = this.CheckPermission("SettingMenu", Operations.Create).Result;
                    //if (!string.IsNullOrEmpty(resSettigUserCreate))
                    //{
                    //    return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserCreate });
                    //}
                    var result = await _poService.Insert(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã " + model.Code + " đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã đơn hang : " + model.Code + " đã thêm thành công vào hệ thống", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
                else
                {
                    //var resSettigUpdate = this.CheckPermission("SettingMenu", Operations.Update).Result;
                    //if (!string.IsNullOrEmpty(resSettigUpdate))
                    //{
                    //    return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUpdate });
                    //}
                    var result = await _poService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã đơn hàng " + model.Name + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã đơn hàng " + model.Name + " đã cập nhật thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SavePI(PIParamModel model)
        {
            try
            {
                if (model.PIId == Guid.Empty || model.PIId == null)
                {
                    var result = await _poService.InsertPI(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "PI đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "PI đã thêm thành công vào hệ thống", Data = model.PIId });
                    else
                        return new BadRequestObjectResult(new { StatusCode = "", Message = $"Lỗi hệ thống" });
                }
                else
                {
                    return new OkObjectResult(new { StatusCode = 200, Message = "PI không được cập nhật", Data = model.PIId });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            //var resSettigUserDelete = this.CheckPermission("SettingUser", Operations.Delete).Result;
            //if (!string.IsNullOrEmpty(resSettigUserDelete))
            //{
            //    return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserDelete });
            //}
            await _poService.Delete(id);

            return new OkObjectResult(new { StatusCode = 200, Message = "Xóa đơn hàng thành công", Data = id });

        }

        [HttpGet]
        public IActionResult GetListPOByCustomerId(Guid customerId)
        {
            List<Ngochau.Models.NgoChauModel.PO> model = new List<Ngochau.Models.NgoChauModel.PO>();
            model = _poService.GetListPOByCustomerId(customerId).Select(x => new Ngochau.Models.NgoChauModel.PO() { Id = x.Id, Name = x.Code + " - " + x.Name }).ToList();
            return new OkObjectResult(model);
        }
    }
}
