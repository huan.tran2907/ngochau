﻿using Ngochau.Models;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class StatusController : BaseController
    {
        IStatusService _statusService;
        public StatusController(IAuthorizationService authorizationService, IStatusService statusService) : base(authorizationService)
        {
            _statusService = statusService;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region API
        [HttpGet]
        public IActionResult GetStatus()
        {
            List<Status> model = new List<Status>();
            model = _statusService.GetStatus().Select(x => new Status() { Id = x.Id, Name = x.Name }).ToList();
            return new OkObjectResult(model);
        }
        #endregion
    }
}
