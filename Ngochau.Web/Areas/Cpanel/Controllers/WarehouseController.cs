﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class WarehouseController : BaseController
    {
        public WarehouseController(IAuthorizationService authorizationService) : base (authorizationService)
        {

        }
        public IActionResult Warehouse()
        {
            return View();
        }

        public IActionResult WarehouseDetail()
        {
            return View();
        }
    }
}
