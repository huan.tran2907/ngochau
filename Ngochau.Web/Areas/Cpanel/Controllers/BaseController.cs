﻿using Ngochau.Web.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    [Area("Cpanel")]
    [Authorize]
    public class BaseController : Controller
    {
        private readonly IAuthorizationService _authorizationService;
        public BaseController(IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }
        public async Task<string> CheckPermission(string functionId, OperationAuthorizationRequirement action)
        {
            var result = await _authorizationService.AuthorizeAsync(User, functionId, action);
            if (result.Succeeded == false)
                return "/cpanel/authen/accessDenied";
            else
                return string.Empty;
        }

        protected string GetUserIdLogin()
        {
            return User.FindFirstValue(ClaimTypes.Name);
        }
        protected string GetProfileUserLogin()
        {
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}
