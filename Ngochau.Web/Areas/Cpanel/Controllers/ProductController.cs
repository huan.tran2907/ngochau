﻿using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class ProductController : BaseController
    {
        IProductService _productService;
        public ProductController(IProductService productService, IAuthorizationService authorizationService) : base(authorizationService)
        {
            _productService = productService;
        }
        public IActionResult Product()
        {
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }
        
        #region  API CAtegory
        [HttpGet]
        public IActionResult GetById(Guid id)
        {
            ProductResultModel model = new ProductResultModel();
            model = _productService.GetById(id);
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetProducts(string keyword,int take)
        {
            List<Product> products = new List<Product>();
            products = _productService.GetProducts(keyword, take).Results.Select(x => new Product() { Id = x.Id, Code = x.BarCode, Name = x.Name }).ToList();
            return new OkObjectResult(products);
        }
        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int status, int page, int pageSize)
        {
            var model = _productService.GetAllPagingAsync(keyword, status, page, pageSize);
            return new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEntity(Product model)
        {
            try
            {
                if (model.Id == Guid.Empty || model.Id == null)
                {
                    //var resSettigUserCreate = this.CheckPermission("SettingMenu", Operations.Create).Result;
                    //if (!string.IsNullOrEmpty(resSettigUserCreate))
                    //{
                    //    return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserCreate });
                    //}
                    var result = await _productService.Insert(model);
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã " + model.Code + " đã tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã sản phẩm : " + model.Code + " đã thêm thành công vào hệ thống", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
                else
                {
                    //var resSettigUpdate = this.CheckPermission("SettingMenu", Operations.Update).Result;
                    //if (!string.IsNullOrEmpty(resSettigUpdate))
                    //{
                    //    return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUpdate });
                    //}
                    var result = await _productService.Update(model);
                    if (result.ResultType == 404)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã sản phẩm " + model.Code + " không tồn tại trong hệ thống" });
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = result.ResultType, Message = "Mã sản phẩm " + model.Code + " đã cập nhật thành công", Data = model.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = result.ResultType, Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty || id == null)
                {
                    return new BadRequestObjectResult(new { StatusCode = 404, Message = $"Không tìm thấy sản phẩm để xóa" });
                }
                else
                {
                    var result = await _productService.Delete(id);
                    if (result == 416)
                        return new BadRequestObjectResult(new { StatusCode = 416, Message = $"Sản phẩm không tồn tại để xóa!" });
                    if (result == 404)
                        return new BadRequestObjectResult(new { StatusCode = 416, Message = $"Không tìm thấy sản phẩm để xóa" });
                }
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công" });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }
        #endregion
    }
}
