﻿using Ngochau.Web.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class PermissionController : BaseController
    {
        public PermissionController(IAuthorizationService authorizationService) : base(authorizationService)
        {

        }
        public IActionResult Permission()
        {
            if (!string.IsNullOrEmpty(this.CheckPermission("SettingRole", Operations.Update).Result))
            {
                return new RedirectResult(this.CheckPermission("SettingRole", Operations.Update).Result);
            }
            return View();
        }
    }
}
