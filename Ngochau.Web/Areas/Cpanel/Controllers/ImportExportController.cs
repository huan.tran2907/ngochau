﻿using Ngochau.Web.Service.Cpanel.Interface;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using static Ngochau.Web.Model.Cpanel.ParamModels.PurchaseOrderParamModel;
using Ngochau.Web.Model.ResultModels;
using OfficeOpenXml;
using Path = System.IO.Path;
using DocumentFormat.OpenXml.Spreadsheet;
using Ngochau.Web.Service.Cpanel.Implement;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class ImportExportController : BaseController
    {
        IPOService _poService;
        IColorService _colorService;
        public ImportExportController(IAuthorizationService authorizationService, IPOService poService, IColorService colorService) : base(authorizationService)
        {
            _poService = poService;
            _colorService = colorService;
        }

        [HttpPost]
        public async Task<IActionResult> ImportMasterOrder(IFormFile data, Guid id)
        {
            var files = Request.Form.Files;
            var result = ReadProductInfo(files, id);
            return new OkObjectResult(new { code = 200, message = "OK", data = result.Result.ResultType });
        }
        public Task<CreateUpdateDeleteResultModel<Guid>> ReadProductInfo(IFormFileCollection files, Guid customerId)
        {
            PO pO = new PO();
            pO.CustomerId = customerId;
            pO.Products = new List<ProductItem>();
            foreach (var file in files)
            {
                if (file.ContentType == "application/pdf")
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        file.CopyTo(memoryStream);
                        var reader = new PdfReader(memoryStream.ToArray());

                        for (int page = 1; page <= reader.NumberOfPages; page++)
                        {
                            var strategy = new LocationTextExtractionStrategy();
                            var currentPageText = PdfTextExtractor.GetTextFromPage(reader, page, strategy);
                            // In ra thông tin sản phẩm từ trang PDF hiện tại
                            System.Console.WriteLine($"Product info on page {page}:");
                            System.Console.WriteLine(currentPageText);
                            if(string.IsNullOrEmpty(pO.PurchaseOrder) || string.IsNullOrWhiteSpace(pO.PurchaseOrder))
                            {
                                pO = PurchaseOrder(currentPageText);
                            }    
                            var pattern = @"Master Barcode\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n.+?\n(.+?)\n(.+?)\n.+?\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n(.+?)\n";
                            MatchCollection matches = Regex.Matches(currentPageText, pattern, RegexOptions.Singleline);
                            
                            try
                            {
                                for(int i = 0; i < matches.Count(); i++)
                                {
                                    ProductItem product = GetProductsFromInput(matches[i].ToString());
                                    pO.Products.Add(product);
                                }    
                            }
                            catch (Exception ex)
                            {
                                reader.Close(); 
                            }
                        }
                        reader.Close();
                    }
                }
            }
            pO.CustomerId = customerId;
            return _poService.InsertPDFExcel(pO);
        }

        public ProductItem GetProductsFromInput(string input)
        {
            // Split the input into lines
            string[] lines = input.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            ProductItem product = new ProductItem();
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].ToString().StartsWith("Master Barcode"))
                {
                    try
                    {
                        //Hàm lấy barcode
                        string[] parts = lines[i + 1].Split(' ');

                        if(parts.Length > 0 && parts.Length == 2) //TH1
                        {
                            product.ProductBarCode = parts[0];
                            product.MPN = parts[1];
                            //Lấy giá trị
                            string[] partsSub = lines[i + 2].Split(' ');
                            if (partsSub.Length > 0)
                            {
                                product.UnitsOrdered = int.Parse(partsSub[1]);
                                product.UnitCost = decimal.Parse(partsSub[2]);
                                product.PackCost = decimal.Parse(partsSub[3]);
                                product.TotalAmonut = decimal.Parse(partsSub[4]);
                                product.UnitCBM = decimal.Parse(partsSub[5]);
                            }
                            string[] partsPacksOrdered = lines[i + 3].Split(' ');
                            if (partsPacksOrdered.Length > 0)
                            {
                                product.PacksOrdered = int.Parse(partsPacksOrdered[1]);
                                product.TotalCBM = decimal.Parse(partsPacksOrdered[2]);
                            }
                            product.ProductName = lines[i + 4].Trim();
                            product.ProductCode = lines[i + 5].Trim();
                        }   
                        else //TH2
                        {
                            if (parts.Length > 0 && parts.Length == 3)
                            {
                                product.ProductBarCode = parts[0];
                                product.MPN = parts[1];
                                product.TotalAmonut = decimal.Parse(parts[2]);
                            }
                            //Lấy giá trị
                            string[] parts1 = lines[i + 2].Split(' ');
                            if (parts1.Length > 0)
                            {
                                product.UnitsOrdered = int.Parse(parts1[1]);
                                product.UnitCost = decimal.Parse(parts1[2]);
                                product.PacksOrdered = int.Parse(parts1[3]);
                                product.PackCost = decimal.Parse(parts1[4]);
                                product.UnitCBM = decimal.Parse(parts1[5]);
                                product.TotalCBM = decimal.Parse(parts1[6]);
                            }
                            product.ProductName = lines[i + 3].Trim();
                            product.ProductCode = lines[i + 4].Trim();
                        }
                    }
                    catch(Exception ex) { }
                }


                if (lines[i].ToString().StartsWith("Item#"))
                {
                    product.ItemCode = GetValueFromLine(lines[i]);

                }
                if (lines[i].ToString().StartsWith("Finish"))
                {
                    product.Finish = GetValueFromLine(lines[i]);

                }

                if (lines[i].ToString().StartsWith("Dimension"))
                {
                    product.Dimension = GetValueFromLine(lines[i]);
                }

                if (lines[i].ToString().StartsWith("Description"))
                {
                    product.Description = GetValueFromLine(lines[i]);
                }

                if (lines[i].ToString().StartsWith("Supplier Code"))
                {
                    string[] parts5 = lines[i].ToString().Trim().Split(':');
                    if (parts5.Length > 0)
                    {
                        string[] strSupplierCode = parts5[1].Split(' ');
                        product.SupplierCode = strSupplierCode[1];
                        string[] strSupplierName = parts5[2].Split(' ');
                        product.SupplierName = strSupplierName[1];
                        product.SupplierMaterial = parts5[4];
                    }
                }

                if (lines[i].ToString().StartsWith("Box Weight"))
                {
                    product.BoxWeight = GetValueFromLine(lines[i]);
                }
            }
            return product;
        }

        public static List<ProductItem> ExtractProducts(string pdfContent)
        {
            List<ProductItem> products = new List<ProductItem>();

            string[] lines = pdfContent.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            int lineCount = lines.Length;

            for (int i = 0; i < lineCount; i++)
            {
                string line = lines[i].Trim();

                if (line.StartsWith("Master Barcode"))
                {
                    string mpn = GetValueFromLine(line);
                    string description = GetValueFromLine(lines[i + 1]);
                    int quantity = int.Parse(GetValueFromLine(lines[i + 2]));
                    decimal price = decimal.Parse(GetValueFromLine(lines[i + 3]));

                    ProductItem product = new ProductItem
                    {
                        MPN = mpn,
                        Description = description,
                    };

                    products.Add(product);
                }
            }

            return products;
        }

        private static string GetValueFromLine(string line)
        {
            int separatorIndex = line.IndexOf(':');
            if (separatorIndex >= 0)
            {
                return line.Substring(separatorIndex + 1).Trim();
            }

            return string.Empty;
        }

        private PO PurchaseOrder(string input)
        {
            PO purchaseOrder = new PO();
            string poPattern = @"PO#[\s\n]+(\w+)";
            string orderDatePattern = @"Order Date[\s\n]+(\d{2}/\d{2}/\d{4})";
            string shipViaPattern = @"Ship VIA[\s\n]+(\w+)";
            string mgExFactoryPattern = @"MG Ex-Factory[\s\n]+(\d{2}/\d{2}/\d{4})";
            string mgEtaUsPortPattern = @"MG ETA US Port[\s\n]+(\d{2}/\d{2}/\d{4})";
            string mgAhdPattern = @"MG AHD[\s\n]+(\d{2}/\d{2}/\d{4})";
            purchaseOrder.PurchaseOrder = GetMatchValue(input, poPattern);
            purchaseOrder.OrderDate = GetMatchValue(input, orderDatePattern);
            purchaseOrder.ShipVIA = GetMatchValue(input, shipViaPattern);
            purchaseOrder.MGExFactory = GetMatchValue(input, mgExFactoryPattern);
            purchaseOrder.MGETAUSPort = GetMatchValue(input, mgEtaUsPortPattern);
            purchaseOrder.MGAHD = GetMatchValue(input, mgAhdPattern);
            return purchaseOrder;
        }
        private static string GetMatchValue(string input, string pattern)
        {
            Match match = Regex.Match(input, pattern);
            return match.Success ? match.Groups[1].Value.Trim() : "";
        }

        [HttpPost]
        public async Task<IActionResult> ImportColors(IFormFile data)
        {
            var files = Request.Form.Files;
            if (files == null || files.Count() <= 0)
            {
                return BadRequest("File is empty or null.");
            }
            Dictionary<string, string> colors = new Dictionary<string, string>();
            foreach (var file in files)
            {
                // Kiểm tra xem tệp có phải là tệp Excel không (đảm bảo tệp có đuôi .xlsx)
                if (!file.FileName.EndsWith(".xlsx"))
                {
                    return BadRequest("Không đúng định dạng file Excel, xin kiểm tra lại (.xlsx).");
                }

                // Đường dẫn tạm thời để lưu tệp Excel trước khi đọc
                var tempFilePath = Path.GetTempFileName();

                // Lưu tệp Excel vào đường dẫn tạm thời
                using (var stream = new FileStream(tempFilePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                

                // Đọc dữ liệu từ tệp Excel sử dụng thư viện EPPlus
                using (var package = new ExcelPackage(new FileInfo(tempFilePath)))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0]; // Chọn trang tính đầu tiên (index bắt đầu từ 0)

                    int rowCount = worksheet.Dimension.Rows; // Số hàng trong bảng
                    int colCount = worksheet.Dimension.Columns; // Số cột trong bảng

                    // Đọc dữ liệu từ từng ô trong bảng và thêm vào danh sách colors
                    for (int row = 1; row <= rowCount; row++)
                    {

                        colors.Add(worksheet.Cells[row, 1].Value.ToString(), worksheet.Cells[row, 2].Value.ToString());

                        //for (int col = 1; col <= colCount; col++)
                        //{
                        //    object cellValue = worksheet.Cells[row, col].Value;
                        //    if (cellValue != null)
                        //    {
                        //        colors.Add(cellValue.ToString(), cellValue.ToString());
                        //    }
                        //}
                    }
                }

                // Xóa tệp Excel tạm thời sau khi đã đọc dữ liệu
                System.IO.File.Delete(tempFilePath);
            }

            _colorService.InsertUpdateColor(colors);

            // Trả về danh sách colors trong kết quả của hàm
            return new OkObjectResult(new { code = 200, message = "OK", data = "" });
        }
    }
}