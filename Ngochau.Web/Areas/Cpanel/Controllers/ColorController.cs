﻿using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Authorization;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class ColorController : BaseController
    {
        IColorService _colorService;
        public ColorController(IAuthorizationService authorizationService, IColorService colorService) : base(authorizationService)
        {
            _colorService = colorService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Color()
        {
            return View();
        }
        #region API
        [HttpGet]
        public IActionResult GetColors()
        {
            List<Color> model = new List<Color>();
            model = _colorService.GetColors().Select(x => new Color() { Id = x.Id, Name = x.Code + " | " + x.Name }).ToList();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetColorBySearch(string keyword, int take)
        {
            List<Color> model = new List<Color>();
            model = _colorService.GetColors(keyword, take).Select(x => new Color() { Id = x.Id, Code = x.Code, Name = x.Code + " | " + x.Name }).ToList();
            return new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            var model = _colorService.GetById(id);
            return new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEntity(Color userVm)
        {
            try
            {
                CreateUpdateDeleteResultModel<Guid> result = new CreateUpdateDeleteResultModel<Guid>();
                if (userVm.Id == null || userVm.Id == Guid.Empty)
                {
                    var resSettigUserCreate = this.CheckPermission("SettingUser", Operations.Create).Result;
                    if (!string.IsNullOrEmpty(resSettigUserCreate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserCreate });
                    }
                    result = await _colorService.Insert(userVm);
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = 200, Message = "Thêm mới container " + userVm.Code + " thành công", Data = result.Id });
                    if (result.ResultType == 416)
                        return new OkObjectResult(new { StatusCode = 416, Message = "Mã " + userVm.Code + " đã tồn tại trong hệ thống.", Data = "" });
                    else
                        return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống" });
                }
                else
                {
                    var resSettigUserUpdate = this.CheckPermission("SettingUser", Operations.Update).Result;
                    if (!string.IsNullOrEmpty(resSettigUserUpdate))
                    {
                        return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserUpdate });
                    }
                    result = await _colorService.Update(userVm);
                    if (result.ResultType == 200)
                        return new OkObjectResult(new { StatusCode = 200, Message = "Cập nhật container " + userVm.Code + " thành công", Data = result.Id });
                    else
                        return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống" });
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var model = _colorService.GetAllPagingAsync(keyword, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var resSettigUserDelete = this.CheckPermission("SettingUser", Operations.Delete).Result;
            if (!string.IsNullOrEmpty(resSettigUserDelete))
            {
                return new OkObjectResult(new { StatusCode = 403, Message = "Bạn không có quyền để thực hiện chức năng này, xin vui lòng liên hệ admin để được cấp quyền", Data = resSettigUserDelete });
            }
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                await _colorService.Delete(id);

                return new OkObjectResult(new { StatusCode = 200, Message = "Xóa vai trò thành công", Data = id });
            }
        }
        #endregion
    }
}
