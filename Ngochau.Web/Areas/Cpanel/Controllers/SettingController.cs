﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Cpanel.Controllers
{
    public class SettingController : BaseController
    {
        public SettingController(IAuthorizationService authorizationService) : base(authorizationService)
        {

        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
