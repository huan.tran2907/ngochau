﻿using Ngochau.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Ngochau.Web.Areas.Admin.Models.EntityModel
{
    public class Function
    {
        public string Id { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { set; get; }

        [Required]
        [StringLength(250)]
        public string URL { set; get; }


        [StringLength(128)]
        public string ParentId { set; get; }

        public string IconCss { get; set; }
        public int SortOrder { set; get; }
        public Status Status { set; get; }
    }
}
