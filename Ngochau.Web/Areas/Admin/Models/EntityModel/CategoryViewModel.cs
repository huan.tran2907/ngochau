﻿using Ngochau.Models;
using Ngochau.Models.Enums;

namespace Ngochau.Web.Areas.Admin.Models.EntityModel
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public string Description { get; set; }
        public string Content { get; set; }

        public int? ParentId { get; set; }

        public int? HomeOrder { get; set; }

        public string Image { get; set; }

        public int? HomeFlag { get; set; }

        public DateTime DateCreated { set; get; }
        public DateTime DateModified { set; get; }
        public int SortOrder { set; get; }
        public int Status { set; get; }
        public string SeoPageTitle { set; get; }
        public string SeoAlias { set; get; }
        public string SeoKeywords { set; get; }
        public string SeoDescription { set; get; }

        public string Language { get; set; }
        public int? Type { get; set; }
    }
}
