﻿using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;
using Microsoft.AspNetCore.Mvc;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Areas.Admin.Controllers
{
    
    public class ProductController : BaseController
    {
        IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        public IActionResult Product()
        {
            return View();
        }
        public IActionResult Products()
        {
            return View();
        }

        #region  API CAtegory
        [HttpGet]
        public IActionResult GetById(Guid id)
        {
            Product model = new Product();
            model = _productService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAll(string keyWord)
        {
            List<Product> model = new List<Product>();
            model = _productService.GetAll(keyWord).ToList();
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(Product model)
        {
            try
            {
                model.DateCreated = DateTime.Now;
                //model.DateModified = DateTime.Now;
                var result = await _productService.InsertOrUpdate(model);
                if (result != 200)
                {
                    return new BadRequestObjectResult(new { StatusCode = result, Message = result == 416 ? "SEO Alias already exists" : "Not found data" });
                }
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công" });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteEntity(Guid id)
        {
            try
            {
                if (id == Guid.Empty || id == null)
                {
                    return new BadRequestObjectResult(new { StatusCode = 404, Message = $"Không tìm thấy tin tức" });
                }
                else
                {
                    var result = await _productService.Delete(id);
                    if (result == 416)
                        return new BadRequestObjectResult(new { StatusCode = 416, Message = $"Sản phẩm không tồn tại để xóa!" });
                    if (result == 404)
                        return new BadRequestObjectResult(new { StatusCode = 416, Message = $"Không tìm thấy tin tức" });
                }
                return new OkObjectResult(new { StatusCode = 200, Message = "Thành công" });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { StatusCode = 500, Message = $"Lỗi hệ thống: {ex}" });
            }
        }
        #endregion
    }
}
