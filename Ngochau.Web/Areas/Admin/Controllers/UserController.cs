﻿using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Admin.Controllers
{
    
    public class UserController : BaseController
    {
        IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        public IActionResult Index()
        {
            AppUser mode = new AppUser();
            //mode.Id = Guid.NewGuid();
            mode.Discriminator = "AppUser";
            mode.FullName = "Administrator";
            mode.BirthDay = DateTime.Now;
            mode.Balance = 0;
            mode.Avatar = "";
            mode.CreateDate = DateTime.Now;
            mode.ModifyDate = DateTime.Now;
            mode.Status = 1;
            mode.UserName = "admin@gmail.com";
            mode.NormalizedUserName = "admin@gmail.com";
            mode.Email = "admin@gmail.com";
            mode.NormalizedEmail = "admin@gmail.com";
            mode.EmailConfirmed = true;
            mode.ConcurrencyStamp = "";
            mode.PhoneNumber = "";
            mode.PhoneNumberConfirmed = false;
            mode.TwoFactorEnabled = false;
            mode.LockoutEnd = null;
            mode.LockoutEnabled = false;
            mode.AccessFailedCount = 0;
            var hasher = new PasswordHasher<AppUser>();
            mode.PasswordHash = hasher.HashPassword(mode, "AdminHuanTran@@2907*!");
            _userService.InsertOrUpdate(mode);
            return View();
        }


    }
}
