﻿using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Admin.Controllers
{
    
    public class AdHomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
