﻿using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Areas.Admin.Controllers.Components
{
    public class SideBarViewComponent : ViewComponent
    {
        private IFunctionService _functionService;
        public SideBarViewComponent(IFunctionService functionService)
        {
            _functionService = functionService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            IEnumerable<Function> functions = new List<Function>();
            functions = await _functionService.GetAll(string.Empty);
            //}
            return View(functions);
        }
    }
}
