﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Areas.Admin.Service.IService;

namespace Ngochau.Web.Areas.Admin.Service
{
    public class ProductService : IProductService
    {
        IUnitOfWork _unitOfWork;
        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Product GetById(Guid id)
        {
            return _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == id);
        }
        
        public async Task<int> Delete(Guid id)
        {
            var pro = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == id);
            if (pro != null)
            {
                _unitOfWork.ProductRepository.Removed(pro);
                _unitOfWork.Save();
                return 200;
            }
            else
                return 404;

        }

        public async Task<int> InsertOrUpdate(Product model)
        {
            if (model.Id == null || model.Id == Guid.Empty)
            {
                var entity = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (entity == null) // Thêm mới
                {
                    _unitOfWork.ProductRepository.Add(model);
                }
                else
                {
                    return 416; //SEO Alias already exists
                }
            }
            else
            {
                var itemCate = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == model.Id);
                if (itemCate != null)
                {
                    var alias = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id != model.Id && x.Code == model.Code);
                    if (alias == null)
                    {
                        itemCate.Name = model.Name;
                        //itemCate.Description = model.Description;
                        //itemCate.Content = model.Content;
                        //itemCate.ParentId = model.ParentId;
                        itemCate.Image = model.Image;
                        itemCate.DateCreated = model.DateCreated;
                        //itemCate.DateModified = model.DateModified;
                        //itemCate.SortOrder = model.SortOrder;
                        //itemCate.Status = model.Status;
                        _unitOfWork.ProductRepository.Update(itemCate);
                    }
                    else
                    {
                        return 416; //SEO Alias already exists
                    }
                }
                else
                {
                    return 404; // "Data is not found" });
                }
            }
            _unitOfWork.Save();
            return 200;
        }

        public List<Product> GetAll(string keyWord = null)
        {
            return _unitOfWork.ProductRepository.FindAll(x => string.IsNullOrEmpty(keyWord) || x.Name.Equals(keyWord)).ToList();
        }
    }
}
