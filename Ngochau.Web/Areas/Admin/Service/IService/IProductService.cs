﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Areas.Admin.Service.IService
{
    public interface IProductService
    {
        List<Product> GetAll(string keyWord = null);
        Product GetById(Guid id);
        Task<int> InsertOrUpdate(Product model);
        Task<int> Delete(Guid id);
    }
}
