﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace Ngochau.Web.Areas.Admin.Service
{
    public class UserService : IUserService
    {
        IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;
        RoleManager<AppRole> _roleManager;
        public UserService(IUnitOfWork unitOfWork, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> AddAsync(UserParamModel userVm)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            //var userExist = _userManager.FindByNameAsync(userVm.Email);
            var userExist = _unitOfWork.UserRepository.GetFirstOrDefault(x => x.Email == userVm.Email);
            if (userExist != null)
            {
                idResult.ResultType = 416;
                return idResult;
            }
            try
            {
                var user = new AppUser();
                user.Id = Guid.NewGuid();
                user.Discriminator = "AppUser";
                user.FullName = userVm.FullName;
                user.BirthDay = DateTime.Now;
                user.Balance = 0;
                user.Avatar = userVm.Avatar;
                user.CreateDate = DateTime.Now;
                user.ModifyDate = DateTime.Now;
                user.Status = userVm.Status;
                user.UserName = userVm.UserName;
                user.NormalizedUserName = userVm.UserName;
                user.Email = userVm.Email;
                user.NormalizedEmail = userVm.Email;
                user.EmailConfirmed = true;
                user.PhoneNumber = userVm.PhoneNumber;
                user.PhoneNumberConfirmed = false;
                user.TwoFactorEnabled = false;
                user.LockoutEnd = null;
                user.LockoutEnabled = false;
                user.AccessFailedCount = 0;
                user.Address = userVm.Address;
                var result = await _userManager.CreateAsync(user, userVm.Password);
                if (result.Succeeded && userVm.Roles.Count > 0)
                {
                    var appUser = await _userManager.FindByNameAsync(user.UserName);
                    if (appUser != null)
                    {
                        var currentRoleIds = _roleManager.Roles.ToList().Where(x => userVm.Roles.Contains(x.Code)).Select(a => a.Id).ToList();
                        var roleResult = await _userManager.AddToRolesAsync(appUser, userVm.Roles);
                    }
                }
                idResult.Id = user.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            await _userManager.DeleteAsync(user);
        }

        public async Task<List<UserParamModel>> GetAllAsync()
        {
            List<UserParamModel> lstUser = new List<UserParamModel>();
            foreach (var item in _userManager.Users.ToList())
            {
                UserParamModel itemUser = new UserParamModel()
                {
                    Id = item.Id,
                    FullName = item.FullName,
                    BirthDay = ((DateTime)item.BirthDay).ToString("dd/MM/yyyy"),
                    Email = item.Email,
                    UserName = item.UserName,
                    Address = item.Address,
                    PhoneNumber = item.PhoneNumber,
                    Avatar = item.Avatar,
                    Status = item.Status,
                    CreatedDate = ((DateTime)item.CreateDate).ToString("dd/MM/yyyy")
                };
                lstUser.Add(itemUser);
            };
            return lstUser;
        }

        public PagedResult<UserParamModel> GetAllPagingAsync(string keyword, int page, int pageSize)
        {
            var query = _userManager.Users;
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.FullName.Contains(keyword)
                || x.UserName.Contains(keyword)
                || x.Email.Contains(keyword));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(x => new UserParamModel()
            {
                UserName = x.UserName,
                Avatar = x.Avatar,
                BirthDay = x.BirthDay.ToString(),
                Email = x.Email,
                FullName = x.FullName,
                Id = x.Id,
                PhoneNumber = x.PhoneNumber,
                Status = x.Status,
                CreatedDate = ((DateTime)x.CreateDate).ToString("dd/MM/yyyy")

            }).ToList();
            var paginationSet = new PagedResult<UserParamModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public async Task<UserParamModel> GetById(Guid id)
        {
            UserParamModel userVm = new UserParamModel();
            var item = await _userManager.FindByIdAsync(id.ToString());
            if (item != null)
            {
                userVm.Id = item.Id;
                userVm.FullName = item.FullName;
                userVm.BirthDay = ((DateTime)item.BirthDay).ToString("dd/MM/yyyy");
                userVm.Email = item.Email;
                userVm.UserName = item.UserName;
                userVm.Address = item.Address;
                userVm.PhoneNumber = item.PhoneNumber;
                userVm.Avatar = item.Avatar;
                userVm.Status = item.Status;
                userVm.CreatedDate = ((DateTime)item.CreateDate).ToString("dd/MM/yyyy");
                var roles = await _userManager.GetRolesAsync(item);
                userVm.Roles = roles.ToList();
            }
            return userVm;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> UpdateAsync(UserParamModel userVm)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var user = await _userManager.FindByIdAsync(userVm.Id.ToString());

                //Remove current roles in db
                var currentRoles = await _userManager.GetRolesAsync(user);
                if (currentRoles.Count() > 0)
                {
                    var currentRoleIds = _roleManager.Roles.ToList().Where(x => currentRoles.Contains(x.Code)).Select(a => a.Id).ToList();
                    var lstCurrentRoles = _unitOfWork.UserRoleRepository.GetAll().Where(x => x.UserId == user.Id && currentRoleIds.Contains(x.RoleId)).ToList();
                    _unitOfWork.UserRoleRepository.RemoveRange(lstCurrentRoles);
                    _unitOfWork.Save();
                }

                if (userVm.Roles.Count() > 0)
                {
                    foreach (var item in userVm.Roles)
                    {
                        var roleItem = _roleManager.Roles.ToList().FirstOrDefault(x=>x.Code == item);
                        if(roleItem != null)
                        {
                            IdentityUserRole<Guid> userRole = new IdentityUserRole<Guid>();
                            userRole.UserId = user.Id;
                            userRole.RoleId = roleItem.Id;
                            _unitOfWork.UserRoleRepository.Add(userRole);
                            _unitOfWork.Save();
                        }    
                    }
                }
                //Update user detail
                user.FullName = userVm.FullName;
                user.Status = userVm.Status;
                user.Email = userVm.Email;
                user.PhoneNumber = userVm.PhoneNumber;
                user.Address = userVm.Address;
                user.Avatar = userVm.Avatar;
                var resultUpdate = await _userManager.UpdateAsync(user);
                idResult.Id = user.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }

        public async Task<int> InsertOrUpdate(AppUser model)
        {
            //var entity = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.SeoAlias == model.SeoAlias);
            //if (entity == null) // Thêm mới
            //{
            _unitOfWork.UserRepository.Add(model);
            //}
            //else
            //{
            //return 416; //SEO Alias already exists
            //}
            _unitOfWork.Save();
            return 200;
        }
    }
}
