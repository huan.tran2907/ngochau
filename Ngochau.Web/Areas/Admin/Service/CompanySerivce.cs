﻿using Ngochau.DataAccess.Repository;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Areas.Admin.Service.IService;

namespace Ngochau.Web.Areas.Admin.Service
{
    public class CompanySerivce : ICompanySerivce
    {
        private IUnitOfWork _unitOfWork;
        public CompanySerivce(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Company GetAll()
        {
            return _unitOfWork.CompanyRepository.GetAll().FirstOrDefault();
        }
    }
}
