﻿function DropDownListSelected(jsonData, _strName, html_ID, _selected = '') {
    var listItems = '';
    if (_selected === '') {
        listItems += "<option value='' selected='selected'> ---- " + _strName + "  ----</option>";
        for (var i = 0; i < jsonData.length; i++) {
            listItems += "<option value='" + jsonData[i]['id'] + "'>" + jsonData[i]['name'] + "</option>";
        }
    }
    else {
        listItems += "<option value=''> ---- " + _strName + " ----</option>";
        for (var a = 0; a < jsonData.length; a++) {
            if (_selected.toString() === jsonData[a]['id'].toString()) {
                listItems += "<option selected='selected' value='" + jsonData[a]['id'] + "'>" + jsonData[a]['name'] + "</option>";
            }
            else {
                listItems += "<option value='" + jsonData[a]['id'] + "'>" + jsonData[a]['name'] + "</option>";
            }
        }
    }
    $("#" + html_ID).html(listItems);
}

function RadionButtonSelectedValueSet(name, SelectdValue) {
    $('input[name="' + name + '"][value="' + SelectdValue + '"]').attr('checked', 'checked');
}

function getUrlVars(url) {
    var vars = {};
    var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

var hastSoft = {
    configs: {
        pageSize: 10,
        pageIndex: 1
    },
    notify: function (message, type) {
        $.notify(message, {
            // whether to hide the notification on click
            clickToHide: true,
            // whether to auto-hide the notification
            autoHide: true,
            // if autoHide, hide after milliseconds
            autoHideDelay: 5000,
            // show the arrow pointing at the element
            arrowShow: true,
            // arrow size in pixels
            arrowSize: 5,
            // position defines the notification position though uses the defaults below
            position: '...',
            // default positions
            elementPosition: 'top right',
            globalPosition: 'top right',
            // default style
            style: 'bootstrap',
            // default class (string or [string])
            className: type,
            // show animation
            showAnimation: 'slideDown',
            // show animation duration
            showDuration: 400,
            // hide animation
            hideAnimation: 'slideUp',
            // hide animation duration
            hideDuration: 200,
            // padding between element and notification
            gap: 2
        });
    },
    confirm: function (message, okCallback) {
        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: 'Confirm',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    okCallback();
                }
            }
        });
    },
    dateFormatJson: function (datetime) {
        if (datetime == null || datetime == '')
            return '';
        var newdate = new Date(parseInt(datetime.substr(6)));
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        if (hh < 10)
            hh = "0" + hh;
        if (mm < 10)
            mm = "0" + mm;
        return day + "/" + month + "/" + year;
    },
    dateTimeFormatJson: function (datetime) {
        if (datetime == null || datetime == '')
            return '';
        var newdate = new Date(parseInt(datetime.substr(6)));
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();
        var ss = newdate.getSeconds();
        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        if (hh < 10)
            hh = "0" + hh;
        if (mm < 10)
            mm = "0" + mm;
        if (ss < 10)
            ss = "0" + ss;
        return day + "/" + month + "/" + year + " " + hh + ":" + mm + ":" + ss;
    },
    startLoading: function () {
        if ($('.mark_custome').length > 0)
            $('.mark_custome').removeClass('hide');
    },
    stopLoading: function () {
        if ($('.mark_custome').length > 0)
            $('.mark_custome')
                .addClass('hide');
    },
    getStatus: function (status) {
        if (status === 1)
            return '<span class="badge bg-success">Kích hoạt</span>';
        else
            return '<span class="badge bg-danger">Khoá</span>';
    },

    getIsRule: function (status) {
        if (status === 1)
            return '<span class="badge bg-success">Sử dụng rule đặt biệt diposit</span>';
        else
            return '<span class="badge bg-success">Rule bình thường</span>';
    },

    getStatusProcess: function (status) {
        if (status === 1)
            return '<span class="badge badge-info-lighten">Mới</span>';
        else
            if (status === 2)
                return '<span class="badge badge-warning-lighten">Đang xử lý</span>';
            else
                    if (status === 3)
                        return '<span class="badge badge-success-lighten">Đóng</span>';
    },
    getImage: function (image) {
        if (image === '' || image === null || image === undefined)
            return "<img src='/assets/images/users/no-image.jpg' width='40px' class='rounded-circle' alt='Hình đại diện mặc định'>";
        else
            return "<img src='" + image + "' class='rounded-circle' width='40px' alt='Hình đại diện'>";
    },
    formatNumber: function (number, precision) {
        if (!isFinite(number)) {
            return number.toString();
        }

        var a = number.toFixed(precision).split('.');
        a[0] = a[0].replace(/\d(?=(\d{3})+$)/g, '$&,');
        return a.join('.');
    },
    unflattern: function (arr) {
        var map = {};
        var roots = [];
        for (var i = 0; i < arr.length; i += 1) {
            var node = arr[i];
            node.children = [];
            map[node.id] = i; // use map to look-up the parents
            if (node.parentId !== null) {
                arr[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }
}

$(document).ajaxSend(function (e, xhr, options) {
    if (options.type.toUpperCase() == "POST" || options.type.toUpperCase() == "PUT") {
        var token = $('form').find("input[name='__RequestVerificationToken']").val();
        xhr.setRequestHeader("RequestVerificationToken", token);
    }
});


function RemoveUnicode(obj) {
    debugger;
    var str;
    if (eval(obj))
        str = eval(obj).value;
    else
        str = obj;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    /*thay the 1 so ky tu dat biet*/
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|-+|–|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\“|\”|\&|\#|\[|\]|~|$|_/g, " ");
    /**/
    //str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
    str = str.replace(/\s+/g, "-");
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-  
    str = str.replace(/^\-+|\-+$/g, "");
    //cắt bỏ ký tự - ở đầu và cuối chuỗi 
    //eval(obj).value = str.toUpperCase();
    return str;
}

function ParseUrl(objsent, objreceive) {
    objreceive.val(RemoveUnicode(objsent));
}

