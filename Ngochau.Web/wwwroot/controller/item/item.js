﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    handleButtonSave();
    loadData(id);
    initDropDownProducts('');
});

function initDropDownProducts(selectedId) {
    $.ajax({
        url: "/Cpanel/product/GetProducts",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn sản phẩm', 'ddlProducts', selectedId);
        }
    });
}

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function Save(type) {
    var form = $("#frmItem")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    } else {
        event.preventDefault();
        var data = {
            "Id": $('#txtId').val(),
            "Code": $('#txtCode').val(),
            "Name": $('#txtName').val(),
            "CBM": $('#txtItemCBM').val(),
            "CodeY": $('#txtItemCodeY').val(),
            "PriceNew": $('#txtItemPrice').val(),
            "PriceOld": $('#txtItemPriceOld').val(),
            "Desc": $('#txtDesc').val(),
            "MPN": $('#txtMPN').val(),
            "BoxDimension": $('#txtBoxDimension').val(),
            "BoxWeight": $('#txtBoxWeight').val(),
            "ProductId": $('#ddlProducts').val()
            //"ItemStatus": $("#chkActive").is(":checked") === true ? 1 : 0,
        }
        $.ajax({
            url: '/cpanel/item/SaveEntity',
            type: 'POST',
            beforeSend: function (request) {
                hastSoft.startLoading();
            },
            data: data,
            dataType: "json",
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/item/itemdetail?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/item/itemdetail";
                        if (type === 2)
                            window.location.href = "/cpanel/item/index";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (xhr) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: xhr.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}


function loadData(id) {
    if (id !== undefined) {
        document.getElementById("txtCode").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/item/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                $("#txtId").val(result.id);
                $("#txtCode").val(result.code);
                $("#txtName").val(result.name);
                $("#txtItemCodeY").val(result.codeY);
                $("#txtItemCBM").val(result.cbm);
                $("#txtItemPrice").val(result.priceNew);
                $("#txtItemPriceOld").val(result.priceOld);
                $("#txtMPN").val(result.mpn);
                $("#txtDesc").val(result.desc);
                initDropDownProducts(result.productId);
                $('#txtBoxDimension').val(result.boxDimension);
                $("#txtBoxWeight").val(result.boxWeight);
                $('#chkActive').prop('checked', result.itemStatus === 1 ? true : false);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}