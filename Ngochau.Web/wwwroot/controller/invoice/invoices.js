﻿var selectedInvoiceId = null;

$(document).ready(function () {
    loadData(false);
});

$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});

function OnClickSearch() {
    loadData(true);
}

function loadData(isPageChanged) {
    hastSoft.startLoading();
    var template = $('#table-template-invoice').html();
    var render = "";
    var search = $('#txtSearchInvoice').val();
    var searchStatus = $('#ddlStatusSearch').val() ? $('#ddlStatusSearch').val() : null;
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            status: searchStatus,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/invoice/GetAllPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Code: item.code,
                        Name: item.name,
                        ContainerName: item.containerName,
                        TotalMoney: item.totalMoney,
                        Status: item.status,
                        StatusName: item.statusName,
                        CreateDate: item.createDate
                    });
                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            $('#tbInvoice').html('');
            if (data.results.length !== 0) {
                wrapPaging(data.rowCount, function () {
                    loadData();
                }, isPageChanged);
                $('#tbInvoice').html(render);
            }

            $('.form-check-input.chkRole').on('change', function (event) {
                if (event.target.checked) {
                    selectedInvoiceId = $(this).siblings('.InvoiceId').val();

                    $('.form-check-input.chkRole').each(function () {
                        if ($(this).siblings('.InvoiceId').val() !== selectedInvoiceId) {
                            $(this).prop('checked', false)
                        }
                    });
                } else {
                    selectedInvoiceId = null;
                }
            });
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
            hastSoft.stopLoading();
        }
    });
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / parseInt($('#ddlShowPage').val()));
    console.log(totalsize)
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        if (recordCount > 0) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}

function DeleteInvoice(id, status) {
    if (status == 0) {
        Swal.fire({
            title: 'Bạn có chắc xóa không?',
            text: "Bạn sẽ không khôi phục lại được!",
            icon: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy'
        }).then((result) => {
            if (result.isConfirmed) {
                if (id !== '') {
                    $.ajax({
                        type: "POST",
                        url: "/Cpanel/Invoice/DeleteInvoice",
                        data: { id: id },
                        dataType: "json",
                        beforeSend: function () {
                            hastSoft.startLoading();
                        },
                        success: function (response) {
                            hastSoft.stopLoading();
                            if (response.statusCode === 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Thành công!',
                                    text: result.message,
                                    confirmButtonText: 'Đóng',
                                }).then((resultAlter) => {
                                    loadData(false);
                                });
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Không thành công!',
                                    text: response.message,
                                    confirmButtonText: 'Đóng',
                                });
                            }
                        },
                        error: function (status) {
                            Swal.fire(
                                'Xóa không thành công!',
                                status.message,
                                'Ok'
                            )
                            hastSoft.stopLoading();
                        }
                    });
                }
                else {
                    Swal.fire(
                        'Xóa không thành công!',
                        'Không tìm thấy danh mục để xóa',
                        'Ok'
                    )
                }
            }
        })
    }
}

function ExportExcel() {
    if (!selectedInvoiceId) {
        Swal.fire({
            icon: 'error',
            title: 'Vui lòng chọn hóa đơn!',
            text: xhr.message,
            confirmButtonText: 'Đóng',
        });
        return;
    }

    let url = "/cpanel/Invoice/ExportInvoice?invoiceId=" + selectedInvoiceId;
    window.location = url;
}