﻿var ciplIdSelected = null;
var selectedCustomer = null;
var isDisableDelete = false;

var url = window.location.href;
var id = getUrlVars(url)['id'];

$(document).ready(function () {
    if (id) {
        loadDataDetail(id);
    } else {
        initTreeDropDownCIPL('');
    }

    $('#tblProducts').on('keyup', '.txtQuantity', function () {
        const poQuantity = parseInt($(this).attr('poQuantity'));
        var quantity = parseInt($(this).val());

        if (quantity > 0 && poQuantity >= quantity) {
            onChangeQuantity($(this), quantity);
        } else {
            $(this).val(poQuantity);
            onChangeQuantity($(this), poQuantity);
        }
    });
});

function onChangeQuantity($this, quantity) {
    var price = $this.parent().parent().find('.txtPrice').val();
    var percent = selectedCustomer.deposit;
    var totalDeposit = ((quantity * price) * percent) / 100;
    var totalMoney = price * quantity;
    $this.parent().parent().find('.txtDeposit').val(totalDeposit);
    $this.parent().parent().find('.txtTotalMoney').val(totalMoney);
    TotalValue();
}

const groupedData = (data) => {
    return data.reduce((result, item) => {
        const key = item.poCode;
        if (!result[key]) {
            result[key] = [];
        }
        result[key].push(item);
        return result;
    }, {});
}

function getCustomer(customerId) {
    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: customerId },
        dataType: 'json',
        async: false,
        success: function (response) {
            selectedCustomer = response;
        }
    });
}

function loadDataDetail(id) {
    if (id !== undefined && id !== 0) {
        $.ajax({
            type: "GET",
            url: "/Cpanel/Invoice/GetById",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: async function (result) {
                initTreeDropDownCIPL(result.invoiceDetails[0].ciplId);
                selectedCIPL = await getCIPL(result.invoiceDetails[0].ciplId);
                ciplIdSelected = selectedCIPL.id;

                const isDisabled = result.status == 1 ? true : false;
                if (!isDisabled) {
                    showApplyPaymentButton(result.invoiceId);
                } else {
                    isDisableDelete = true;
                    $('#apply-payment').hide();
                }

                getCustomer(result.customerId);

                $('#txtInvoiceId').val(result.invoiceId);
                $('#txtInvoiceCode').val(result.code);
                $('#txtInvoiceName').val(result.name);
                $('#dtCreateDate').datepicker("setDate", new Date(result.createDate));

                var items = [];
                const listGroupByPOCode = groupedData(result.invoiceDetails);
                for (let poCode in listGroupByPOCode) {
                    let listInvoiceDetails = result.invoiceDetails.filter(pd => pd.poCode == poCode);

                    items.push(`<tr class="block-${ciplIdSelected}"><th colspan="8">Mã PO: ${poCode}</th></tr>`);
                    $.each(listInvoiceDetails, function (key, val) {
                        const row = templateRowCIPLDetail(val);
                        items.push(row);
                    });
                }
                $("#tblProducts tbody").html(items);

                setReadOnly()

                TotalValue();
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }

    const setReadOnly = () => {
        $('#txtInvoiceCode').prop('disabled', true);
        $('#txtInvoiceName').prop('disabled', true);
        $('#dtCreateDate').prop('disabled', true);
        $('#ddlCIPLs').prop('disabled', true);
    }
}

async function CIPLChange() {
    var ciplId = $(`#ddlCIPLs`).val();
    if (!ciplId) {
        ciplIdSelected = null;
        $("#tblProducts tbody").html('');
        TotalValue();
        return;
    } 

    const cipl = await getCIPL(ciplId);
    if (cipl) {
        ciplIdSelected = cipl.id;

        var items = [];
        const listGroupByPOCode = groupedData(cipl.ciplDetails);
        for (let poCode in listGroupByPOCode) {
            let listciplDetails = cipl.ciplDetails.filter(pd => pd.poCode == poCode);

            items.push(`<tr class="block-${ciplIdSelected}"><th colspan="8">Mã PO: ${poCode}</th></tr>`);
            $.each(listciplDetails, function (key, val) {
                const row = templateRowCIPL(val);
                items.push(row);
            });
        }
        $("#tblProducts tbody").html(items);

        TotalValue();
    }
}

//async function CIPLChange() {
//    var ciplId = $(`#ddlCIPLs`).val();
//    if (!ciplId) {
//        selectedCIPL = null;
//        $("#tblProducts tbody").html("");
//        TotalValue();
//        return;
//    };

//    const cipl = await getCIPL(ciplId);
//    if (cipl) {
//        selectedCIPL = cipl;

//        const listGroupByMOCode = groupedData(cipl.ciplDetails);

//        for (let ciplCode in listGroupByMOCode) {
//            let listPlanDetails = plan.planDetails.filter(pd => pd.planCode == planCode);

//            var items = [];
//            items.push(`<tr class="block-${ciplId}"><th colspan="8">Mã CIPL: ${cipl.code}</th></tr>`);
//            $.each(listPlanDetails, function (key, val) {
//                const row = templateRowCIPL(val);
//                items.push(row);
//            });
//        }
//        $("#tblProducts tbody").html(items);
//        TotalValue();
//    }
//}

function getCIPL(ciplId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/Cpanel/CIPL/GetById",
            data: { Id: ciplId },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                hastSoft.stopLoading();
                resolve(result);
            },
            error: function (err) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
                reject(err);
            }
        });
    });
}

const templateRowCIPL = (data) => {
    let colorId = data?.colorId ?? '';

    return `<tr class="${data.ciplId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control ciplId' value = '${data.ciplId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control ciplDetailId' value = '${data.ciplDetailId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeColor' value = '${data.descCodeColor}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descColorFails' value = '${data.descColorFails}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.quantity}'  value='${data.quantity}' class='form-control txtQuantity' readonly></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${data.money}'></td>
                </tr>`
}

const templateRowCIPLDetail = (data) => {
    let colorId = data?.colorId ?? '';

    return `<tr class="${data.ciplId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control ciplId' value = '${data.ciplId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeColor' value = '${data.descCodeColor}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descColorFails' value = '${data.descColorFails}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control invoiceDetailId' value = '${data.invoiceDetailId}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.quantity}' value='${data.quantity}' class='form-control txtQuantity' readonly></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${data.money}'></td>
            </tr>`
}

function SavePayment() {
    var createDate = $('#dtCreateDate').datepicker("getDate");
    var formattedDate = moment(createDate).format("YYYY-MM-DD");

    var data = {
        InvoiceId: $('#txtInvoiceId').val(),
        Code: $('#txtInvoiceCode').val(),
        Name: $('#txtInvoiceName').val(),
        CIPLId: $('#ddlCIPLs').val(),
        InvoiceDetails: GetDataInTables()
    };

    if ((!data.PlanId || !data.Code || !data.Name || !data.CustomerId) && (id && !data.InvoiceId)) {
        Swal.fire({
            icon: 'warning',
            title: 'Cảnh báo...',
            text: 'Vui lòng nhập đầy đủ thông tin!'
        });
        return;
    }

    $.ajax({
        url: '/cpanel/invoice/SaveInvoice',
        type: 'POST',
        beforeSend: function () {
            hastSoft.startLoading();
        },
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.statusCode === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((result) => {
                    window.location.href = "/cpanel/invoice/invoice?id=" + response.data;
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

$(document).on("click", ".delete-button", function () {
    if (!isDisableDelete) {
        const classTr = $(this).closest("tr")[0].classList.value;
        $(this).closest("tr").remove();

        if ($(`.${classTr}`).length == 0) {
            $(`.block-${classTr}`).remove();

            poIdSelected = poIdSelected.filter(poId => poId != classTr);
        }

        TotalValue();
    }
});

function TotalValue() {
    $("#spanTotalDeposit").val('');
    $("#spanTotalMoney").val('');
    var array = GetDataInTables();
    var totalDeposit = 0;
    var totalMoney = 0;
    $.each(array, function (index, value) {
        totalDeposit += parseFloat(value.Deposit);
        totalMoney += value.TotalMoney;
    });

    $("#spanTotalDeposit").html(totalDeposit + "$");
    $("#spanTotalMoney").html(totalMoney + "$");
}

function GetDataInTables() {
    var productArrays = [];

    $("#tblProducts tbody tr").each(function () {
        var productId = $(this).find('input[class="productId"]').val();
        var itemId = $(this).find('input[class="itemId"]').val();
        var itemName = $(this).find('input[class="itemName"]').val();
        var colorId = $(this).find('input[class="colorId"]').val();
        var colorName = $(this).find('input[class="colorName"]').val();
        var quantity = parseInt($(this).closest('tr').find('.txtQuantity').val());
        var deposit = parseFloat($(this).closest('tr').find('.txtDeposit').val());
        var unitPrice = parseFloat($(this).closest('tr').find('.txtPrice').val());
        var totalMoney = parseFloat($(this).closest('tr').find('.txtTotalMoney').val());
        var descCodeColor = $(this).find('input[class="descCodeColor"]').val();
        var descColorFails = $(this).find('input[class="descColorFails"]').val();
        var ciplId = $(this).find('.ciplId').val();
        var ciplDetailId = $(this).find('.ciplDetailId').val();

        var product = {
            CIPLId: ciplId,
            CIPLDetailId: ciplDetailId,
            DescCodeColor: descCodeColor,
            DescColorFails: descColorFails,
            StrProduct: productId,
            UnitPrice: unitPrice,
            Deposit: deposit,
            Quantity: quantity,
            StrItem: itemId,
            StrItemName: itemName,
            StrColor: colorId,
            StrColorName: colorName,
            TotalMoney: totalMoney
        };

        if (productId !== undefined)
            productArrays.push(product);
    });
    return productArrays;
}

function showApplyPaymentButton(id) {
    $('#apply-payment').append(`<a onclick="ApplyPayment('${id}')" class="btn btn-primary"><i class="uil-plus-circle"></i>&nbsp; Xuất hóa đơn</a>`)
}

function ApplyPayment(invoiceId) {
    $.ajax({
        url: '/cpanel/invoice/ApplyPayment',
        type: 'POST',
        beforeSend: function () {
            hastSoft.startLoading();
        },
        data: {
            invoiceId: invoiceId
        },
        dataType: "json",
        success: function (response) {
            if (response == true) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((result) => {
                    location.reload();
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}



// --------------- LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //
function initTreeDropDownStatus(selectedId) {
    $.ajax({
        url: "/Cpanel/Status/GetStatus",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn trạng thái', 'ddlStatus', selectedId);
        }
    });
}

function initTreeDropDownPlan(selectedId) {
    $.ajax({
        url: "/Cpanel/Plan/GetAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn kế hoạch', 'ddlPlans', selectedId);
        }
    });
}

function initTreeDropDownCIPL(selectedId) {
    $.ajax({
        url: "/Cpanel/CIPL/GetAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn CIPL', 'ddlCIPLs', selectedId);
        }
    });
}
// --------------- KẾT THÚC LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //