﻿
var selectedCustomer = null;
var planIdSelected = null;
let isDisabled = false;

let listPlans = [];

var url = window.location.href;
var id = getUrlVars(url)['id'];

$(document).ready(function () {
    $("#formTopSearch").css("display", "none");

    if (id) {
        loadDataDetail(id);
    } else {
        initTreeDropDownCustmers('');
        initDropDownContainers('');
    }

    $('#tblProducts').on('keyup', '.txtQuantity', function () {
        const poQuantity = parseInt($(this).attr('poQuantity'));
        var quantity = parseInt($(this).val());

        if (quantity > 0 && poQuantity >= quantity) {
            onChangeQuantity($(this), quantity);
        } else {
            $(this).val(poQuantity);
            onChangeQuantity($(this), poQuantity);
        }
    });
});

const groupedData = (data) => {
    return data.reduce((result, item) => {
        const key = item.poCode;
        if (!result[key]) {
            result[key] = [];
        }
        result[key].push(item);
        return result;
    }, {});
}

function onChangeQuantity($this, quantity) {
    var price = $this.parent().parent().find('.txtPrice').val();
    var percent = selectedCustomer.deposit;
    var totalDeposit = ((quantity * price) * percent) / 100;
    var totalMoney = price * quantity;
    $this.parent().parent().find('.txtDeposit').val(totalDeposit);
    $this.parent().parent().find('.txtTotalMoney').val(totalMoney);
    TotalValue();
}

function loadDataDetail(id) {
    if (id !== undefined && id !== 0) {
        $.ajax({
            type: "GET",
            url: "/Cpanel/CIPL/GetById",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                initTreeDropDownCustmers(result.customerId);
                $.ajax({
                    url: "/Cpanel/Customer/GetById",
                    type: 'GET',
                    data: { id: result.customerId },
                    dataType: 'json',
                    async: false,
                    success: function (response) {
                        selectedCustomer = response;
                    }
                });
               // $("#ddlCustomer").change();

                initDropDownContainers(result.containerId);
                $("#ddlContainers").change();

                //initDropDownPlans(result.customerId, result.ciplDetails[0].planId);


                let plan = listPlans.find(p => p.id == result.ciplDetails[0].planId);
                initDropDownPlans(result.customerId, result.ciplDetails[0].planId)


                $('#txtCIPLId').val(result.ciplId);
                $('#txtCIPLCode').val(result.code);
                $('#txtCIPLName').val(result.name);


                isDisabled = result.isHasInvoice;
                const listGroupByPOCode = groupedData(result.ciplDetails);

                for (let poCode in listGroupByPOCode) {
                    let listCIPLDetails = result.ciplDetails.filter(pd => pd.poCode == poCode);
                    let selectedPlanId = listCIPLDetails[0].planId;
                    planIdSelected = selectedPlanId;

                    var items = [];
                    items.push(`<tr class="block-${selectedPlanId}">
                                    <th colspan="8">Mã PO: ${poCode}</th>
                                </tr>`);
                    $.each(listCIPLDetails, function (key, val) {
                        const row = templateRowPlanDetail(val);
                        items.push(row);
                    });
                    $("#tblProducts tbody").append(items);
                }

                setReadOnly();

                TotalValue();
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }

    const setReadOnly = () => {
        $('#ddlCustomer').prop('disabled', true);
        $('#txtCIPLCode').prop('disabled', true);
        $('#txtCIPLName').prop('disabled', true);
        $('#ddlContainers').prop('disabled', true);
        $('#ddlPlans').prop('disabled', true);
    }
}

async function PlanChange() {
    var planId = $(`#ddlPlans`).val();
    if (!planId) return;

    const plan = await getPlanForCIPL(planId);
    console.log(plan)
    if (plan) {
        if (plan.planDetails.length == 0) {
            Swal.fire({
                icon: 'warning',
                title: 'Cảnh báo...',
                text: 'Mã Plan này không còn sản phầm!'
            });
            return;
        }

        planIdSelected = plan.id;

        const listGroupByPOCode = groupedData(plan.planDetails);

        for (let poCode in listGroupByPOCode) {
            let listPlanDetails = plan.planDetails.filter(pd => pd.poCode == poCode);

            var items = [];
            items.push(`<tr class="block-${planIdSelected}">
                            <th colspan="8">Mã PO: ${poCode}</th>
                        </tr>`);
            $.each(listPlanDetails, function (key, val) {
                const row = templateRowPlan(val);
                items.push(row);
            });
            $("#tblProducts tbody").append(items);
        }

        TotalValue();
    }
}

function getPlanForCIPL(planId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/Cpanel/CIPL/GetPlanForCIPL",
            data: { planId: planId },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                hastSoft.stopLoading();
                resolve(result);
            },
            error: function (err) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
                reject(err);
            }
        });
    });
}

const templateRowPlan = (data) => {
    let colorId = data?.colorId ?? "";

    return `<tr class="${data.planId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control planId' value = '${data.planId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control planDetailId' value = '${data.planDetailId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeColor' value = '${data.descCodeColor}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descColorFails' value = '${data.descColorFails}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.remainQuantity}' value='${data.quantity}' class='form-control txtQuantity'></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${data.money}'></td>
            </tr>`
}

const templateRowPlanDetail = (data) => {
    let colorId = data?.colorId ?? '';
    let readOnly = isDisabled ? 'readonly' : '';

    return `<tr class="${data.planId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control planId' value='${data.planId}' style='width: 120px;'>
                    <input type='hidden' class='form-control descCodeColor' value = '${data.descCodeColor}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeFails' value = '${data.descCodeFails}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control ciplDetailId' value = '${data.ciplDetailId}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.remainQuantity}' value='${data.quantity}' class='form-control txtQuantity' ${readOnly}></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${data.money}'></td>
                </tr>`
}

function GetCustomerData(cusId) {
    return new Promise((resolve) => {
        $.ajax({
            url: "/Cpanel/Customer/GetById",
            type: 'GET',
            data: { id: cusId },
            dataType: 'json',
            async: false,
            success: function (response) {
                resolve(response);
            }
        });
    });
}

function SaveCIPL() {
    if (id && planIdSelected == null) {
        Swal.fire({
            icon: 'warning',
            title: 'Cảnh báo...',
            text: 'Vui lòng chọn kế hoạch!'
        });
        return;
    }

    var data = {
        CIPLId: $('#txtCIPLId').val(),
        Code: $('#txtCIPLCode').val(),
        Name: $('#txtCIPLName').val(),
        ContainerId: $('#ddlContainers').val(),
        CustomerId: selectedCustomer.id,
        CIPLDetails: GetDataInTables()
    };

    if ((!data.Code || !data.Name || !data.CustomerId || !data.ContainerId) || (id && !data.CIPLId)) {
        Swal.fire({
            icon: 'warning',
            title: 'Cảnh báo...',
            text: 'Vui lòng nhập đầy đủ thông tin!'
        });
        return;
    }

    $.ajax({
        url: '/cpanel/CIPL/SaveCIPL',
        type: 'POST',
        beforeSend: function () {
            hastSoft.startLoading();
        },
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.statusCode === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((result) => {
                    window.location.href = "/cpanel/CIPL/CIPL?id=" + response.data;
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

function getListPlanByCustomerId(customerId) {
    return new Promise((resolve) => {
        $.ajax({
            url: "/Cpanel/Plan/GetListPlanByCustomerId",
            type: 'GET',
            data: { customerId: customerId },
            dataType: 'json',
            async: false,
            success: function (response) {
                resolve(response);
            }
        });
    })
}

$("#ddlCustomer").change(async function () {
    const customerId = $("#ddlCustomer").val();
    listPlans = await getListPlanByCustomerId(customerId);
    DropDownListSelected(listPlans, 'Chọn kế hoạch', 'ddlPlans', '');

    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: customerId },
        dataType: 'json',
        async: false,
        success: function (response) {
            selectedCustomer = response;
            //$("#txtCustomerDeposit").val(response.deposit);
            console.log(selectedCustomer);
        }
    });
});

function GetDataInTables() {
    var productArrays = [];

    $("#tblProducts tbody tr").each(function () {
        var productId = $(this).find('input[class="productId"]').val();
        var descCodeColor = $(this).find('input[class="descCodeColor"]').val();
        var descColorFails = $(this).find('input[class="descColorFails"]').val();
        var itemId = $(this).find('input[class="itemId"]').val();
        var itemName = $(this).find('input[class="itemName"]').val();
        var colorId = $(this).find('input[class="colorId"]').val();
        var colorName = $(this).find('input[class="colorName"]').val();
        var quantity = parseInt($(this).closest('tr').find('.txtQuantity').val());
        var deposit = parseFloat($(this).closest('tr').find('.txtDeposit').val());
        var unitPrice = parseFloat($(this).closest('tr').find('.txtPrice').val());
        var totalMoney = parseFloat($(this).closest('tr').find('.txtTotalMoney').val());
        var poDetailId = $(this).find('.poDetailId').val();
        var planId = $(this).find('.planId').val();
        var planDetailId = $(this).find('.planDetailId').val();

        var product = {
            PlanId: planId,
            PlanDetailId: planDetailId,
            DescCodeColor: descCodeColor,
            DescColorFails: descColorFails,
            StrProduct: productId,
            UnitPrice: unitPrice,
            Deposit: deposit,
            Quantity: quantity,
            StrItem: itemId,
            StrItemName: itemName,
            StrColor: colorId,
            StrColorName: colorName,
            TotalMoney: totalMoney
        };

        if (productId !== undefined)
            productArrays.push(product);
    });
    return productArrays;
}

function TotalValue() {
    $("#spanTotalDeposit").val('');
    $("#spanTotalMoney").val('');
    var array = GetDataInTables();
    var totalDeposit = 0;
    var totalMoney = 0;
    $.each(array, function (index, value) {
        totalDeposit += parseFloat(value.Deposit);
        totalMoney += value.TotalMoney;
    });

    $("#spanTotalDeposit").html(totalDeposit + "$");
    $("#spanTotalMoney").html(totalMoney + "$");
}

//$(document).on("click", ".delete-button", function () {
//    const classTr = $(this).closest("tr")[0].classList.value;
//    $(this).closest("tr").remove();

//    const classPlan = classTr.split(/-(.*)/s)[1];
//    $(`.${classPlan}`).remove();
//    TotalValue();
//});


// --------------- LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //
function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomer', selectedId);
        }
    });
}

function initDropDownContainers(selectedId) {
    $.ajax({
        url: "/Cpanel/Container/GetContainers",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn Container', 'ddlContainers', selectedId);
        }
    });
}

function initDropDownPlans(customerId, planSelectedId) {
    $.ajax({
        url: "/Cpanel/Plan/GetListPlanByCustomerId",
        data: { customerId: customerId },
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            $('#ddlPlans').empty();
            console.log(customerId)
            console.log(planSelectedId)
            DropDownListSelected(response, 'Chọn kế hoạch', 'ddlPlans', planSelectedId);
        }
    });
}

// --------------- KẾT THÚC LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //