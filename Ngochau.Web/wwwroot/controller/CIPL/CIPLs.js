﻿var selectedCIPLId = null;

$(document).ready(function () {
    loadData(false);
});

$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});

function OnClickSearch() {
    loadData(true);
}

function loadData(isPageChanged) {
    hastSoft.startLoading();
    var template = $('#table-template-cipl').html();
    var render = "";
    var search = $('#txtSearchCIPL').val();
    var searchStatus = $('#ddlStatusSearch').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            //status: searchStatus,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/CIPL/GetAllPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Code: item.code,
                        Name: item.name,
                        Container: item.container,
                        CustomerName: item.customerName,
                        TotalDeposit: item.totalDeposit,
                        TotalMoney: item.totalMoney,
                        CreateDate: item.createDate
                    });
                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            $('#tbCIPL').html('');
            if (data.results.length !== 0) {
                wrapPaging(data.rowCount, function () {
                    loadData();
                }, isPageChanged);
                $('#tbCIPL').html(render);
            }

            $('.form-check-input.chkRole').on('change', function (event) {
                if (event.target.checked) {
                    selectedCIPLId = $(this).siblings('.CIPLId').val();

                    $('.form-check-input.chkRole').each(function () {
                        if ($(this).siblings('.CIPLId').val() !== selectedCIPLId) {
                            $(this).prop('checked', false)
                        }
                    });
                } else {
                    selectedCIPLId = null;
                }
            });
            hastSoft.stopLoading();
        },
        error: function (status) {
            hastSoft.notify('Cannot loading data', 'error');
            hastSoft.stopLoading();
        }
    });
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / parseInt($('#ddlShowPage').val()));
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        if (recordCount > 0) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}

function DeleteCIPL(ciplId) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (ciplId !== '') {
                $.ajax({
                    type: "POST",
                    url: "/Cpanel/CIPL/DeleteCIPL",
                    data: { ciplId: ciplId },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        hastSoft.stopLoading();
                        if (response.resultType == null) {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Cảnh báo!',
                                text: "Không thể xóa vì đang tồn tại ở Invoice!",
                                confirmButtonText: 'Đóng',
                            });
                        } else {
                            if (response.resultType === 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Thành công!',
                                    text: result.message,
                                    confirmButtonText: 'Đóng',
                                }).then((resultAlter) => {
                                    loadData(false);
                                });
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Không thành công!',
                                    text: response.message,
                                    confirmButtonText: 'Đóng',
                                });
                            }
                        }
                    },
                    error: function (status) {
                        Swal.fire(
                            'Xóa không thành công!',
                            status.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    });
}

function ExportExcel() {
    if (!selectedCIPLId) {
        Swal.fire({
            icon: 'error',
            title: 'Vui lòng chọn CIPL!',
            confirmButtonText: 'Đóng',
        });
        return;
    }

    let url = "/cpanel/CIPL/ExportCIPL?ciplId=" + selectedCIPLId;
    console.log(url);
    window.location = url;
}
