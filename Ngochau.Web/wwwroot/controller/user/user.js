﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    loadRoles();
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    loadUser(id);
    handleButtonSave();
});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function loadUser(id) {
    if (id !== undefined && id !== 0) {
        $("#devPassword").css("display", "none");
        $.ajax({
            type: "GET",
            url: "/Cpanel/User/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (data) {
                $('#txtId').val(data.id);
                $('#txtFullName').val(data.fullName);
                $('#txtUserName').val(data.userName);
                $('#txtAddress').val(data.address);
                $('#txtHiddenImageUrl').val(data.avatar);
                $('#chkActive').prop('checked', data.status === 1 ? true : false);
                // Hiển thị check trong bảng role
                $.each($('#btlRoles tbody tr'), function (i, item) {
                    debugger;
                    var idRole = $(this).find('.codeRole').html();
                    $.each(data.roles, function (j, jitem) {
                        if (idRole === jitem) {
                            $(item).find('.chkRole').first().prop('checked', true);
                            CheckRole();
                        }
                    });
                });
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}


function Save(type) {
    debugger;
    var roles = [];
    var form = $("#frmUser")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    }
    else {
        var txtId = $("#txtId").val();
        var txtFullName = $("#txtFullName").val();
        var txtUserName = $("#txtUserName").val();
        var txtPassword = $("#txtPassword").val();
        var fileImage = $("#txtHiddenImageUrl").val();
        var txtAddress = $("#txtAddress").val();
        var chkActive = $("#chkActive").is(":checked") === true ? 1 : 0;
        $.ajax({
            type: "POST",
            url: "/Cpanel/user/SaveEntity",
            data: {
                Id: txtId,
                FullName: txtFullName,
                UserName: txtUserName,
                Email: txtUserName,
                Password: txtPassword,
                Avatar: fileImage,
                Roles: getRoleIds(roles),
                Status: chkActive,
                Address: txtAddress
            },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/user/index?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/user/index";
                        if (type === 2)
                            window.location.href = "/cpanel/user/users";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (response) {
                Swal.fire({
                    icon: 'error',  
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

function loadRoles() {
    var template = $('#table-template-roles').html();
    var render = "";
    var search = null;//$('#txtKeyword').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/role/GetAll',
        dataType: 'json',
        success: function (data) {
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    if (item.id !== 0) {
                        render += Mustache.render(template, {
                            Id: item.id,
                            Code: item.code,
                            Description: item.description
                        });

                    }
                });
                $('#btlBodyRoles').html(render);
            }
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
        }
    });
}

$("#fileImage").on('change', function () {
    var fileUpload = $(this).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "/Cpanel/Upload/UploadAvatarUser",
        contentType: false,
        processData: false,
        data: data,
        success: function (path) {
            $('#txtHiddenImageUrl').val(path);
            hastSoft.notify('Upload image succesful!', 'success');

        },
        error: function () {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
});


$('#chkParentRole').on('click', function () {
    $('.chkRole').prop('checked', $(this).prop('checked'));
});

function CheckRole() {
    var res = 0;
    var totalRow = $('#btlRoles tbody tr').length;
    $('#btlRoles tbody tr').each(function () {
        var ichkRole = $(this).find('.chkRole').is(":checked");
        if (ichkRole === true) {
            res++;
        }
    });
    if (res === totalRow) {
        $("#chkParentRole").prop("checked", true);
    }
    else {
        $("#chkParentRole").prop("checked", false);
    }
}

function getRoleIds(lstRoleIds) {
    $('#btlRoles tbody tr').each(function () {
        var ichkRole = $(this).find('.chkRole').is(":checked");
        var idRole = $(this).find('.codeRole').html();
        if (ichkRole === true) {
            lstRoleIds.push(idRole);
        }
    });
    return lstRoleIds;
}
