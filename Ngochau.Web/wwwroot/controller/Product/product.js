﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    registerControls();
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    loadData(id);
    handleButtonSave();

});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function registerControls() {
    CKEDITOR.replace('txtContent', {});

    //Fix: cannot click on element ck in modal
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
        $(document)
            .off('focusin.bs.modal') // guard against infinite focus loop
            .on('focusin.bs.modal', $.proxy(function (e) {
                if (
                    this.$element[0] !== e.target && !this.$element.has(e.target).length
                    // CKEditor compatibility fix start.
                    && !$(e.target).closest('.cke_dialog, .cke').length
                    // CKEditor compatibility fix end.
                ) {
                    this.$element.trigger('focus');
                }
            }, this));
    };
}


function loadData(id) {
    if (id !== undefined && id !== 0) {
        $.ajax({
            type: "GET",
            url: "/Cpanel/Product/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (data) {
                $('#txtId').val(data.id);
                $('#txtName').val(data.name);
                $('#txtSKU').val(data.sku);
                $('#txtBarCode').val(data.barCode);
                $('#txtDescShort').val(data.description);
                $('#txtHiddenImageUrl').val(data.image);
                CKEDITOR.instances.txtContent.setData(data.content);
                $('#chkActive').prop('checked', data.status === 1 ? true : false);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}

$("#fileImage").on('change', function () {
    var fileUpload = $(this).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "/Cpanel/Upload/UploadProduct",
        contentType: false,
        processData: false,
        data: data,
        success: function (path) {
            $('#txtHiddenImageUrl').val(path);
            hastSoft.notify('Upload image succesful!', 'success');

        },
        error: function () {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
});


function Save(type) {
    var form = $("#frmProduct")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    } else {
        event.preventDefault();

        var id = $('#txtId').val();
        var name = $('#txtName').val();
        var sku = $('#txtSKU').val();
        var barcode = $('#txtBarCode').val();
        var description = $('#txtDescShort').val();
        var content = CKEDITOR.instances.txtContent.getData();
        var image = $('#txtHiddenImageUrl').val();
        var order = parseInt($('#txtOrder').val());
        var chkIsActive = $("#chkActive").is(":checked") === true ? 1 : 0;

        $.ajax({
            url: '/cpanel/Product/SaveEntity',
            type: 'POST',
            beforeSend: function (request) {
                hastSoft.startLoading();
            },
            data: {
                Id: id,
                Name: name,
                Description: description,
                SKU: sku,
                Content: content,
                BarCode: barcode,
                SortOrder: order,
                Image: image,
                Status: chkIsActive
            },
            dataType: "json",
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/product/product?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/product/product";
                        if (type === 2)
                            window.location.href = "/cpanel/product/index";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (xhr) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: xhr.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

var productController = function () {
    this.initialize = function () {
        registerControls();
        registerEvents();
        resetFormMaintainance();
        var url = window.location.href;
        var id = getUrlVars(url)['id'];
        loadData(id);
    }

    function registerControls() {
        CKEDITOR.replace('txtContent', {});

        //Fix: cannot click on element ck in modal
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            $(document)
                .off('focusin.bs.modal') // guard against infinite focus loop
                .on('focusin.bs.modal', $.proxy(function (e) {
                    if (
                        this.$element[0] !== e.target && !this.$element.has(e.target).length
                        // CKEditor compatibility fix start.
                        && !$(e.target).closest('.cke_dialog, .cke').length
                        // CKEditor compatibility fix end.
                    ) {
                        this.$element.trigger('focus');
                    }
                }, this));
        };
    }
    
    

    function getUrlVars(url) {
        var vars = {};
        var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }
}