﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    loadData(id);
    handleButtonSave();
});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function Save(type) {
    var form = $("#frm")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    }
    else {
        var txtId = $("#txtId").val();
        var txtCode = $("#txtCode").val();
        var txtName = $("#txtName").val();
        var chkIsActive = $("#chkActive").is(":checked") === true ? 1 : 0;
        $.ajax({
            type: "POST",
            url: "/Cpanel/color/SaveEntity",
            data: {
                Id: txtId,
                Code: txtCode,
                Name: txtName,
                IsActive: chkIsActive
            },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/color/color?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/color/color";
                        if (type === 2)
                            window.location.href = "/cpanel/color/index";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (response) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

function loadData(id) {
    if (id !== undefined && id !== 0) {
        document.getElementById("txtCode").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/color/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                $("#txtId").val(result.id);
                $("#txtCode").val(result.code);
                $("#txtName").val(result.name);
                $('#chkActive').prop('checked', result.isActive === 1 ? true : false);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}