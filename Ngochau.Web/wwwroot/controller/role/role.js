﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    $("#btnAssignPermission").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    loadData(id);
    handleButtonSave();
});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function AssignPermission() {
    window.location.href = "/cpanel/permission/permission?roleId=" + $('#txtId').val();
}

function Save(type) {

    var form = $("#frmFunction")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    } else {
        event.preventDefault();
        var data = {
            "Id": $('#txtId').val(),
            "Code": $('#txtCode').val(),
            "Name": $('#txtName').val()
        }

        $.ajax({
            type: "POST",
            url: "/Cpanel/role/SaveEntity",
            data: data,
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                if (result.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: result.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/role/role?id=" + result.data;
                        if (type === 1)
                            window.location.href = "/cpanel/role/role";
                        if (type === 2)
                            window.location.href = "/cpanel/role/roles";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: result.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (response) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: result.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

function loadData(id) {
    if (id !== undefined && id !== 0) {
        document.getElementById("txtCode").readOnly = true;
        $("#btnAssignPermission").css("display", "block");
        $.ajax({
            type: "GET",
            url: "/Cpanel/Role/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (data) {
                $('#txtId').val(data.id);
                $('#txtCode').val(data.code);
                $('#txtName').val(data.description);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}