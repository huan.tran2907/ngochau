﻿$(document).ready(function () {
    loadData();
});
$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});
function loadData(isPageChanged) {
    var template = $('#table-template').html();
    var render = "";
    var search = $('#txtKeyword').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/role/GetAllPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Code: item.code,
                        Description: item.description
                    });

                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            wrapPaging(data.rowCount, function () {
                loadData();
            }, isPageChanged);
            $('#tblRoles').html(render);
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
        }
    });
}

function TopSearch() {
    loadData();
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / hastSoft.configs.pageSize);
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        $('#paginationUL').empty();
        $('#paginationUL').removeData("twbs-pagination");
        $('#paginationUL').unbind("page");
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}
function updateRole(id) {
    window.location.href = "/cpanel/role/role?id=" + id;
}
function deleteRole(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/cpanel/role/Delete",
                    data: { id: id },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        if (response.statusCode === 200) {
                            hastSoft.stopLoading();
                            Swal.fire({
                                icon: 'success',
                                title: 'Thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            }).then((resultAlter) => {
                                loadData(false);
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Không thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            });
                            hastSoft.stopLoading();
                        }
                    },
                    error: function (response) {
                        Swal.fire(
                            'Xóa không thành công!',
                            response.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    })
}

