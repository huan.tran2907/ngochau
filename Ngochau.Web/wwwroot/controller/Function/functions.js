﻿$(document).ready(function () {
    loadData();
});
function TopSearch() {
    loadData();
}
function loadData() {
    var strUrl = "/cpanel/Function/GetAll";
    return $.ajax({
        type: "GET",
        url: strUrl,
        dataType: "json",
        beforeSend: function () {
            hastSoft.startLoading();
        },
        success: function (response) {
            let stt = 0;
            var template = $('#result-data-function').html();
            var render = "";
            $.each(response.data, function (i, item) {
                render += Mustache.render(template, {
                    STT: "",
                    treegridparent: item.parentId !== null ? "treegrid-parent-" + item.parentId : "",
                    Id: item.id,
                    Code: item.code,
                    Name: item.name,
                    Url: item.url,
                    IconCss: item.iconCss,
                    SortOrder: item.sortOrder,
                    Status: hastSoft.getStatus(item.status)
                });
            });
            $('#tblFunctions').html(render);
            $('.tree').treegrid();
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
        }
    });
}

function Update(id) {
    window.location.href = "/cpanel/function/function?id=" + id;
}

function Delete(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/cpanel/function/delete",
                    data: { id: id },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        if (response.statusCode === 200) {
                            hastSoft.stopLoading();
                            Swal.fire({
                                icon: 'success',
                                title: 'Thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            }).then((resultAlter) => {
                                loadData();
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Không thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            });
                            hastSoft.stopLoading();
                        }
                    },
                    error: function (response) {
                        Swal.fire(
                            'Xóa không thành công!',
                            response.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    })
}
