﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    initTreeDropDownFunctions('');
    loadData(id);
    handleButtonSave();
});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function initTreeDropDownFunctions(selectedId) {
    $.ajax({
        url: "/Cpanel/Function/GetAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response.data, 'Chọn chức năng', 'ddlFunction', selectedId);
        }
    });
}

function Save(type) {
    var form = $("#frmFunction")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    } else {
        event.preventDefault();
        var data = {
            "Id": $('#txtId').val(),
            "Code": $('#txtCode').val(),
            "Name": $('#txtName').val(),
            "URL": $('#txtUrl').val(),
            "ParentId": $('#ddlFunction').val(),
            "IconCss": $('#txtIconCss').val(),
            "SortOrder": $("#txtSortOrder").val(),
            "Status": $("#chkActive").is(":checked") === true ? 1 : 0
        }
        $.ajax({
            url: '/cpanel/function/SaveEntity',
            type: 'POST',
            beforeSend: function (request) {
                hastSoft.startLoading();
            },
            data: data,
            dataType: "json",
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/function/function?id=" + result.data;
                        if (type === 1)
                            window.location.href = "/cpanel/function/function";
                        if (type === 2)
                            window.location.href = "/cpanel/function/functions";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (xhr) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: xhr.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

function loadData(id) {
    if (id !== undefined && id !== 0) {
        //document.getElementById("ddlFunction").prop('disabled', true);
        $('#ddlFunction').attr('disabled', true);
        document.getElementById("txtCode").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/Function/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                initTreeDropDownFunctions(result.data.parentId);
                $('#txtId').val(result.data.id);
                $('#txtCode').val(result.data.code);
                $('#txtName').val(result.data.name);
                $('#txtIconCss').val(result.data.iconCss);
                $('#txtSortOrder').val(result.data.sortOrder);
                $('#txtUrl').val(result.data.url);
                $('#chkActive').prop('checked', result.data.status === 1 ? true : false);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}