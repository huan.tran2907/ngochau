﻿
var selectedCustomer = null;
var poIdSelected = [];
let isDisableDelete = false;

var url = window.location.href;
var id = getUrlVars(url)['id'];

$(document).ready(function () {
    $("#formTopSearch").css("display", "none");

    if (id) {
        loadDataDetail(id);
    } else {
        initTreeDropDownCustmers('');
        //initDropDownContainers('');
    }

    $('#tblProducts').on('keyup', '.txtQuantity', function () {
        const poQuantity = parseInt($(this).attr('poQuantity'));
        var quantity = parseInt($(this).val());

        if (quantity > 0 && poQuantity >= quantity) {
            onChangeQuantity($(this), quantity);
        } else {
            $(this).val(poQuantity);
            onChangeQuantity($(this), poQuantity);
        }
    });
});

function onChangeQuantity($this, quantity) {
    var price = $this.parent().parent().find('.txtPrice').val();
    var percent = selectedCustomer.deposit;
    var totalDeposit = ((quantity * price) * percent) / 100;
    var totalMoney = price * quantity;
    $this.parent().parent().find('.txtDeposit').val(totalDeposit);
    $this.parent().parent().find('.txtTotalMoney').val(totalMoney);
    TotalValue();
}


function loadDataDetail(id) {
    if (id !== undefined && id !== 0) {
        //document.getElementById("txtPO").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/Plan/GetById",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                if (result.status == 1) {
                    isDisableDelete = true;
                    $('#apply-payment').hide();
                } 

                initTreeDropDownCustmers(result.customerId);
                $("#ddlCustomer").change();

                $('#txtPlanId').val(result.planId);
                $('#txtPlanCode').val(result.code);
                $('#txtPlanName').val(result.name);
                //$('#dtPlanDate').datepicker("setDate", new Date(result.planDate));

                const listGroupByPOCode = groupedData(result.planDetails);

                for (let poCode in listGroupByPOCode) {
                    let listPlanDetails = result.planDetails.filter(pd => pd.poCode == poCode);
                    let selectedPoId = listPlanDetails[0].poId;
                    poIdSelected.push(selectedPoId);

                    var items = [];
                    items.push(`<tr class="block-${selectedPoId}"><th colspan="8">Mã PO: ${poCode}</th></tr>`);
                    $.each(listPlanDetails, function (key, val) {
                        const row = templateRowPODetail(val);
                        items.push(row);
                    });
                    $("#tblProducts tbody").append(items);
                }

                setReadOnly();

                TotalValue();
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }

    const groupedData = (data) => {
        return data.reduce((result, item) => {
            const key = item.poCode;
            if (!result[key]) {
                result[key] = [];
            }
            result[key].push(item);
            return result;
        }, {});
    }

    const setReadOnly = () => {
        $('#ddlCustomer').prop('disabled', true);
        $('#txtPlanCode').prop('disabled', true);
        $('#txtPlanName').prop('disabled', true);
        $('#ddlContainers').prop('disabled', true);
        $('#ddlPOs').prop('disabled', true);
    }
}


async function POChange() {
    var poId = $(`#ddlPOs`).val();
    if (!poId) return;

    const po = await getPOForPlan(poId);
    console.log(po);
    if (po) {
        $("#ddlPOs").val("").trigger("change");
        if (poIdSelected.find(poId => poId == po.poId)) {
            Swal.fire({
                icon: 'warning',
                title: 'Cảnh báo...',
                text: 'Mã PO này đã được thêm trước đó!'
            });
            return;
        }

        if (po.poDetails.length == 0) {
            Swal.fire({
                icon: 'warning',
                title: 'Cảnh báo...',
                text: 'Mã PO này không còn sản phầm!'
            });
            return;
        }

        poIdSelected.push(po.poId);

        var items = [];
        items.push(`<tr class="block-${poId}"><th colspan="8">Mã PO: ${po.code}</th></tr>`);
        $.each(po.poDetails, function (key, val) {
            const row = templateRowPO(val);
            items.push(row);
        });
        $("#tblProducts tbody").append(items);

        TotalValue();
    }
}

function getPOForPlan(poId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/Cpanel/Plan/GetPOForPlan",
            data: { poId: poId },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                hastSoft.stopLoading();
                resolve(result);
            },
            error: function (err) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
                reject(err);
            }
        });
    });
}

const templateRowPO = (data) => {
    let colorId = data?.colorId ?? "";

    let totalMoney = data.quantity * data.unitPrice;

    return `<tr class="${data.poId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control poId' value = '${data.poId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control poDetailId' value = '${data.poDetailId}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.remainQuantity}' value='${data.remainQuantity}' class='form-control txtQuantity'></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${totalMoney}'></td>
                <td style='text-align: start'><a class='action-icon delete-button'> <i class='mdi mdi-delete'></i></a></td>
                </tr>`
}

const templateRowPODetail = (data) => {
    let colorId = data?.colorId ?? '';
    let totalMoney = data.quantity * data.unitPrice;

    return `<tr class="${data.poId}">
                <td style='text-align: start'> ${data.productCode}-${data.productName} <input type='hidden' class='productId' value='${data.productId}'>
                    <input type='hidden' class='form-control poId' value = '${data.poId}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeColor' value = '${data.descCodeColor}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control descCodeFails' value = '${data.descCodeFails}' style = 'width: 120px;'>
                    <input type='hidden' class='form-control planDetailId' value = '${data.planDetailId}' style = 'width: 120px;'>
                </td>
                <td style='text-align: start'> ${data.itemCode}-${data.itemName} 
                    <input type='hidden' class='itemId' value='${data.itemId}'>
                    <input type='hidden' class='itemName' value='${data.itemName}'>
                </td>
                <td style='text-align: start'> ${data.colorCode}-${data.colorName} 
                    <input type='hidden' class='colorId' value='${colorId}'>
                    <input type='hidden' class='colorName' value='${data.colorName}'>
                </td>
                <td style='width: 120px'><input style='text-align: start' type='text' id='txtQuantity' poQuantity='${data.remainQuantity}' value='${data.quantity}' class='form-control txtQuantity' ></td>
                <td style='text-align: start'><input type='text' class='form-control txtPrice' readonly value='${data.unitPrice}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtDeposit' readonly value='${data.deposit}'></td>
                <td style='text-align: start'><input type='text' class='form-control txtTotalMoney' readonly value='${totalMoney}'></td>
                <td style='text-align: start'><a class='action-icon delete-button'> <i class='mdi mdi-delete'></i></a></td>
                </tr>`
}

function GetCustomerData(cusId) {
    return new Promise((resolve) => {
        $.ajax({
            url: "/Cpanel/Customer/GetById",
            type: 'GET',
            data: { id: cusId },
            dataType: 'json',
            async: false,
            success: function (response) {
                resolve(response);
            }
        });
    });
}

function SavePlan() {
    //var planDate = $('#dtPlanDate').datepicker("getDate");
    //var formattedPlanDate = moment(planDate).format("YYYY-MM-DD");

    //var dueDate = $('#dtDueDate').datepicker("getDate");
    //var formattedDueDate = moment(dueDate).format("YYYY-MM-DD");

    var data = {
        PlanId: $('#txtPlanId').val(),
        Code: $('#txtPlanCode').val(),
        Name: $('#txtPlanName').val(),
        //PlanDate: formattedPlanDate,
        //DueDate: formattedDueDate,
        CustomerId: selectedCustomer.id,
        PlanDetails: GetDataInTables()
    };

    if ((!data.Code || !data.Name || !data.CustomerId) || (id && !data.PlanId)) {
        Swal.fire({
            icon: 'warning',
            title: 'Cảnh báo...',
            text: 'Vui lòng nhập đầy đủ thông tin!'
        });
        return;
    } 

    $.ajax({
        url: '/cpanel/plan/SavePlan',
        type: 'POST',
        beforeSend: function () {
            hastSoft.startLoading();
        },
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.statusCode === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((result) => {
                    window.location.href = "/cpanel/plan/plan?id=" + response.data;
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

function getListPOByCustomerId(customerId) {
    return new Promise((resolve) => {
        $.ajax({
            url: "/Cpanel/MasterOrder/GetListPOByCustomerId",
            type: 'GET',
            data: { customerId: customerId },
            dataType: 'json',
            async: false,
            success: function (response) {
                //console.log(response);
                resolve(response);
            }
        });
    })
}

$("#ddlCustomer").change(async function () {
    const customerId = $("#ddlCustomer").val();
    const listPOs = await getListPOByCustomerId(customerId);
    console.log(listPOs);
    DropDownListSelected(listPOs, 'Chọn đơn hàng', 'ddlPOs', '');

    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: customerId },
        dataType: 'json',
        async: false,
        success: function (response) {
            selectedCustomer = response;
            //$("#txtCustomerDeposit").val(response.deposit);
            console.log(selectedCustomer);
        }
    });
});

$(document).on("click", ".delete-button", function () {
    if (!isDisableDelete) {
        const classTr = $(this).closest("tr")[0].classList.value;
        $(this).closest("tr").remove();

        if ($(`.${classTr}`).length == 0) {
            $(`.block-${classTr}`).remove();

            poIdSelected = poIdSelected.filter(poId => poId != classTr);
        }

        TotalValue();
    }
});

function TotalValue() {
    $("#spanTotalDeposit").val('');
    $("#spanTotalMoney").val('');
    var array = GetDataInTables();
    var totalDeposit = 0;
    var totalMoney = 0;
    $.each(array, function (index, value) {
        totalDeposit += parseFloat(value.Deposit);
        totalMoney += value.TotalMoney;
    });

    $("#spanTotalDeposit").html(totalDeposit + "$");
    $("#spanTotalMoney").html(totalMoney + "$");
}

function GetDataInTables() {
    var productArrays = [];

    $("#tblProducts tbody tr").each(function () {
        var productId = $(this).find('input[class="productId"]').val();
        var descCodeColor = $(this).find('input[class="descCodeColor"]').val();
        var descColorFails = $(this).find('input[class="descColorFails"]').val();
        var itemId = $(this).find('input[class="itemId"]').val();
        var itemName = $(this).find('input[class="itemName"]').val();
        var colorId = $(this).find('input[class="colorId"]').val();
        var colorName = $(this).find('input[class="colorName"]').val();
        var quantity = parseInt($(this).closest('tr').find('.txtQuantity').val());
        var deposit = parseFloat($(this).closest('tr').find('.txtDeposit').val());
        var unitPrice = parseFloat($(this).closest('tr').find('.txtPrice').val());
        var totalMoney = parseFloat($(this).closest('tr').find('.txtTotalMoney').val());
        var poDetailId = $(this).find('.poDetailId').val();
        var poId = $(this).find('.poId').val();
        var planDetailId = $(this).find('.planDetailId').val();

        var product = {
            PlanDetailId: planDetailId,
            POId: poId,
            PODetailId: poDetailId,
            DescCodeColor: descCodeColor,
            DescColorFails: descColorFails,
            StrProduct: productId,
            UnitPrice: unitPrice,
            Deposit: deposit,
            Quantity: quantity,
            StrItem: itemId,
            StrItemName: itemName,
            StrColor: colorId,
            StrColorName: colorName,
            TotalMoney: totalMoney
        };

        if (productId !== undefined)
            productArrays.push(product);
    });
    return productArrays;
}


//function ParseText() {
//    var percent = parseInt($("#txtCustomerDeposit").val());
//    $("#tblProducts tbody tr").each(function () {
//        var quantity = parseInt($(this).closest('tr').find('.txtQuantity').val());
//        var price = parseFloat($(this).closest('tr').find('.txtPrice').val());
//        if (price > 0 && quantity > 0 && percent > 0) {
//            var money = (price * quantity);
//            var deposit = (money * percent) / 100;
//            $(this).closest('tr').find('.txtDeposit').val(deposit);
//            $(this).closest('tr').find('.txtTotalMoney').val(money);
//        }
//    });
//    TotalValue();
//}

$("#ddlContainers").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtContainerCode").val(arr[0].trim());
});

// --------------- LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //
function initTreeDropDownStatus(selectedId) {
    $.ajax({
        url: "/Cpanel/Status/GetStatus",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn trạng thái', 'ddlStatus', selectedId);
        }
    });
}

function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomer', selectedId);
        }
    });
}

function initDropDownContainers(selectedId) {
    $.ajax({
        url: "/Cpanel/Container/GetContainers",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn Container', 'ddlContainers', selectedId);
        }
    });
}

// --------------- KẾT THÚC LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //