﻿var selectedPlanId = null;

$(document).ready(function () {
    loadData(false);
});

$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});

function OnClickSearch() {
    loadData(true);
}

function loadData(isPageChanged) {
    hastSoft.startLoading();
    var template = $('#table-template-plan').html();
    var render = "";
    var search = $('#txtSearchPlan').val();
    var searchStatus = $('#ddlStatusSearch').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            status: searchStatus,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/plan/GetAllPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Code: item.code,
                        Name: item.name,
                        TotalMoney: item.totalMoney,
                        CreateDate: item.createDate
                    });
                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            $('#tbPlan').html('');
            if (data.results.length !== 0) {
                wrapPaging(data.rowCount, function () {
                    loadData();
                }, isPageChanged);
                $('#tbPlan').html(render);
            }

            $('.form-check-input.chkRole').on('change', function (event) {
                if (event.target.checked) {
                    selectedPlanId = $(this).siblings('.PlanId').val();

                    $('.form-check-input.chkRole').each(function () {
                        if ($(this).siblings('.PlanId').val() !== selectedPlanId) {
                            $(this).prop('checked', false)
                        }
                    });
                } else {
                    selectedPlanId = null;
                }
            });
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
            hastSoft.stopLoading();
        }
    });
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / parseInt($('#ddlShowPage').val()));
    console.log(totalsize)
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        if (recordCount > 0) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}

function DeletePlan(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/Cpanel/Plan/DeletePlan",
                    data: { id: id },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        hastSoft.stopLoading();
                        if (response.resultType == null) {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Cảnh báo!',
                                text: "Không thể xóa vì đang tồn tại ở CIPL!",
                                confirmButtonText: 'Đóng',
                            });
                        } else {
                            if (response.resultType === 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Thành công!',
                                    text: result.message,
                                    confirmButtonText: 'Đóng',
                                }).then((resultAlter) => {
                                    loadData(false);
                                });
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Không thành công!',
                                    text: response.message,
                                    confirmButtonText: 'Đóng',
                                });
                            }
                        }
                    },
                    error: function (status) {
                        Swal.fire(
                            'Xóa không thành công!',
                            status.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    });
}

function ExportExcel1() {
    hastSoft.startLoading();
    var selectedIds = [];
    $('tbody#tbPlan tr').each(function () {
        var ichkRole = $(this).find('.chkRole').is(":checked");
        if (ichkRole === true) {
            var planId = $(this).find('.PlanId').val();
            selectedIds.push(planId);
        }
    });


    $.ajax({
        url: '/cpanel/Plan/ExportPlans',
        type: 'POST',
        traditional: true, // Sử dụng traditional để truyền mảng kiểu GUID
        data: { ids: selectedIds },
        success: function (response) {
            // Xử lý kết quả thành công
            let a = document.createElement('a');
            a.href = response.data;
            a.download = 'ExportedDataPlans.xlsx';
            a.style.display = 'none';

            // Thêm phần tử a vào body và kích hoạt sự kiện click để tải xuống
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            // Giải phóng đường dẫn URL tạm thời
            window.URL.revokeObjectURL(response.data);
            hastSoft.stopLoading();
        },
        error: function (xhr, status, error) {
            Swal.fire({
                icon: 'error',
                title: 'Export data không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

function ExportExcel() {
    if (!selectedPlanId) {
        Swal.fire({
            icon: 'error',
            title: 'Vui lòng chọn kế hoạch!',
            confirmButtonText: 'Đóng',
        });
        return;
    }

    let url = "/cpanel/Plan/ExportPlan?planId=" + selectedPlanId;
    console.log(url);
    window.location = url;
}