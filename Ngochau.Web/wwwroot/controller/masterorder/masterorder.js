﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    initTreeDropDownCustmers('');
    initDropDownItems('');
    initDropDownColors('');
    initDropDownProducts('');
    initDropDownContainers('');
    initTreeDropDownStatus('');
    loadDataDetail(id);
    EventSearchColors();
    EventSearchProducts();
    EventSearchItems();
    EventSearchPrice();
    EventSearchQuantity();
});

//PRICE

function EventSearchQuantity() {
    $('#tblProducts').on('keyup', '.clsQuantity', function () {
        var quantity = $(this).val();
        var price = $(this).parent().parent().find('.clsPrice').val();
        var percent = $('#txtCustomerDeposit').val();
        var totalDeposit = ((quantity * price) * percent) / 100;
        var totalMoney = price * quantity;
        $(this).parent().parent().find('.clsDeposit').val(totalDeposit);
        $(this).parent().parent().find('.clsMoney').val(totalMoney);
        TotalValue();
    });
}

function EventSearchPrice() {
    $('#tblProducts').on('keyup', '.clsPrice', function () {
        var quantity = $(this).val();
        var price = $(this).parent().parent().find('.clsQuantity').val();
        var percent = $('#txtCustomerDeposit').val();
        var totalDeposit = ((quantity * price) * percent) / 100;
        var totalMoney = price * quantity;
        $(this).parent().parent().find('.clsDeposit').val(totalDeposit);
        $(this).parent().parent().find('.clsMoney').val(totalMoney);
        TotalValue();
    });
}

// COLOR
function EventSearchColors() {
    $('#tblProducts').on('click', '.search-box-color input', function () {
        $(this).parent().find('.search-results').fadeIn();
    });

    $('#tblProducts').on('blur', '.search-box-color input', function () {
        $(this).parent().find('.search-results').fadeOut();
    });

    $('#tblProducts').on('keyup', '.search-box-color input', function () {
        $('.search-results').html('');
        $(this).parent().find('.search-results').fadeIn();
        var searchValue = $(this).val();
        $.ajax({
            url: "/Cpanel/Color/GetColorBySearch",
            type: 'GET',
            data: { keyword: searchValue, take: 5 },
            dataType: 'json',
            async: false,
            success: function (response) {
                $('.search-results').html('');
                for (var i = 0; i < response.length; i++) {
                    $('.search-results').append("<p><a class'aInValue' onclick=addInTextBoxColor()> " + response[i].name + " </a></p>");
                }
            }
        });
    });
}

function addInTextBoxColor() {
    $('#tblProducts').on('click', 'a', function (e) {
        e.preventDefault();
        var valuea = $(this).text();
        var input = $(this).closest('.search-box-color').find('input');
        input.val(valuea);
        $('.search-results').html('');
    });
}

//KẾT THÚC COLOR


// PRODUCT
function EventSearchProducts() {
    $('#tblProducts').on('click', '.search-box-product input', function () {
        $(this).parent().find('.search-results').fadeIn();
    });

    $('#tblProducts').on('blur', '.search-box-product input', function () {
        $(this).parent().find('.search-results').fadeOut();
    });

    $('#tblProducts').on('keyup', '.search-box-product input', function () {
        $(this).parent().find('.search-results').fadeIn();
        var searchValue = $(this).val();
        $.ajax({
            url: "/Cpanel/Product/GetProducts",
            type: 'GET',
            data: { keyword: searchValue, take: 5 },
            dataType: 'json',
            async: false,
            success: function (response) {
                $('.search-results').html('');
                for (var i = 0; i < response.length; i++) {
                    $('.search-results').append("<p><a class'aInValue' onclick=addInTextBoxProduct('" + response[i].id + "')> " + response[i].barCode + " | " + response[i].name + " </a></p>");
                }
            }
        });
    });
}

function addInTextBoxProduct(proId) {
    $('#tblProducts').on('click', 'a', function (e) {
        e.preventDefault();
        var valuea = $(this).text();
        var input = $(this).closest('.search-box-product').find('.clsProduct');
        input.val(valuea);
        $('.search-results').html('');
    });
}

// KẾT THÚC PRODUCT

function EventSearchItems() {
    $('#tblProducts').on('click', '.search-box-item input', function () {

        $(this).parent().find('.search-results').fadeIn();
    });

    $('#tblProducts').on('blur', '.search-box-item input', function () {
        $(this).parent().find('.search-results').fadeOut();

    });

    $('#tblProducts').on('keyup', '.search-box-item input', function () {
        var searchValue = $(this).val();
        $.ajax({
            url: "/Cpanel/Item/GetItemBySearch",
            type: 'GET',
            data: { keyword: searchValue, take: 5 },
            dataType: 'json',
            async: false,
            success: function (response) {
                $('.search-results').html('');
                for (var i = 0; i < response.length; i++) {
                    $('.search-results').append("<p><a onclick=addInTextBoxItem('" + response[i].code + "')> " + response[i].name + " </a></p>");
                }
            }
        });
    });
}

function addInTextBoxItem(value) {
    $('#tblProducts').on('click', '.search-results', function (e) {
        var input = $(this).closest('.search-box-item').find('input');
        input.val(value);
        $('.search-results').html('');
    });
}

// --------------- LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //

function initTreeDropDownStatus(selectedId) {
    $.ajax({
        url: "/Cpanel/Status/GetStatus",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn trạng thái', 'ddlStatus', selectedId);
        }
    });
}

function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomer', selectedId);
        }
    });
}

function initDropDownItems(selectedId) {
    $.ajax({
        url: "/Cpanel/Item/GetItems",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn Item', 'ddlItems', selectedId);
        }
    });
}

function initDropDownColors(selectedId) {
    $.ajax({
        url: "/Cpanel/Color/GetColors",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn màu', 'ddlColors', selectedId);
        }
    });
}

function initDropDownContainers(selectedId) {
    $.ajax({
        url: "/Cpanel/Container/GetContainers",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn Container', 'ddlContainers', selectedId);
        }
    });
}

function initDropDownProducts(selectedId) {
    $.ajax({
        url: "/Cpanel/product/GetProducts",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn sản phẩm', 'ddlProducts', selectedId);
        }
    });
}

// --------------- KẾT THÚC LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //

// --------------- BẮT ĐẦU XỬ LÝ SỰ KIỆN CHANGE CỦA DROPDOWNLIST --------------- //

$("#ddlCustomer").change(function () {
    var itemId = $("#ddlCustomer").val()
    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: itemId },
        dataType: 'json',
        async: false,
        success: function (response) {
            $("#txtPhone").val(response.phone);
            $("#txtAddress").val(response.customerAddress);
            $("#txtCustomerDeposit").val(response.deposit);
            CalculatorDeposit("txtDeposit");
        }
    });
});

function CalculatorDeposit(idHtml) {
    var quantity = $("#txtQuantity").val();
    var price = $("#txtPrice").val();
    var percent = $("#txtCustomerDeposit").val();
    if (price > 0 && quantity > 0 && percent > 0) {
        var totalDeposit = ((price * quantity) * percent) / 100
        $("#" + idHtml).val(totalDeposit);
    }
    else {
        $("#" + idHtml).val(0);
    }
}

function ParseText(objsent) {
    CalculatorDeposit("txtDeposit");
}

$("#ddlItems").change(function () {
    var itemId = $("#ddlItems").val()
    $.ajax({
        url: "/Cpanel/Item/GetById",
        type: 'GET',
        data: { id: itemId },
        dataType: 'json',
        async: false,
        success: function (response) {
            $("#txtPrice").val(response.itemPrice);
            $("#txtCBM").val(response.itemCBM);
            CalculatorDeposit("txtDeposit");
        }
    });
});

$("#ddlProducts").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#productCode").val(arr[0].trim());
    $("#productName").val(arr[1].trim());
});

$("#ddlColors").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtColorCode").val(arr[0].trim());
});

$("#ddlItems").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtItemCode").val(arr[0].trim());
});

$("#ddlContainers").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtContainerCode").val(arr[0].trim());
});

// --------------- KẾT THÚC XỬ LÝ SỰ KIỆN CHANGE CỦA DROPDOWNLIST --------------- //

// --------------- BẮT ĐẦU XỬ LÝ SỰ KIỆN LƯU CÁC ROWS VÀO TABLE --------------- //

function Validate() {
    var result = true;
    var ddlCustomer = $("#ddlCustomer").val();
    var productId = $("#ddlProducts").val();
    var quantity = $("#txtQuantity").val();
    var itemId = $("#ddlItems").val();
    var colorId = $("#ddlColors").val();
    if (ddlCustomer === '') {
        Swal.fire({
            icon: 'error',
            title: 'Điền đầy đủ thông tin',
            text: 'Bạn phải chọn khách hàng'
        });
        result = false;
        return;
    }

    if (productId === '') {
        Swal.fire({
            icon: 'error',
            title: 'Điền đầy đủ thông tin',
            text: 'Bạn phải chọn sản phẩm'
        });
        result = false;
        return;
    }

    if (itemId === '') {
        Swal.fire({
            icon: 'error',
            title: 'Điền đầy đủ thông tin',
            text: 'Bạn phải chọn Item'
        });
        result = false;
        return;
    }

    if (quantity === '' || quantity === 0) {
        Swal.fire({
            icon: 'error',
            title: 'Điền đầy đủ thông tin',
            text: 'Bạn phải điền số lượng'
        });
        result = false;
        return;
    }

    if (colorId === '') {
        Swal.fire({
            icon: 'error',
            title: 'Điền đầy đủ thông tin',
            text: 'Bạn phải chọn màu'
        });
        result = false;
        return;
    }


    return result;
}

$(document).on("click", ".delete-button", function () {
    $(this).closest("tr").remove();
    TotalValue();
});

// ---------------KẾT THÚC XỬ LÝ SỰ KIỆN LƯU CÁC ROWS VÀO TABLE --------------- //

// ---------------BẮT ĐẦU XỬ LÝ SỰ KIỆN LƯU VÀO DATABASE --------------- //


function SaveMasterOrder() {
    var data = {
        Id: $('#txtId').val(),
        Code: $('#txtPO').val(),
        Name: $('#txtPOName').val(),
        CustomerId: $('#ddlCustomer').val(),
        Status: $('#ddlStatus').val(),
        MasterOrderDetailParamModel: GetDataInTables()
    };

    $.ajax({
        url: '/cpanel/masterorder/Save',
        type: 'POST',
        beforeSend: function (request) {
            hastSoft.startLoading();
        },
        data: data,
        dataType: "json",
        success: function (response) {

            if (response.statusCode === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((resultAlter) => {
                    loadData(false);
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

function GetDataInTables() {

    var productArrays = [];

    $("#tblProducts tbody tr").each(function () {
        var guidPODId = $(this).find('.clsPODId').val();
        var strProd = $(this).find('.clsProduct').val();
        var strItem = $(this).find('.clsItem').val();
        var strColor = $(this).find('.clsColor').val();
        var fPrice = $(this).find('.clsPrice').val();
        var iQuantity = $(this).find('.clsQuantity').val();
        var iCBM = $(this).find('.clsCBM').val();
        var fMonney = $(this).find('.clsMoney').val();
        var fDeposit = $(this).find('.clsDeposit').val();
        var quantity = (iQuantity !== '' || iQuantity !== null || iQuantity !== undefined) ? parseInt(iQuantity) : 0;
        var unitPrice = (fPrice !== '' || fPrice !== null || fPrice !== undefined) ? parseFloat(fPrice) : 0;
        var totalMoney = (fMonney !== '' || fMonney !== null || fMonney !== undefined) ? parseFloat(fMonney) : 0;
        var deposit = (fDeposit !== '' || fDeposit !== null || fDeposit !== undefined) ? parseFloat(fDeposit) : 0;
        var product = {
            MasterOrderDetailId: guidPODId,
            StrProduct: strProd,
            UnitPrice: unitPrice,
            Deposit: deposit,
            Quantity: quantity,
            CBM: iCBM,
            StrItem: strItem,
            StrColor: strColor,
            TotalMoney: totalMoney
        };
        if (strProd !== undefined && strItem !== undefined && strColor !== undefined)
            productArrays.push(product);
    });
    return productArrays;
}

function loadDataDetail(id) {
    if (id !== undefined && id !== 0) {
        document.getElementById("txtPO").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/MasterOrder/GetById",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                debugger;
                initTreeDropDownCustmers(result.customerId);
                initTreeDropDownStatus(result.status)
                $('#txtPO').val(result.code);
                $('#txtId').val(result.id);
                $('#txtPOName').val(result.name);
                $('#txtPhone').val(result.customer.phone);
                $('#txtAddress').val(result.customer.customerAddress);
                $("#txtCustomerDeposit").val(result.customer.deposit);
                var items = [];
                var stt = 1;
                $.each(result.poDetails, function (key, val) {
                    debugger;
                    items.push("<tr>");
                    items.push("<td>" + stt + "</td>");
                    items.push("<td>" + ProductHTML(val.productSKU, val.productName) + PODHTML(val.id) + "</td>");
                    items.push("<td>" + ItemHTML(val.itemCode, val.itemName) + "</td>");
                    items.push("<td>" + ColorHTML(val.colorCode, val.colorName) + "</td>");
                    items.push("<td>" + QuantityHTML(val.quantity) + "</td>");
                    items.push("<td>" + PriceHTML(val.unitPrice, val.colorFail, val.descriptionColorFail) + "</td>");
                    items.push("<td>" + CBMHTML(val.cbm) + "</td>");
                    items.push("<td>" + DispositHTML(val.deposit) + "</td>");
                    items.push("<td>" + TotalMoneyHTML(val.totalMoney) + "</td>");
                    items.push("<td><a class='action-icon delete-button'> <i class='mdi mdi-delete'></i></a></td>");
                    items.push("</tr>");
                    stt++;
                });
                $("#tblProducts tbody").append(items.join(""));
                /*$('#chkActive').prop('checked', result.data.status === 1 ? true : false);*/
                TotalValue();
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}

function TotalValue() {
    $("#spanTotalDeposit").val('');
    $("#spanTotalMoney").val('');
    var array = GetDataInTables();
    var totalDeposit = 0;
    var totalMoney = 0;

    $.each(array, function (index, value) {
        totalDeposit += (parseFloat(value.Deposit));
        totalMoney += value.TotalMoney;
    });

    $("#spanTotalDeposit").html(totalDeposit.toFixed(2) + " $");
    $("#spanTotalMoney").html(totalMoney + " $");
}

function ProductHTML(productCode, productName) {
    var strDev = "<div class='search-box search-box-product'>";
    strDev += "<input type='text' value = '" + productCode + " | " + productName + "' class='form-control clsProduct' placeholder='Tìm kiếm sản phẩm' style = 'width: 500px'>";
    strDev += "<div class='search-results'></div>";
    strDev += "</div>";
    return strDev;
}

function PODHTML(PODId) {
    return "<input type='hidden' class='form-control clsPODId' value = '" + PODId + "' style = 'width: 120px;'>";
}

function ColorHTML(colorCode, colorName) {
    var color = "";
    if (colorCode !== "" && colorName !== "") {
        color = colorCode + " | " + colorName;
    }
    return "<div class='search-box search-box-color'><input type='text' class='form-control clsColor' placeholder='Tìm kiếm màu' style = 'width: 150px' value='" + color + "'><div class='search-results'></div></div>";
}

function ItemHTML(itemCode, itemName) {
    return "<div class='search-box search-box-item'><input type='text' class='form-control clsItem' placeholder='Tìm kiếm item' style = 'width: 150px' value='" + itemCode + "' ><div class='search-results'></div></div>";
}

function PriceHTML(price, colorFail, title) {
    return "<input type='text' class='form-control clsPrice' title='" + title + "' placeholder='Đơn giá' value = '" + price + "' style = 'width: 120px; border: 1px solid " + colorFail + "'>";
}

function QuantityHTML(quantity) {
    return "<input type='text' class='form-control clsQuantity' placeholder='Số lượng' value = '" + quantity + "' style = 'width: 120px'>";
}

function CBMHTML(cbm) {
    return "<input type='text' class='form-control clsCBM' placeholder='CBM' value = '" + cbm + "' style = 'width: 120px'>";
}

function DispositHTML(disposit) {
    return "<input type='number' readonly class='form-control clsDeposit' placeholder='Số lượng' value = '" + disposit + "' style = 'width: 120px'>";
}

function TotalMoneyHTML(money) {
    return "<input type='number' readonly class='form-control clsMoney' placeholder='Số lượng' value = '" + money + "' style = 'width: 120px'>";
}


