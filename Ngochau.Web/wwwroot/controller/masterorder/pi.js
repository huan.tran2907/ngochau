﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    initTreeDropDownCustmers('');
    initDropDownItems('');
    initDropDownColors('');
    initDropDownProducts('');
    initDropDownContainers('');
    initTreeDropDownPOs('');
    initTreeDropDownStatus('');
    loadDataDetail(id);
});

function POChange() {
    var POId = $("#ddlPOs").val();
    $.ajax({
        type: "GET",
        url: "/Cpanel/MasterOrder/GetById",
        data: { Id: POId },
        dataType: "json",
        beforeSend: function () {
            hastSoft.startLoading();
        },
        success: function (result) {
            initTreeDropDownCustmers(result.customerId);
            CustomerChangeValue();
            $('#ddlCustomer').prop('disabled', true);
            var items = [];
            $("#tblProducts tbody").html("");
            var stt = 0;
            debugger;
            $.each(result.poDetails, function (key, val) {
                debugger;
                stt++;
                items.push("<tr>");
                items.push("<td>" + stt + "</td>");
                items.push("<td>" + ProductHTML(val.productSKU, val.productName, val.productId) + PODetailID(val.id) + "</td>");
                items.push("<td>" + ItemHTML(val.itemCode, val.itemName, val.itemId) + "</td>");
                items.push("<td>" + ColorHTML(val.colorCode, val.colorName, val.colorId) + "</td>");
                items.push("<td>" + QuantityHTML(val.quantity) + "</td>");
                items.push("<td>" + PriceHTML(val.unitPrice) + "</td>");
                items.push("<td>" + DispositHTML(val.deposit) + "</td>");
                items.push("<td>" + TotalMoneyHTML(val.totalMoney) + "</td>");
                items.push("</tr>");
            });
            $("#tblProducts tbody").append(items.join(""));
            TotalValue();
            hastSoft.stopLoading();
        },
        error: function (status) {
            hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
            hastSoft.stopLoading();
        }
    });
}

// TABLE ROWS
function PODetailID(PODId) {
    return "<input type='hidden' class='form-control clsPODId' value = '" + PODId + "' style = 'width: 550px;'>";
}
function ProductHTML(productCode, productName, productId) {
    return "<input type='text' readonly class='form-control clsProduct' value = '" + productCode + " | " + productName + "' style = 'width: 550px;'><input type='hidden' readonly class='form-control clsProductId' value = '" + productId + "' style = 'width: 120px;'>";
}

function ColorHTML(colorCode, colorName, colorId) {
    var color = "";
    if (colorCode !== "" && colorName !== "") {
        color = colorCode + " | " + colorName;
    }
    return "<input type='text' readonly class='form-control clsColor' placeholder='màu' style = 'width: 120px' value='" + colorCode + "'><input type='hidden' class='clsColorId' value='" + colorId + "'>";
}

function ItemHTML(itemCode, itemName, itemId) {
    return "<input type='text' readonly class='form-control clsItem' placeholder='item' style = 'width: 120px' value='" + itemCode + "' ><input type='hidden' class='clsItemId' value='" + itemId + "'>";
}


function PriceHTML(price) {
    return "<input type='text' readonly  onkeyup = 'ParseText();' onblur = 'ParseText();' class='form-control clsPrice' placeholder='Đơn giá' value = '" + price + "'>";
}

function QuantityHTML(quantity) {
    return "<input type='text' readonly onkeyup = 'ParseText();' onblur = 'ParseText();' class='form-control clsQuantity' placeholder='Số lượng' value = '" + quantity + "' style = 'width: 120px'>";
}

function DispositHTML(disposit) {
    return "<input type='number' readonly class='form-control clsDeposit' placeholder='deposit' value = '" + disposit + "' style = 'width: 120px'>";
}

function TotalMoneyHTML(money) {
    return "<input type='number' readonly class='form-control clsMoney' placeholder='Tổng tền' value = '" + money + "' style = 'width: 120px'>";
}

//TABLE ROWS

function AddProductInRow() {
    var table = document.getElementById("tblProducts");
    // Tạo một hàng mới
    var newRow = table.insertRow();

    // Thêm các ô (td) vào hàng mới
    var ProductCode = newRow.insertCell(0); //Mã sản phẩm
    var ItemCode = newRow.insertCell(1); //Mã Item
    var ColorCode = newRow.insertCell(2); //Mã màu
    var Quantity = newRow.insertCell(3); //Số lượng
    var Price = newRow.insertCell(4); //Đơn giá
    var totalMoney = newRow.insertCell(5);//Tổng tiền
    var cell11 = newRow.insertCell(6);

    // Thiết lập nội dung cho các ô
    ProductCode.innerHTML = "<select name='ddlPOs' class='form-control select2 ddlPOs' data-toggle='select2'></select>"
    ItemCode.innerHTML = "<select name='ddlProducts' class='form-control select2' data-toggle='select2'></select>"
    ColorCode.innerHTML = "<select name='ddlProducts' class='form-control select2' data-toggle='select2'></select>"
    Quantity.innerHTML = "<input type='number' id='txtQuantity' placeholder='0' class='form-control'>";
    Price.innerHTML = "<input type='number' id='txtQuantity' placeholder='0' class='form-control'>";
    totalMoney.innerHTML = "";
    cell11.innerHTML = "<a class='action-icon delete-button'> <i class='mdi mdi-delete'></i></a>";
    addOptionToDropdownlist("ddlPOs", $(ProductCode).find(".ddlPOs"), false);
}

function addOptionToDropdownlist(dropdownlistClass, tdIndex, optionValue, optionText, activeOption) {
    // Tìm dropdownlist cần sửa đổi bằng class
    var $dropdownlist = $("." + dropdownlistClass);

    // Tìm td cần sửa đổi
    var $td = $("td:nth-child(" + (tdIndex + 1) + ")");

    // Thêm tùy chọn mới vào dropdownlist
    $dropdownlist.append($("<option></option>").attr("value", optionValue).text(optionText));

    // Đặt giá trị mới cho dropdownlist trong td cụ thể
    $td.find("." + dropdownlistClass).val(activeOption);
}


function DropDownPOs(selectedId) {
    $.ajax({
        url: "/Cpanel/MasterOrder/GetPOs",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            addDropdownlistToTable("ddlPOs", response, selectedId);
        }
    });
}
// --------------- LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //
function initTreeDropDownStatus(selectedId) {
    $.ajax({
        url: "/Cpanel/Status/GetStatus",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn trạng thái', 'ddlStatus', selectedId);
        }
    });
}

function initTreeDropDownPOs(selectedId) {
    $.ajax({
        url: "/Cpanel/MasterOrder/GetPOs",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn đơn hàng', 'ddlPOs', selectedId);
        }
    });
}

function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomer', selectedId);
        }
    });
}

function initDropDownItems(selectedId) {
    $.ajax({
        url: "/Cpanel/Item/GetItems",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn Item', 'ddlItems', selectedId);
        }
    });
}

function initDropDownColors(selectedId) {
    $.ajax({
        url: "/Cpanel/Color/GetColors",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn màu', 'ddlColors', selectedId);
        }
    });
}

function initDropDownContainers(selectedId) {
    $.ajax({
        url: "/Cpanel/Container/GetContainers",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn Container', 'ddlContainers', selectedId);
        }
    });
}

function initDropDownProducts(selectedId) {
    $.ajax({
        url: "/Cpanel/product/GetProducts",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn sản phẩm', 'ddlProducts', selectedId);
        }
    });
}

// --------------- KẾT THÚC LOAD DANH SÁCH CÁC ENTRY VÀO DROPDOWNLIST --------------- //

// --------------- BẮT ĐẦU XỬ LÝ SỰ KIỆN CHANGE CỦA DROPDOWNLIST --------------- //

$("#ddlCustomer").change(function () {
    var cusId = $("#ddlCustomer").val()
    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: cusId },
        dataType: 'json',
        async: false,
        success: function (response) {
            $("#txtPhone").val(response.phone);
            $("#txtAddress").val(response.customerAddress);
            $("#txtCustomerDeposit").val(response.deposit);
        }
    });
});

function CustomerChangeValue() {
    var cusId = $("#ddlCustomer").val()
    $.ajax({
        url: "/Cpanel/Customer/GetById",
        type: 'GET',
        data: { id: cusId },
        dataType: 'json',
        async: false,
        success: function (response) {
            $("#txtPhone").val(response.phone);
            $("#txtAddress").val(response.customerAddress);
            $("#txtCustomerDeposit").val(response.deposit);
        }
    });
}

$("#ddlItems").change(function () {
    var itemId = $("#ddlItems").val()
    $.ajax({
        url: "/Cpanel/Item/GetItemById",
        type: 'GET',
        data: { id: itemId },
        dataType: 'json',
        async: false,
        success: function (response) {
            $("#txtPrice").val(response.itemPrice);
            $("#txtCBM").val(response.itemCBM);
        }
    });
});

$("#ddlProducts").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#productCode").val(arr[0].trim());
    $("#productName").val(arr[1].trim());
});

$("#ddlColors").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtColorCode").val(arr[0].trim());
});

$("#ddlItems").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtItemCode").val(arr[0].trim());
});

$("#ddlContainers").change(function () {
    var selectedValue = $(this).val();
    var selectedText = $(this).find("option:selected").text();
    var arr = selectedText.split("-");
    $("#txtContainerCode").val(arr[0].trim());
});

// --------------- KẾT THÚC XỬ LÝ SỰ KIỆN CHANGE CỦA DROPDOWNLIST --------------- //

// --------------- BẮT ĐẦU XỬ LÝ SỰ KIỆN LƯU CÁC ROWS VÀO TABLE --------------- //

$(document).on("click", ".delete-button", function () {
    $(this).closest("tr").remove();
    TotalValue();
});

function TotalValue() {
    $("#spanTotalDeposit").val('');
    $("#spanTotalMoney").val('');
    var array = GetDataInTables();
    var totalDeposit = 0;
    var totalMoney = 0;

    $.each(array, function (index, value) {
        totalDeposit += (parseFloat(value.Deposit));
        totalMoney += value.TotalMoney;
    });

    $("#spanTotalDeposit").html(totalDeposit.toFixed(2) + " $");
    $("#spanTotalMoney").html(totalMoney + " $");
}

// ---------------KẾT THÚC XỬ LÝ SỰ KIỆN LƯU CÁC ROWS VÀO TABLE --------------- //

// ---------------BẮT ĐẦU XỬ LÝ SỰ KIỆN LƯU VÀO DATABASE --------------- //


function SavePI() {
    var data = {
        Id: $('#txtId').val(),
        POId: $('#ddlPOs').val(),
        Name: $('#txtPOName').val(),
        PIDate: $('#txtPIDate').val(),
        CustomerId: $('#ddlCustomer').val(),
        PIDetailParamModel: GetDataInTables()
    };

    $.ajax({
        url: '/cpanel/MasterOrder/SavePI',
        type: 'POST',
        beforeSend: function (request) {
            hastSoft.startLoading();
        },
        data: data,
        dataType: "json",
        success: function (response) {

            if (response.statusCode === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                }).then((resultAlter) => {
                    loadData(false);
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
            }
            hastSoft.stopLoading();
        },
        error: function (xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });
}

function GetDataInTables() {
    var productArrays = [];
    $("#tblProducts tbody tr").each(function () {
        var podId = $(this).find('.clsPODId').val();
        var productId = $(this).find('.clsProductId').val();
        var itemId = $(this).find('.clsItemId').val();
        var colorId = $(this).find('input[class="clsColorId"]').val();
        var quantity = parseInt($(this).closest('tr').find('.clsQuantity').val());
        var deposit = parseFloat($(this).closest('tr').find('.clsDeposit').val());
        var unitPrice = parseFloat($(this).closest('tr').find('.clsPrice').val());
        var totalMoney = parseFloat($(this).closest('tr').find('.clsMoney').val());

        var product = {
            MasterOrderDetailId: podId,
            StrProduct: productId,
            UnitPrice: unitPrice,
            Deposit: deposit,
            Quantity: quantity,
            StrItem: itemId,
            StrColor: colorId,
            TotalMoney: totalMoney
        };
        if (productId !== undefined)
            productArrays.push(product);
    });
    return productArrays;
}

function loadDataDetail(id) {
    if (id !== undefined && id !== 0) {
        var dropdown = document.getElementById("ddlPOs");
        dropdown.disabled = true;
        var cus = document.getElementById("ddlCustomer");
        cus.disabled = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/MasterOrder/GetByPIId",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                debugger;
                initTreeDropDownCustmers(result.customerId);
                initTreeDropDownPOs(result.poId);
                $("#txtPIDate").val(result.piDate)
                var items = [];
                var stt = 1;
                $.each(result.piDetails, function (key, val) {
                    debugger;
                    stt++;
                    items.push("<tr>");
                    items.push("<td>" + stt + "</td>");
                    items.push("<td>" + ProductHTML(val.productSKU, val.productName, val.productId) + PODetailID(val.piDetailId) + "</td>");
                    items.push("<td>" + ItemHTML(val.itemCode, val.itemName, val.itemId) + "</td>");
                    items.push("<td>" + ColorHTML(val.colorCode, val.colorName, val.colorId) + "</td>");
                    items.push("<td>" + QuantityHTML(val.quantity) + "</td>");
                    items.push("<td>" + PriceHTML(val.unitPrice) + "</td>");
                    items.push("<td>" + DispositHTML(val.deposit) + "</td>");
                    items.push("<td>" + TotalMoneyHTML(val.totalMoney) + "</td>");
                    items.push("</tr>");
                });
                $("#tblProducts tbody").append(items.join(""));
                TotalValue();
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}

function ParseText() {
    var percent = parseInt($("#txtCustomerDeposit").val());
    $("#tblProducts tbody tr").each(function () {
        var quantity = parseInt($(this).closest('tr').find('.txtQuantity').val());
        var price = parseFloat($(this).closest('tr').find('.txtPrice').val());
        if (price > 0 && quantity > 0 && percent > 0) {
            var money = (price * quantity);
            var deposit = (money * percent) / 100;
            $(this).closest('tr').find('.txtDeposit').val(deposit);
            $(this).closest('tr').find('.txtTotalMoney').val(money);
        }
    });
    TotalValue();
}

