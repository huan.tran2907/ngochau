﻿$(document).ready(function () {
    loadData(false);
    initTreeDropDownCustmers('');
    
});


//CHECK
$('#chkParentRole').on('click', function () {
    $('.chkRole').prop('checked', $(this).prop('checked'));
});

function CheckRole() {
    var res = 0;
    var totalRow = $('#tblPOs tbody tr').length;
    $('#tblPOs tbody tr').each(function () {
        var ichkRole = $(this).find('.chkRole').is(":checked");
        if (ichkRole === true) {
            res++;
        }
    });
    if (res === totalRow) {
        $("#chkParentRole").prop("checked", true);
    }
    else {
        $("#chkParentRole").prop("checked", false);
    }
}
//END CHECK

$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});

function OnClickSearch() {
    loadData(true);
}
function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomer', selectedId);
        }
    });
}

function ExportExcel() {
    debugger;
    hastSoft.startLoading();
    var selectedIds = [];
    $('#tblPIs tbody tr').each(function () {
        debugger;
        var ichkRole = $(this).find('.chkRole').is(":checked");
        if (ichkRole === true) {
            var piId = $(this).find('.PiId').val(); 
            selectedIds.push(piId);
        }
    });

    $.ajax({
        url: '/cpanel/MasterOrder/ExportPIs',
        type: 'POST',
        traditional: true, // Sử dụng traditional để truyền mảng kiểu GUID
        data: { ids: selectedIds },
        success: function (response) {
            // Xử lý kết quả thành công
            let a = document.createElement('a');
            a.href = response.data;
            a.download = 'ExportedDataPIs.xlsx';
            a.style.display = 'none';

            // Thêm phần tử a vào body và kích hoạt sự kiện click để tải xuống
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            // Giải phóng đường dẫn URL tạm thời
            window.URL.revokeObjectURL(response.data);
            hastSoft.stopLoading();
        },
        error: function (xhr, status, error) {
            Swal.fire({
                icon: 'error',
                title: 'Export data không thành công!',
                text: xhr.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        }
    });   
}

function loadData(isPageChanged) {
    hastSoft.startLoading();
    var template = $('#pis').html();
    var render = "";
    var search = $('#txtSearchPlan').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/masterorder/GetAllPisPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    debugger;
                    render += Mustache.render(template, {
                        Stt: stt++,
                        PIId: item.piId,
                        POCode: item.poCode,
                        POName: item.poName,
                        CustomerCode: item.customerCode,
                        CustomerName: item.customerName,
                        CreateDate: item.createDate,
                        CreateTime: item.createTime,
                        PIDate: item.piDate,
                        PITime: item.piTime,
                        TotalQuantity: item.totalQuantity,
                        TotalDeposit: item.totalDeposit,
                        TotalMoney: item.totalMoney
                    });

                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            $('#tbPIs').html('');
            if (data.results.length !== 0) {
                wrapPaging(data.rowCount, function () {
                    loadData();
                }, isPageChanged);
                $('#tbPIs').html(render);
            }
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
            hastSoft.stopLoading();
        }
    });
    hastSoft.stopLoading();
}

function TopSearch() {
    loadData(false);
}
function UpdateUser(id) {
    window.location.href = "/cpanel/user/index?id=" + id;
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / parseInt($('#ddlShowPage').val()));
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        if (recordCount > 0) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}

function Delete(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/Cpanel/MasterOrder/DeletePI",
                    data: { id: id },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        if (response.statusCode === 200) {
                            hastSoft.stopLoading();
                            Swal.fire({
                                icon: 'success',
                                title: 'Thành công!',
                                text: result.message,
                                confirmButtonText: 'Đóng',
                            }).then((resultAlter) => {
                                loadData(false);
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Không thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            });
                            hastSoft.stopLoading();
                        }
                    },
                    error: function (status) {
                        Swal.fire(
                            'Xóa không thành công!',
                            status.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    })
}


function ImportFile() {
    var fileInput = document.getElementById("formFile");
    var file = fileInput.files[0];
    var id = $("#ddlCustomer").val(); 

    var formData = new FormData();
    formData.append("file", file);
    formData.append("id", id);

    $.ajax({
        url: "/Cpanel/ImportExport/ImportMasterOrder",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            hastSoft.notify('Upload image succesful!', 'success');
        },
        error: function (xhr, status, error) {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
}

// IMPORT FILE EXCELL
$('#ImportExcelPDF').click(function () {
    $('#fileInput').click();
});

$('#fileInput').change(function () {
    var fileUpload = $(this).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "/Cpanel/ImportExport/ImportMasterOrder",
        contentType: false,
        processData: false,
        data: data,
        success: function (path) {
            $('#txtHiddenImageUrl').val(path);
            hastSoft.notify('Upload image succesful!', 'success');

        },
        error: function () {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
});
