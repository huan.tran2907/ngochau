﻿$(document).ready(function () {
    loadData();
});

function Update(id) {
    window.location.href = "/cpanel/category/category?id=" + id;
}

function loadData() {
    var template = $('#table-template').html();
    var render = "";
    var search = $('#txtKeyword').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search
        },
        url: '/cpanel/Category/GetAllTrees',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Name: item.name,
                        Status: hastSoft.getStatus(item.status),
                        Avatar: hastSoft.getImage(item.image)
                    });
                }
            });
            //if (render !== '') {
            $('#tblCategory').html(render);
            //}
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
        }
    });
}

$('#btnSearch').on('click', function () {
    $('#tblCategory').html('');
    loadData();
});

function Delete(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/Admin/Category/DeleteEntity",
                    data: { id: parseInt(id) },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        var category = new categoriesController();
                        category.initialize();
                        hastSoft.stopLoading();
                        Swal.fire(
                            'Xóa thành công!',
                            'Bạn đã xóa danh mục thành công',
                            'Ok'
                        )
                    },
                    error: function (status) {
                        Swal.fire(

                            'Xóa không thành công!',
                            status.responseJSON.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    })
}

