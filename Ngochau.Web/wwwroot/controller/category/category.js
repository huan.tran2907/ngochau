﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    $("#btnAssignPermission").css("display", "none");
    initTreeDropDownCategory('');
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    loadData(id);
    handleButtonSave();

});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

//var categoryController = function () {
//    this.initialize = function () {
//        registerControls();
//        registerEvents();
//        resetFormMaintainance();
//        var url = window.location.href;
//        var id = getUrlVars(url)['id'];
//        loadData(id);
//    }

//    function registerControls() {
//        CKEDITOR.replace('txtContent', {});

//        //Fix: cannot click on element ck in modal
//        $.fn.modal.Constructor.prototype.enforceFocus = function () {
//            $(document)
//                .off('focusin.bs.modal') // guard against infinite focus loop
//                .on('focusin.bs.modal', $.proxy(function (e) {
//                    if (
//                        this.$element[0] !== e.target && !this.$element.has(e.target).length
//                        // CKEditor compatibility fix start.
//                        && !$(e.target).closest('.cke_dialog, .cke').length
//                        // CKEditor compatibility fix end.
//                    ) {
//                        this.$element.trigger('focus');
//                    }
//                }, this));
//        };
//    }
//    function resetFormMaintainance() {
//        initTreeDropDownCategory('');
//        $('#hidId').val(0);
//        $('#txtName').val('');
//        $('#txtDesc').val('');
//        $('#txtOrder').val('');
//        $('#txtHomeOrder').val('');
//        $('#txtImage').val('');
//        $('#txtMetakeyword').val('');
//        $('#txtMetaDescription').val('');
//        $('#txtSeoPageTitle').val('');
//        $('#txtSeoAlias').val('');
//        $('#ckStatus1').prop('checked', true);
//        $('#radHienThiTrangChu1').prop('checked', true);
//        $('#radType2').prop('checked', true);
//        $('#radShowItem1').prop('checked', true);
//        $('#radMenuMaijor1').prop('checked', true);
//    }
//    function initTreeDropDownCategory(selectedId) {
//        $.ajax({
//            url: "/Admin/Category/GetAllTrees",
//            type: 'GET',
//            dataType: 'json',
//            async: false,
//            success: function (response) {
//                DropDownListSelected(response, 'Chọn danh mục', 'ddlCategory', selectedId);
//            }
//        });
//    }
//    function registerEvents() {
//        $('#btnSelectImg').on('click', function () {
//            $('#fileInputImage').click();
//        });
//        $("#fileInputImage").on('change', function () {
//            var fileUpload = $(this).get(0);
//            var files = fileUpload.files;
//            var data = new FormData();
//            for (var i = 0; i < files.length; i++) {
//                data.append(files[i].name, files[i]);
//            }
//            $.ajax({
//                type: "POST",
//                url: "/Admin/Upload/UploadImageCategory",
//                contentType: false,
//                processData: false,
//                data: data,
//                success: function (path) {
//                    $('#txtImage').val(path);
//                    hastSoft.notify('Upload image succesful!', 'success');

//                },
//                error: function () {
//                    hastSoft.notify('There was error uploading files!', 'error');
//                }
//            });
//        });


//        $('body').on('click', '#btnDelete', function (e) {
//            e.preventDefault();
//            var that = $('#hidIdM').val();
//            hastSoft.confirm('Are you sure to delete?', function () {
//                $.ajax({
//                    type: "POST",
//                    url: "/Admin/Category/Delete",
//                    data: { id: that },
//                    dataType: "json",
//                    beforeSend: function () {
//                        hastSoft.startLoading();
//                    },
//                    success: function () {
//                        hastSoft.notify('Deleted success', 'success');
//                        hastSoft.stopLoading();
//                        loadData();
//                    },
//                    error: function (status) {
//                        hastSoft.notify('Has an error in deleting progress', 'error');
//                        hastSoft.stopLoading();
//                    }
//                });
//            });
//        });
//    }
//}

function initTreeDropDownCategory(selectedId) {
    $.ajax({
        url: "/Cpanel/Category/GetAllTrees",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            DropDownListSelected(response, 'Chọn danh mục', 'ddlCategory', selectedId);
        }
    });
}

$("#fileImage").on('change', function () {
    var fileUpload = $(this).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "/Cpanel/Upload/UploadCategory",
        contentType: false,
        processData: false,
        data: data,
        success: function (path) {
            $('#txtHiddenImageUrl').val(path);
            hastSoft.notify('Upload image succesful!', 'success');

        },
        error: function () {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
});

function loadData(id) {
    if (id !== undefined && id !== 0) {
        $("#ddlCategory").attr('readonly')
        $.ajax({
            type: "GET",
            url: "/Cpanel/Category/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (data) {
                initTreeDropDownCategory(data.parentId);
                $('#txtId').val(data.id);
                $('#txtCode').val(data.code);
                $('#txtName').val(data.name);
                $('#txtUrl').val(data.url);
                $('#txtDescShort').val(data.description);
                $('#txtHiddenImageUrl').val(data.image);
                $('#txtSeoTitle').val(data.seoPageTitle);
                $('#txtSeoKeyword').val(data.seoKeywords);
                $('#txtSeoDescription').val(data.seoDescription);
                $('#txtSeoUrl').val(data.seoAlias);
                $('#txtOrder').val(data.sortOrder);
                $('#txtHomeOrder').val(data.homeOrder);
                $('input[name="radShowHome"][value="' + data.homeFlag + '"]').attr('checked', 'checked');
                $('input[name="radStatus"][value="' + data.status + '"]').attr('checked', 'checked');
                $('input[name="radShowNow"][value="' + data.showItem + '"]').attr('checked', 'checked');
                $('input[name="radType"][value="' + data.type + '"]').attr('checked', 'checked');
                //$('input[name="radMenuMaijor"][value="' + data.menuMaijor + '"]').attr('checked', 'checked');
                hastSoft.stopLoading();

            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}


function Save(type) {
    var form = $("#frmCategory")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    } else {
        event.preventDefault();

        var id = $('#txtId').val();
        var name = $('#txtName').val();
        var code = $('#txtCode').val();
        var parentId = $('#ddlCategory').val();
        var description = $('#txtDescShort').val();
        //var content = CKEDITOR.instances.txtContent.getData();
        var url = $('#txtUrl').val();
        var image = $('#txtHiddenImageUrl').val();
        var order = parseInt($('#txtOrder').val());
        var homeOrder = parseInt($('#txtHomeOrder').val());

        var seoKeyword = $('#txtSeoKeyword').val();
        var seoMetaDescription = $('#txtSeoDescription').val();
        var seoPageTitle = $('#txtSeoTitle').val();
        var seoAlias = $('#txtSeoUrl').val();
        var status = $('input[name="radStatus"]:checked').val();
        var showHome = $('input[name="radShowHome"]:checked').val();
        var showItem = $('input[name="radShowNow"]:checked').val();
        var typeCategory = $('input[name="radType"]:checked').val();

        $.ajax({
            url: '/cpanel/Category/SaveEntity',
            type: 'POST',
            beforeSend: function (request) {
                hastSoft.startLoading();
            },
            data: {
                Id: id,
                Name: name,
                Description: description,
                Code: code,
                //Content: content,
                ParentId: parentId,
                Url: url,
                HomeOrder: homeOrder,
                SortOrder: order,
                HomeFlag: showHome,
                Image: image,
                Status: status,
                Type: typeCategory,
                ShowItem: showItem,
                //MenuMaijor: menuMaijor,
                SeoPageTitle: seoPageTitle,
                SeoAlias: seoAlias,
                SeoKeywords: seoKeyword,
                SeoDescription: seoMetaDescription
            },
            dataType: "json",
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/category/category?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/category/category";
                        if (type === 2)
                            window.location.href = "/cpanel/category/index";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (xhr) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: xhr.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}