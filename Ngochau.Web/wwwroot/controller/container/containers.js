﻿$(document).ready(function () {
    loadData(false);
});

$('#ddlShowPage').on('change', function () {
    hastSoft.configs.pageSize = $(this).val();
    hastSoft.configs.pageIndex = 1;
    loadData(true);
});

function loadData(isPageChanged) {
    debugger;
    hastSoft.startLoading();
    var template = $('#table-template').html();
    var render = "";
    var search = $('#txtKeyword').val();
    $.ajax({
        type: 'GET',
        data: {
            keyword: search,
            page: hastSoft.configs.pageIndex,
            pageSize: hastSoft.configs.pageSize
        },
        url: '/cpanel/container/GetAllPaging',
        dataType: 'json',
        success: function (data) {
            var stt = 1;
            $.each(data.results, function (i, item) {
                if (item.id !== 0) {
                    render += Mustache.render(template, {
                        Stt: stt++,
                        Id: item.id,
                        Code: item.code,
                        Name: item.name,
                        Status: hastSoft.getStatus(item.isActive)
                    });

                }
            });
            var rowTotal = "Hiển thị " + data.currentPage + " đến " + data.pageSize + " trong " + data.rowCount + " mục";
            $('#lblTotalRecords').html(rowTotal);
            if (data.results.length !== 0) {
                wrapPaging(data.rowCount, function () {
                    loadData();
                }, isPageChanged);
                $('#tblUsers').html(render);
            }
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
            hastSoft.notify('Cannot loading data', 'error');
            hastSoft.stopLoading();
        }
    });
}

function TopSearch() {
    loadData(false);
}
function Update(id) {
    window.location.href = "/cpanel/container/container?id=" + id;
}

function wrapPaging(recordCount, callBack, changePageSize) {
    var totalsize = Math.ceil(recordCount / parseInt($('#ddlShowPage').val()));
    //Unbind pagination if it existed or click change pagesize
    if ($('#paginationUL a').length === 0 || changePageSize === true) {
        $('#paginationUL').empty();
        $('#paginationUL').removeData("twbs-pagination");
        $('#paginationUL').unbind("page");
    }
    //Bind Pagination Event
    $('#paginationUL').twbsPagination({
        totalPages: totalsize,
        visiblePages: 7,
        first: 'Đầu',
        prev: 'Trước',
        next: 'Tiếp',
        last: 'Cuối',
        onPageClick: function (event, p) {
            hastSoft.configs.pageIndex = p;
            setTimeout(callBack(), 200);
        }
    });
}

function Delete(id) {
    Swal.fire({
        title: 'Bạn có chắc xóa không?',
        text: "Bạn sẽ không khôi phục lại được!",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.isConfirmed) {
            if (id !== '') {
                $.ajax({
                    type: "POST",
                    url: "/Cpanel/container/Delete",
                    data: { id: id },
                    dataType: "json",
                    beforeSend: function () {
                        hastSoft.startLoading();
                    },
                    success: function (response) {
                        if (response.statusCode === 200) {
                            hastSoft.stopLoading();
                            Swal.fire({
                                icon: 'success',
                                title: 'Thành công!',
                                text: result.message,
                                confirmButtonText: 'Đóng',
                            }).then((resultAlter) => {
                                loadData(false);
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Không thành công!',
                                text: response.message,
                                confirmButtonText: 'Đóng',
                            });
                            hastSoft.stopLoading();
                        }
                    },
                    error: function (status) {
                        Swal.fire(
                            'Xóa không thành công!',
                            status.message,
                            'Ok'
                        )
                        hastSoft.stopLoading();
                    }
                });
            }
            else {
                Swal.fire(
                    'Xóa không thành công!',
                    'Không tìm thấy danh mục để xóa',
                    'Ok'
                )
            }
        }
    })
}

