﻿var roleId = "";
$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    $("#btnAssignPermission").css("display", "none");
    var url = window.location.href;
    roleId = getUrlVars(url)['roleId'];
    loadPermissoin(roleId);
    handleButton();
});

function handleButton() {
    $("#submit-create").click(function () {
        Save();
    });
}

function Close() {
    window.location.href = "/cpanel/role/role?id=" + roleId;
}

function loadPermissoin(roleId) {
    $.when(loadFunctionList())
        .done(fillPermission(roleId));
}

function loadFunctionList() {
    var strUrl = "/cpanel/Function/GetAll";
    return $.ajax({
        type: "GET",
        url: strUrl,
        dataType: "json",
        beforeSend: function () {
            hastSoft.startLoading();
        },
        success: function (response) {
            let stt = 0;
            var template = $('#result-data-function-with-role').html();
            var render = "";
            $.each(response.data, function (i, item) {
                render += Mustache.render(template, {
                    STT: "",
                    treegridparent: item.parentId !== null ? "treegrid-parent-" + item.parentId : "",
                    Id: item.id,
                    Code: item.code,
                    Name: item.name,
                    Status: hastSoft.getStatus(item.status)
                });
            });
            $('#lst-data-function').html(render);
            $('.tree').treegrid();
            handleCheckAll();
            CheckAll();
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
        }
    });
}

function handleCheckAll() {
    var totalRow = $('#tblFunction tbody tr').length;

    $('#chkParentView').on('click', function () {
        $('.chkView').prop('checked', $(this).prop('checked'));

        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });

    $('#chkParentCreate').on('click', function () {
        $('.chkAdd').prop('checked', $(this).prop('checked'));
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('#chkParentUpdate').on('click', function () {
        $('.chkEdit').prop('checked', $(this).prop('checked'));
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('#chkParentDelete').on('click', function () {
        $('.chkDelete').prop('checked', $(this).prop('checked'));
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('.chkView').on('click', function () {
        if ($('.chkView:checked').length === totalRow) {
            $('#chkParentView').prop('checked', true);
        } else {
            $('#chkParentView').prop('checked', false);
        }
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('.chkAdd').on('click', function () {
        if ($('.chkAdd:checked').length === totalRow) {
            $('#chkParentCreate').prop('checked', true);
        } else {
            $('#chkParentCreate').prop('checked', false);
        }
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('.chkEdit').on('click', function () {
        if ($('.chkEdit:checked').length === totalRow) {
            $('#chkParentUpdate').prop('checked', true);
        } else {
            $('#chkParentUpdate').prop('checked', false);
        }
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
    $('.chkDelete').on('click', function () {
        if ($('.chkDelete:checked').length === totalRow) {
            $('#chkParentDelete').prop('checked', true);
        } else {
            $('#chkParentDelete').prop('checked', false);
        }
        var chkParentView = $('#chkParentView').prop('checked');
        var chkParentCreate = $('#chkParentCreate').prop('checked');
        var chkParentUpdate = $('#chkParentUpdate').prop('checked');
        var chkParentDelete = $('#chkParentDelete').prop('checked');
        if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
            $('#chkParentRole').prop('checked', true);
        }
        else {
            $('#chkParentRole').prop('checked', false);
        }
    });
}

function CheckAll() {
    $('#chkParentRole').on('click', function () {
        var totalRow = $('#tblFunction tbody tr').length;
        $('.chkView').prop('checked', $(this).prop('checked'));
        $('.chkAdd').prop('checked', $(this).prop('checked'));
        $('.chkEdit').prop('checked', $(this).prop('checked'));
        $('.chkDelete').prop('checked', $(this).prop('checked'));
        if ($('.chkView:checked').length === totalRow) {
            $('#chkParentView').prop('checked', true);
        } else {
            $('#chkParentView').prop('checked', false);
        }
        if ($('.chkAdd:checked').length === totalRow) {
            $('#chkParentCreate').prop('checked', true);
        } else {
            $('#chkParentCreate').prop('checked', false);
        }
        if ($('.chkEdit:checked').length === totalRow) {
            $('#chkParentUpdate').prop('checked', true);
        } else {
            $('#chkParentUpdate').prop('checked', false);
        }
        if ($('.chkDelete:checked').length === totalRow) {
            $('#chkParentDelete').prop('checked', true);
        } else {
            $('#chkParentDelete').prop('checked', false);
        }
    });
}

function fillPermission(roleId) {
    var strUrl = "/Cpanel/Role/ListAllFunction";
    return $.ajax({
        type: "POST",
        url: strUrl,
        data: {
            roleId: roleId
        },
        dataType: "json",
        beforeSend: function () {
            hastSoft.stopLoading();
        },
        success: function (response) {
            var litsPermission = response;
            $.each($('#tblFunction tbody tr'), function (i, item) {
                $.each(litsPermission, function (j, jitem) {
                    if (jitem.functionId === $(item).data('id')) {
                        $(item).find('.chkView').first().prop('checked', jitem.canRead);
                        $(item).find('.chkAdd').first().prop('checked', jitem.canCreate);
                        $(item).find('.chkEdit').first().prop('checked', jitem.canUpdate);
                        $(item).find('.chkDelete').first().prop('checked', jitem.canDelete);
                    }
                });
            });

            if ($('.chkView:checked').length === $('#tblFunction tbody tr .chkView').length) {
                $('#chkParentView').prop('checked', true);
            } else {
                $('#chkParentView').prop('checked', false);
            }
            if ($('.chkAdd:checked').length === $('#tblFunction tbody tr .chkAdd').length) {
                $('#chkParentCreate').prop('checked', true);
            } else {
                $('#chkParentCreate').prop('checked', false);
            }
            if ($('.chkEdit:checked').length === $('#tblFunction tbody tr .chkEdit').length) {
                $('#chkParentUpdate').prop('checked', true);
            } else {
                $('#chkParentUpdate').prop('checked', false);
            }
            if ($('.chkDelete:checked').length === $('#tblFunction tbody tr .chkDelete').length) {
                $('#chkParentDelete').prop('checked', true);
            } else {
                $('#chkParentDelete').prop('checked', false);
            }
            var chkParentView = $('#chkParentView').prop('checked');
            var chkParentCreate = $('#chkParentCreate').prop('checked');
            var chkParentUpdate = $('#chkParentUpdate').prop('checked');
            var chkParentDelete = $('#chkParentDelete').prop('checked');
            if (chkParentView && chkParentCreate && chkParentUpdate && chkParentDelete) {
                $('#chkParentRole').prop('checked', true);
            }
            else {
                $('#chkParentRole').prop('checked', false);
            }
            hastSoft.stopLoading();
        },
        error: function (status) {
            console.log(status);
        }
    });
}

function Save() {
    var listPermmission = [];
    $.each($('#tblFunction tbody tr'), function (i, item) {
        listPermmission.push({
            RoleId: roleId,
            FunctionId: $(item).data('id'),
            CanRead: $(item).find('.chkView').first().prop('checked'),
            CanCreate: $(item).find('.chkAdd').first().prop('checked'),
            CanUpdate: $(item).find('.chkEdit').first().prop('checked'),
            CanDelete: $(item).find('.chkDelete').first().prop('checked'),
        });
    });
    $.ajax({
        type: "POST",
        url: "/cpanel/role/SavePermission",
        data: {
            listPermmission: listPermmission,
            roleId: roleId
        },
        beforeSend: function () {
            hastSoft.startLoading();
        },
        success: function (response) {
            Swal.fire({
                icon: 'success',
                title: 'Thành công!',
                text: response.message,
                confirmButtonText: 'Đóng',
            });
            hastSoft.stopLoading();
        },
        error: function () {
            hastSoft.notify('Has an error in save permission progress', 'error');
            hastSoft.stopLoading();
        }
    });
}