﻿
function login() {
    var username = $('#txtUserName').val();
    var password = $('#txtPassword').val();
    $.ajax({
        type: 'POST',
        data: {
            Email: username,
            Password: password
        },
        dateType: 'json',
        url: '/Cpanel/Authen/Authen',
        success: function (res) {
            if (res.success) {
                window.location.href = "/cpanel/cpanel/index";
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Đăng nhập không thành công',
                    text: res.message
                })
            }
        }
    })
}
