﻿$(document).ready(function () {
    $("#formTopSearch").css("display", "none");
    var url = window.location.href;
    var id = getUrlVars(url)['id'];
    initTreeDropDownCustmers('');
    loadData(id);
    handleButtonSave();
});

function handleButtonSave() {
    $("#submit-create").click(function () {
        Save(0);
    });
    $("#submit-create-new").click(function () {
        Save(1);
    });
    $("#submit-create-close").click(function () {
        Save(2);
    });
}

function initTreeDropDownCustmers(selectedId) {
    $.ajax({
        url: "/Cpanel/Customer/GetCustomerAll",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            var data = [];
            DropDownListSelected(response, 'Chọn khách hàng', 'ddlCustomers', selectedId);
        }
    });
}


function Save(type) {
    var form = $("#frm")
    if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
        form.addClass('was-validated');
    }
    else {
        var txtId = $("#txtId").val();
        var txtCode = $("#txtCustomerCode").val();
        var txtName = $("#txtCustomerName").val();
        var txtPhone = $("#txtPhone").val();
        var txtMail = $("#txtMail").val();
        var txtFax = $("#txtFax").val();
        var fileImage = $("#txtHiddenImageUrl").val();
        var txtAddress = $("#txtAddress").val();
        var txtDeposit = $("#txtDeposit").val();
        var chkIsRule = $("#chkIsRule").is(":checked") === true ? 1 : 0;
        $.ajax({
            type: "POST",
            url: "/Cpanel/customer/SaveEntity",
            data: {
                Id: txtId,
                Code: txtCode,
                Name: txtName,
                Phone: txtPhone,
                Fax: txtFax,
                Mail: txtMail,
                Deposit: txtDeposit,
                Avatar: fileImage,
                IsRule: chkIsRule,
                CustomerAddress: txtAddress
            },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (response) {

                if (response.statusCode === 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    }).then((resultAlter) => {
                        if (type === 0)
                            window.location.href = "/cpanel/customer/customer?id=" + response.data;
                        if (type === 1)
                            window.location.href = "/cpanel/customer/customer";
                        if (type === 2)
                            window.location.href = "/cpanel/customer/index";
                    });
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Không thành công!',
                        text: response.message,
                        confirmButtonText: 'Đóng',
                    });
                }
                hastSoft.stopLoading();
            },
            error: function (response) {
                Swal.fire({
                    icon: 'error',
                    title: 'Không thành công!',
                    text: response.message,
                    confirmButtonText: 'Đóng',
                });
                hastSoft.stopLoading();
            }
        });
    }
}

function loadData(id) {
    if (id !== undefined && id !== 0) {
        document.getElementById("txtCustomerCode").readOnly = true;
        $.ajax({
            type: "GET",
            url: "/Cpanel/customer/GetById",
            data: { id: id },
            dataType: "json",
            beforeSend: function () {
                hastSoft.startLoading();
            },
            success: function (result) {
                $("#txtId").val(result.id);
                $("#txtCustomerCode").val(result.code);
                $("#txtCustomerName").val(result.name);
                $("#txtPhone").val(result.phone);
                $("#txtMail").val(result.mail);
                $("#txtFax").val(result.fax);
                $("#txtDeposit").val(result.deposit);
                $("#txtHiddenImageUrl").val(result.avatar);
                $("#txtAddress").val(result.customerAddress);
                $('#chkIsRule').prop('checked', result.isRule === 1 ? true : false);
                hastSoft.stopLoading();
            },
            error: function (status) {
                hastSoft.notify('Lỗi hệ thống, xin kiểm tra lại', 'error');
                hastSoft.stopLoading();
            }
        });
    }
}

$("#fileImage").on('change', function () {
    debugger;
    var fileUpload = $(this).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        type: "POST",
        url: "/Cpanel/Upload/UploadAvatarCustomer",
        contentType: false,
        processData: false,
        data: data,
        success: function (path) {
            $('#txtHiddenImageUrl').val(path);
            hastSoft.notify('Upload image succesful!', 'success');

        },
        error: function () {
            hastSoft.notify('There was error uploading files!', 'error');
        }
    });
});