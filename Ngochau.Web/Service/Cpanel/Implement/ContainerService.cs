﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class ContainerService : IContainerService
    {
        IUnitOfWork _unitOfWork;
        public ContainerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var container = _unitOfWork.ContainerRepository.GetFirstOrDefault(x => x.Id == id);
                if (container != null)
                {
                    
                        _unitOfWork.ContainerRepository.Removed(container);
                        _unitOfWork.Save();
                        idResult.ResultType = 200;
                        idResult.Id = id;
                    
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
            }

            return idResult;
        }

        public PagedResult<ContainerResultModel> GetAllPagingAsync(string keyword, int page, int pageSize)
        {
            var query = _unitOfWork.ContainerRepository.GetAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword)
                || x.Code.Contains(keyword));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(x => new ContainerResultModel()
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                IsActive = x.IsActive,

            }).ToList();

            var paginationSet = new PagedResult<ContainerResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public Container GetById(Guid id)
        {
            var model = _unitOfWork.ContainerRepository.GetFirstOrDefault(x => x.Id == id);
            return model;
        }

        public List<Container> GetContainers(string keyword = null)
        {
            return _unitOfWork.ContainerRepository.GetAll().Where(x => (keyword == null || x.Code == keyword || x.Name.Contains(keyword))).ToList();
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(Container model)
        {

            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var resultMasterOrder = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    model.Id = Guid.NewGuid();
                    _unitOfWork.ContainerRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(Container model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var container = _unitOfWork.ContainerRepository.GetFirstOrDefault(x => x.Id == model.Id);
                container.Name = model.Name;
                container.IsActive = model.IsActive;
                _unitOfWork.ContainerRepository.Update(container);
                _unitOfWork.Save();
                idResult.Id = container.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }
    }
}
