﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using System.Linq;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class ColorService : IColorService
    {
        IUnitOfWork _unitOfWork;
        public ColorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var color = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Id == id);
                if (color != null)
                {

                    _unitOfWork.ColorRepository.Removed(color);
                    _unitOfWork.Save();
                    idResult.ResultType = 200;
                    idResult.Id = id;

                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
            }

            return idResult;
        }

        public PagedResult<Color> GetAllPagingAsync(string keyword, int page, int pageSize)
        {
            var query = _unitOfWork.ColorRepository.GetAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword)
                || x.Code.Contains(keyword));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);
            var paginationSet = new PagedResult<Color>()
            {
                Results = query.ToList(),
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }
        public Color GetById(Guid id)
        {
            var model = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Id == id);
            return model;
        }

        public List<Color> GetColors(string keyword = null, int take = 0)
        {
            try
            {
                if (take == 0)
                    return _unitOfWork.ColorRepository.GetAll().Where(x => (keyword == null || x.Code == keyword || x.Name.Contains(keyword))).ToList();
                else
                    return _unitOfWork.ColorRepository.GetAll().Where(x => (keyword == null || x.Code.ToLower().Contains(keyword.ToLower()) || x.Name.ToLower().Contains(keyword.ToLower()))).Take(take).ToList();
            }
            catch
            {
                return new List<Color>();
            }

        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(Color model)
        {

            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var resultMasterOrder = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    model.Id = Guid.NewGuid();
                    _unitOfWork.ColorRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }

        public async Task InsertUpdateColor(Dictionary<string, string> models)
        {
            foreach (var item in models)
            {
                Color col = _unitOfWork.ColorRepository.GetFirstOrDefault(x=>x.Code == item.Key);
                if(col == null)
                {
                    col = new Color();
                    col.Code = item.Key;
                    col.Name = item.Value;
                    Insert(col);
                }
                else
                {
                    col = new Color();
                    col.Code = item.Key;
                    col.Name = item.Value;
                    Update(col);
                }
            }
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(Color model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var color = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Id == model.Id);
                color.Name = model.Name;
                color.IsActive = model.IsActive;
                _unitOfWork.ColorRepository.Update(color);
                _unitOfWork.Save();
                idResult.Id = color.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }


    }
}
