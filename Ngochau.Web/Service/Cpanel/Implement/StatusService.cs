﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class StatusService : IStatusService
    {
        IUnitOfWork _unitOfWork;
        public StatusService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<Guid> Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public Status GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<Status> GetStatus(string keyword = null)
        {
            return _unitOfWork.StatusRepository.GetAll().Where(x => (keyword == null || x.Code == keyword || x.Name.Contains(keyword))).ToList();
        }

        public async Task<CreateUpdateDeleteResultModel<int>> Insert(Status model)
        {

            CreateUpdateDeleteResultModel<int> idResult = new CreateUpdateDeleteResultModel<int>();
            try
            {
                var resultMasterOrder = _unitOfWork.StatusRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    model.Id = 1;
                    _unitOfWork.StatusRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }

        public Task<CreateUpdateDeleteResultModel<Guid>> Update(Status model)
        {
            throw new NotImplementedException();
        }
    }
}
