﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class ItemService : IItemService
    {
        IUnitOfWork _unitOfWork;
        public ItemService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var item = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    _unitOfWork.ItemRepository.Removed(item);
                    _unitOfWork.Save();
                    idResult.ResultType = 200;
                    idResult.Id = id;
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
            }

            return idResult;
        }

        public PagedResult<Item> GetAllPagingAsync(string keyword, int page, int pageSize)
        {
            var query = _unitOfWork.ItemRepository.GetAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword)
                || x.Code.Contains(keyword));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);
            var paginationSet = new PagedResult<Item>()
            {
                Results = query.ToList(),
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public Item GetById(Guid id)
        {
            return _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Id == id);
        }

        public List<Item> GetItems(string keyword = null, int take = 0)
        {
            try
            {
                if (take == 0)
                    return _unitOfWork.ItemRepository.GetAll().Where(x => (keyword == null || x.Code.Contains(keyword))).ToList();
                else
                    return _unitOfWork.ItemRepository.GetAll().Where(x => (keyword == null || x.Code.Contains(keyword))).Take(take).ToList();
            }
            catch 
            {
                return new List<Item>();
            }
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(Item model)
        {

            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var resultMasterOrder = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    model.Id = Guid.NewGuid();
                    model.CreateDate = DateTime.Now;
                    _unitOfWork.ItemRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }
        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(Item model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var item = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Id == model.Id);
                item.Name = model.Name;
                item.CBM = model.CBM;
                item.BoxDimension = model.BoxDimension;
                item.BoxWeight = model.BoxWeight;
                item.PriceNew = model.PriceNew;
                item.PriceOld = model.PriceOld;
                item.CodeY = model.CodeY;
                item.Desc = model.Desc;
                item.MPN = model.MPN;
                _unitOfWork.ItemRepository.Update(item);
                _unitOfWork.Save();
                idResult.Id = item.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }

    }
}
