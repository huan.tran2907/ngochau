﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using static Ngochau.Web.Model.ResultModels.PlanResultModel;
using System.Numerics;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Drawing;
using NuGet.Protocol.Plugins;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class CIPLService : ICIPLService
    {
        private IUnitOfWork _unitOfWork;

        public CIPLService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CIPLResultModel.GetPlanForCIPLResultModel> GetPlanForCIPL(Guid planId)
        {
            CIPLResultModel.GetPlanForCIPLResultModel result = new CIPLResultModel.GetPlanForCIPLResultModel();

            var plan = _unitOfWork.PlanRepository.GetFirstOrDefault(plan => plan.Id == planId);

            if (plan != null)
            {
                result.PlanId = plan.Id;
                result.Code = plan.Code;
                result.Name = plan.Name;
                result.Desc = plan.Desc;
                result.CustomerId = plan.CustomerId;
                result.TotalDeposit = plan.TotalDeposit;
                result.TotalMoney = plan.TotalMoney;
                result.TotalQuantity = plan.TotalQuantity;
                result.CreateDate = plan.CreateDate;

                foreach (var item in plan.PlanDetails.OrderBy(plan => plan.CreateDate))
                {
                    var po = _unitOfWork.PORepository.GetFirstOrDefault(po => po.PODetails.Where(poD => poD.ProductId == item.ProductId).Any() == true);

                    //Check if exist CIPL in the past
                    int? quantityOldCIPL = _unitOfWork.CIPLDetailRepository.GetAll().Where(p => p.ProductId == item.ProductId && p.PlanId == plan.Id)?.Sum(p => p.Quantity);

                    var poDetail = _unitOfWork.PODetailRepository.GetFirstOrDefault(pod => pod.ProductId == item.ProductId);
                    //int currentQuantityPO = poDetail.Quantity.Value - (poDetail.QuantityExport ?? 0);

                    int quantity = item.Quantity.Value;
                    if (quantityOldCIPL > 0)
                    {
                        quantity = quantity - (quantityOldCIPL ?? 0);
                    }

                    if (quantity == 0) continue;

                    var product = _unitOfWork.ProductRepository.GetFirstOrDefault(mo => mo.Id == item.ProductId);
                    var color = _unitOfWork.ColorRepository.GetFirstOrDefault(mo => mo.Id == item.ColorId);
                    var itemData = _unitOfWork.ItemRepository.GetFirstOrDefault(mo => mo.Id == item.ItemId);

                    CIPLResultModel.GetPlanForCIPLDetailResultModel planDetail = new CIPLResultModel.GetPlanForCIPLDetailResultModel();
                    planDetail.PlanDetailId = item.Id;
                    planDetail.PlanId = item.PlanId;
                    planDetail.POCode = po.Code;
                    planDetail.ProductId = item.ProductId;
                    planDetail.ProductCode = product.Code;
                    planDetail.ProductName = product.Name;
                    planDetail.ItemId = item.ItemId;
                    planDetail.ItemCode = itemData.Code;
                    planDetail.ItemName = itemData.Name;
                    planDetail.ColorId = item.ColorId;
                    planDetail.ColorCode = color.Code;
                    planDetail.ColorName = color.Name;

                    planDetail.Quantity = quantity;
                    planDetail.RemainQuantity = quantity;

                    planDetail.CBM = item.CBM;

                    planDetail.Desc = item.Desc;
                    planDetail.Deposit = item.Deposit;
                    planDetail.Money = item.Money;
                    planDetail.UnitPrice = item.UnitPrice;
                    //planDetail.UnitPriceOld = item.UnitPriceOld;

                    planDetail.CreateDate = item.CreateDate;

                    planDetail.DescCodeColor = item.DescCodeColor;
                    planDetail.DescColorFails = item.DescColorFails;

                    result.PlanDetails.Add(planDetail);
                }
            }

            return result;
        }

        public List<CIPLResultModel.CIPLGetAllResultModel> GetAll()
        {
            var query = _unitOfWork.CIPLRepository.GetAll();

            return query
                .Select(p => new CIPLResultModel.CIPLGetAllResultModel()
                {
                    Id = p.Id,
                    Code = p.Code,
                    Name = p.Name
                }).ToList();
        }


        public async Task<CIPLResultModel.CIPLGetByIdResultModel> GetById(Guid Id)
        {
            CIPLResultModel.CIPLGetByIdResultModel ciplResult = new CIPLResultModel.CIPLGetByIdResultModel();
            ciplResult.CIPLDetails = new List<CIPLResultModel.CIPLDetailGetByIdResultModel>();
            var cipl = _unitOfWork.CIPLRepository.GetFirstOrDefault(x => x.Id == Id);
            ciplResult.CIPLId = cipl.Id;
            ciplResult.Code = cipl.Code;
            ciplResult.Name = cipl.Name;
            ciplResult.CustomerId = cipl.CustomerId;
            ciplResult.ContainerId = cipl.ContainerId;
            ciplResult.TotalDeposit = cipl.TotalDeposit;
            ciplResult.TotalMoney = cipl.TotalMoney;
            ciplResult.TotalQuantity = cipl.TotalQuantity;
            ciplResult.CreateDate = cipl.CreateDate;
            ciplResult.IsHasInvoice = _unitOfWork.InvoiceDetailRepository.GetAll().Where(i => i.CIPLId == cipl.Id).Any();

            var plans = _unitOfWork.PlanRepository.GetAll().Select(mo => new { mo.Id, mo.Code }).ToList();
            foreach (var item in cipl.CIPLDetails.OrderBy(cipl2 => cipl2.CreateDate))
            {
                var po = _unitOfWork.PORepository.GetFirstOrDefault(po => po.PODetails.Where(poD => poD.ProductId == item.ProductId).Any() == true);

                var product = _unitOfWork.ProductRepository.GetFirstOrDefault(mo => mo.Id == item.ProductId);
                var color = _unitOfWork.ColorRepository.GetFirstOrDefault(mo => mo.Id == item.ColorId);
                var itemData = _unitOfWork.ItemRepository.GetFirstOrDefault(mo => mo.Id == item.ItemId);

                CIPLResultModel.CIPLDetailGetByIdResultModel ciplDetailResult = new CIPLResultModel.CIPLDetailGetByIdResultModel();
                ciplDetailResult.CIPLDetailId = item.Id;
                ciplDetailResult.CIPLId = item.CIPLId;

                ciplDetailResult.POCode = po.Code;

                ciplDetailResult.PlanId = item.PlanId;
                ciplDetailResult.PlanCode = plans.Where(mo => mo.Id == item.PlanId).FirstOrDefault()?.Code;

                ciplDetailResult.ProductId = item.ProductId;
                ciplDetailResult.ProductCode = product.Code;
                ciplDetailResult.ProductName = product.Name;

                ciplDetailResult.ColorId = item.ColorId;
                ciplDetailResult.ColorCode = color.Code;
                ciplDetailResult.ColorName = color.Name;

                ciplDetailResult.ItemId = item.ItemId;
                ciplDetailResult.ItemCode = itemData.Code;
                ciplDetailResult.ItemName = itemData.Name;

                ciplDetailResult.RemainQuantity = item.Quantity;
                ciplDetailResult.Quantity = item.Quantity;

                ciplDetailResult.Deposit = item.Deposit;
                ciplDetailResult.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                ciplDetailResult.Money = item.Money;

                ciplDetailResult.DescCodeColor = item.DescCodeColor;
                ciplDetailResult.DescColorFails = item.DescColorFails;

                ciplResult.CIPLDetails.Add(ciplDetailResult);
            }
            return ciplResult;
        }

        public PagedResult<CIPLResultModel.CIPLGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            var query = _unitOfWork.CIPLRepository.GetAll();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(q => q.Name.Contains(keyword) || q.Code.Contains(keyword));
            }

            //if (status != null)
            //{
            //    query = query;
            //}

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(q => new CIPLResultModel.CIPLGetAllPagingResultModel()
            {
                Id = q.Id,
                Code = q.Code,
                Name = q.Name,
                Container = _unitOfWork.ContainerRepository.GetFirstOrDefault(c => c.Id == q.ContainerId)?.Name,
                CustomerName = _unitOfWork.CustomerRepository.GetFirstOrDefault(c => c.Id == q.CustomerId)?.Name,
                TotalDeposit = q.TotalDeposit,
                TotalMoney = q.TotalMoney,
                CreateDate = q.CreateDate != null ? ((DateTime)q.CreateDate).ToString("dd/MM/yyyy") : "",
            }).ToList();

            var paginationSet = new PagedResult<CIPLResultModel.CIPLGetAllPagingResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> InsertCIPL(CIPLParamModel.CIPLInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultCIPL = _unitOfWork.CIPLRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultCIPL != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    //Lưu thông tin master order
                    CIPL cIPL = new CIPL();
                    cIPL.Id = Guid.NewGuid();
                    cIPL.Code = model.Code;
                    cIPL.Name = model.Name;
                    cIPL.CustomerId = model.CustomerId;
                    cIPL.ContainerId = model.ContainerId;
                    cIPL.TotalDeposit = model.CIPLDetails.Sum(x => x.Deposit);
                    cIPL.TotalQuantity = model.CIPLDetails.Sum(x => x.Quantity);
                    cIPL.TotalMoney = model.CIPLDetails.Sum(x => x.TotalMoney);
                    cIPL.CreateDate = DateTime.Now;

                    _unitOfWork.CIPLRepository.Add(cIPL);

                    foreach (var item in model.CIPLDetails)
                    {
                        CIPLDetail cIPLDetail = new CIPLDetail();
                        cIPLDetail.Id = Guid.NewGuid();
                        cIPLDetail.CIPLId = cIPL.Id;
                        cIPLDetail.PlanId = item.PlanId;
                        PlanDetail planDetail = _unitOfWork.PlanDetailRepository.GetFirstOrDefault(x => x.Id == item.PlanDetailId);
                        if (planDetail != null)
                        {
                            cIPLDetail.CBM = planDetail.CBM;
                            cIPLDetail.DescCodeColor = planDetail.DescCodeColor;
                            cIPLDetail.DescColorFails = planDetail.DescColorFails;
                        }
                        cIPLDetail.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        cIPLDetail.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        cIPLDetail.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        cIPLDetail.Quantity = item.Quantity;
                        cIPLDetail.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        cIPLDetail.Deposit = item.Deposit;
                        cIPLDetail.Money = item.TotalMoney;
                        cIPLDetail.CreateDate = DateTime.Now;
                        _unitOfWork.CIPLDetailRepository.Add(cIPLDetail);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = cIPL.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(CIPLParamModel.CIPLInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                if (model.CIPLId == Guid.Empty || model.CIPLId == null)
                {
                    idResult.ResultType = 404;

                }
                var cIPL = _unitOfWork.CIPLRepository.GetFirstOrDefault(x => x.Id == model.CIPLId);
                if (cIPL != null)
                {
                    cIPL.Name = model.Name;
                    cIPL.Code = model.Code;
                    cIPL.TotalQuantity = model.CIPLDetails.Sum(x => x.Quantity);
                    cIPL.TotalDeposit = model.CIPLDetails.Sum(x => x.Deposit);
                    cIPL.TotalMoney = model.CIPLDetails.Sum(x => x.TotalMoney);

                    _unitOfWork.CIPLRepository.Update(cIPL);

                    //Remove all before insert new list CIPL detail
                    var listCIPLDetail = _unitOfWork.CIPLDetailRepository.GetAll().Where(pd => pd.CIPLId == model.CIPLId).ToList();
                    _unitOfWork.CIPLDetailRepository.RemoveRange(listCIPLDetail);

                    foreach (var item in model.CIPLDetails)
                    {
                        CIPLDetail cIPLDetailInsert = new CIPLDetail();
                        cIPLDetailInsert.Id = Guid.NewGuid();
                        cIPLDetailInsert.CIPLId = cIPL.Id;
                        cIPLDetailInsert.PlanId = item.PlanId;
                        cIPLDetailInsert.DescCodeColor = item.DescCodeColor;
                        cIPLDetailInsert.DescColorFails = item.DescColorFails;
                        cIPLDetailInsert.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        cIPLDetailInsert.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        cIPLDetailInsert.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        cIPLDetailInsert.Quantity = item.Quantity;
                        cIPLDetailInsert.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        cIPLDetailInsert.Deposit = item.Deposit;
                        cIPLDetailInsert.Money = item.TotalMoney;
                        _unitOfWork.CIPLDetailRepository.Add(cIPLDetailInsert);
                    }

                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = cIPL.Id;
                    idResult.ResultType = 200;
                    return idResult;
                }
                else
                {
                    idResult.ResultType = 404;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> DeleteCIPL(Guid ciplId)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var existInvoice = _unitOfWork.InvoiceDetailRepository.GetAll().Where(x => x.CIPLId == ciplId).Any();

                if (existInvoice == false)
                {
                    var cipl = _unitOfWork.CIPLRepository.GetFirstOrDefault(x => x.Id == ciplId);
                    if (cipl != null)
                    {
                        _unitOfWork.CIPLDetailRepository.RemoveRange(cipl.CIPLDetails);
                        _unitOfWork.CIPLRepository.Removed(cipl);
                        _unitOfWork.Save();
                        _unitOfWork.CommitTransaction();
                        idResult.ResultType = 200;
                        idResult.Id = ciplId;
                    }
                    else
                    {
                        idResult.ResultType = 404;
                        idResult.Id = ciplId;
                        _unitOfWork.RollbackTransaction();
                    }
                }
                else
                {
                    idResult.ResultType = null;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = ciplId;
                _unitOfWork.RollbackTransaction();
            }

            return idResult;
        }

        public async Task<ExcelFileData> ExportCIPL(Guid ciplId)
        {
            var cipl = await GetById(ciplId);

            ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("FEB");

            // Set the font to Times New Roman for the entire worksheet
            ws.Cells.Style.Font.Name = "Times New Roman";

            // Define the title and address information
            var title = "NGO CHAU TRADING COMPANY LIMITED";
            var address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
            var contact = "Tel: (+84 8) 3730 9465 | Fax: (+84 8) 3730 8465";
            var title2 = "COMMERCIAL INVOICE & PACKING LIST";

            SetTextCell(ws, "A2:L2", title, 12, false, true);
            SetTextCell(ws, "A3:L3", address, 12, false, true);
            SetTextCell(ws, "A4:L4", contact, 12, false, true);
            SetTextCell(ws, "A6:L6", title2, 12, true, true);

            SetTextCell(ws, "B8", "PO#", 11, true, false, true);
            SetTextCell(ws, "M8", "PO ETD:", 11, true, false, true);

            SetTextCell(ws, "F7", "Kiểm cuối", 11, false, false, false);
            SetTextCell(ws, "F8", "Date: " + cipl.CreateDate?.ToString("dd/MM/yyyy"), 12, true);

            var customer = GetCustomer(cipl.CustomerId.Value);
            SetTextCell(ws, "A10", "Customer:", 11, false, false, true);
            SetTextCell(ws, "A11:B11", customer.Name, 11, false, false, false);
            SetTextCell(ws, "A12:B12", customer.CustomerAddress, 11, false, false, false);
            SetTextCell(ws, "A13:B13", "P: " + customer.Phone + " | F: " + customer.Fax, 11, false, false, false);

            SetTextCell(ws, "F10", "PO#", 11, true, false, false);
            SetTextCell(ws, "F11", "", 11, false, false, false);
            SetTextCell(ws, "F12", "PO Date", 11, true, false, true);
            SetTextCell(ws, "F13", "", 11, false, false, false);

            SetTextCell(ws, "G10", "CONT# KOCU4088423", 11, true, false, false);
            SetTextCell(ws, "G11", "SEAL# 23 0136605", 11, true, false, false);
            SetTextCell(ws, "G12:G13", "MGB34923", 11, true, true, false);

            SetTextCell(ws, "J10", "Contact", 11, true, false, true);
            SetTextCell(ws, "J11:L11", " SIAMKARAT VIETNAM REP OFFICE", 11, true, false, false);
            SetTextCell(ws, "J12:L12", "Ms. Helen - Merchandiser", 11, false, false, false);

            // Write the headers to the header table
            SetHeaderTableCell(ws);

            int startRowTable = 14;
            int startRowTableData = 14;

            var listGroupCodes = cipl.CIPLDetails.GroupBy(pd => pd.POCode).Select(pd => pd.Key).ToList();
            foreach (var code in listGroupCodes)
            {
                startRowTableData++;
                var groupCode = ws.Cells[string.Format("A{0}:S{1}", startRowTableData, startRowTableData)];
                groupCode.Merge = true;
                groupCode.Value = code;
                groupCode.Style.Font.Size = 11;
                groupCode.Style.Font.Bold = true;
                groupCode.Style.Fill.PatternType = ExcelFillStyle.Solid;
                groupCode.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                var listCIPLDetails = cipl.CIPLDetails.Where(p => p.POCode == code).ToList();
                foreach (var ciplDetail in listCIPLDetails)
                {
                    startRowTableData++;
                    SetDataTableCell(ws, startRowTableData, ciplDetail);
                }
            }
            int endRowTable = startRowTableData;


            //var tableRange = ws.Cells[startRowTable, 1, endRowTable, 19];
            var tableRange = ws.Cells[10, 1, endRowTable, 19];
            var borders = tableRange.Style.Border;
            borders.Left.Style = borders.Right.Style = borders.Top.Style = borders.Bottom.Style = ExcelBorderStyle.Thin;

            endRowTable++;
            ws.Cells[endRowTable, 11].Value = cipl.CIPLDetails.Sum(pd => pd.Quantity);
            ws.Cells[endRowTable, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 11].Value = "PCS";
            ws.Cells[endRowTable + 1, 11].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 12].Value = 0;
            ws.Cells[endRowTable, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 12].Value = "PKGS";
            ws.Cells[endRowTable + 1, 12].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 13].Value = "$  " + cipl.CIPLDetails.Sum(pd => pd.Money);
            ws.Cells[endRowTable, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 13].Value = "USD";
            ws.Cells[endRowTable + 1, 13].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 14].Value = 0;
            ws.Cells[endRowTable, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 14].Value = "KGS";
            ws.Cells[endRowTable + 1, 14].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable + 1, 1].Value = "Country of Origin : Made in VN";
            ws.Cells[endRowTable + 1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            ws.Cells[endRowTable + 2, 1].Value = "I certify that all chemical substances in this shipment comply with all applicable rules or orders under TSCA and that I am not offering a chemical substance for entry in violation of TSCA or any applicable rule or order there under";
            ws.Cells[endRowTable + 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 2, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            return new ExcelFileData
            {
                Name = string.Format(@"cipl-{0}.xlsx", cipl.CreateDate?.ToString("dd-MM-yyyy-hh-mm-ss-fff")),
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                Bytes = excelPackage.GetAsByteArray()
            };
        }

        //void SetTextCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isUnderlined = false)
        //{
        //    var titleCell = worksheet.Cells[cellRange];
        //    titleCell.Merge = true;
        //    titleCell.Value = text;
        //    titleCell.Style.Font.Size = fontSize;
        //    titleCell.Style.Font.Bold = isBold;
        //    titleCell.Style.Font.UnderLine = isUnderlined;
        //    titleCell.AutoFitColumns();
        //}

        // Helper method to set merged cell with text and formatting
        void SetTextCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isCenter = false, bool isUnderlined = false)
        {
            var titleCell = worksheet.Cells[cellRange];
            titleCell.Merge = true;
            titleCell.Value = text;
            titleCell.Style.Font.Size = fontSize;
            titleCell.Style.Font.Bold = isBold;
            titleCell.Style.Font.UnderLine = isUnderlined;
            titleCell.Style.HorizontalAlignment = isCenter ? ExcelHorizontalAlignment.Center : ExcelHorizontalAlignment.Left;
            titleCell.Style.VerticalAlignment = isCenter ? ExcelVerticalAlignment.Center : ExcelVerticalAlignment.Bottom;
            titleCell.AutoFitColumns();
        }

        void SetHeaderTableCell(ExcelWorksheet worksheet)
        {
            var headers = new[]
            {
                "Seq", "Product ID ", "Item code", "Item #", "Product Description", "Finish", "Box Dimension",
                "Box Volume", "GW", "Unit Price", "Qty", "PKGS", "Total amount (FOB)",
                "Total Vol. (m3)", "Total GW (kgs)", "Total GW (lbs)", "Mirror Cm2", "Material", "HTS code"
            };

            var columnWidths = new[]
            {
                13,   // Seq
                33,  // Product ID
                20,  // Item code
                20,   // Item #
                60,  // Product Description
                17,  // Finish
                27,  // Box Dimension
                13,  // Box Volume
                13,   // GW
                13,  // Unit Price
                13,   // Qty
                15,   // PKGS
                17,  // Total amount (FOB)
                17,  // Total Vol. (m3)
                17,  // Total GW (kgs)
                17,  // Total GW (lbs)
                13,  // Mirror Cm2
                13,  // Material
                13   // HTS code
            };


            for (int i = 0; i < headers.Length; i++)
            {
                var cell = worksheet.Cells[14, i + 1];
                cell.Value = headers[i];
                cell.Style.Font.Bold = i == 0 ? false : true;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Center the text
                cell.AutoFitColumns();

                // Set column width (adjust the value as needed)
                worksheet.Column(i + 1).Width = columnWidths[i];
            }
        }

        void SetDataTableCell(ExcelWorksheet worksheet, int row, CIPLResultModel.CIPLDetailGetByIdResultModel ciplDetail)
        {
            var item = GetItem(ciplDetail.ItemId);

            worksheet.Cells[row, 2].Value = ciplDetail?.ProductId.ToString() ?? ""; //Product ID
            worksheet.Cells[row, 3].Value = ciplDetail.ItemCode ?? ""; //Item code
            worksheet.Cells[row, 4].Value = ciplDetail.ItemId.ToString() ?? ""; //Item #
            worksheet.Cells[row, 5].Value = ciplDetail?.ProductName ?? ""; //Product Description
            worksheet.Cells[row, 6].Value = ""; //Finish
            worksheet.Cells[row, 7].Value = item != null ? item.BoxDimension : ""; //Box Dimension (inch)
            worksheet.Cells[row, 8].Value = item != null ? item.BoxWeight : ""; //Box Volume
            worksheet.Cells[row, 9].Value = ""; //GW (lbs)
            worksheet.Cells[row, 10].Value = ciplDetail?.UnitPrice.ToString() ?? ""; //Unit Price
            worksheet.Cells[row, 11].Value = ciplDetail?.Quantity.ToString() ?? ""; //Qty
            worksheet.Cells[row, 11].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[row, 12].Value = ""; //PKGS
            worksheet.Cells[row, 13].Value = "$  " + ciplDetail?.Money ?? ""; //Total amount (FOB)
            worksheet.Cells[row, 13].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[row, 14].Value = ""; //Total Vol. (m3)
            worksheet.Cells[row, 15].Value = ""; //Total GW (kgs)
            worksheet.Cells[row, 16].Value = ""; //Total GW (lbs)
            worksheet.Cells[row, 17].Value = ""; // Mirror Cm2
            worksheet.Cells[row, 18].Value = ""; //Material
            worksheet.Cells[row, 19].Value = ""; //HTS code

            var rowRange = worksheet.Cells[row, 1, row, worksheet.Dimension.End.Column];
            //rowRange.AutoFitColumns();
            worksheet.Row(row).Style.Font.Size = 11;
        }

        private Item GetItem(Guid? itemId)
        {
            return _unitOfWork.ItemRepository.GetFirstOrDefault(i => i.Id == itemId);
        }

        private Customer GetCustomer(Guid customerId)
        {
            return _unitOfWork.CustomerRepository.GetFirstOrDefault(i => i.Id == customerId);
        }
    }
}
