﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using static Ngochau.Web.Model.Cpanel.ParamModels.PurchaseOrderParamModel;
using Ngochau.Models;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class POService : IPOService
    {
        private IUnitOfWork _unitOfWork;

        public POService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var masterOrder = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Id == id);
                if (masterOrder != null)
                {
                    _unitOfWork.PODetailRepository.RemoveRange(masterOrder.PODetails);
                    _unitOfWork.PORepository.Removed(masterOrder);
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.ResultType = 200;
                    idResult.Id = id;
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
                _unitOfWork.RollbackTransaction();
            }

            return idResult;
        }

        public PagedResult<MasterOrderResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize)
        {
            var query = _unitOfWork.PORepository.GetAll();

            query = query.OrderByDescending(x => x.CreateDate).Where(x => (string.IsNullOrEmpty(keyword) || x.Code.Contains(keyword)
            || x.Name.Contains(keyword)) && (status == 0 || x.Status == status));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(x => new MasterOrderResultModel()
            {
                CustomerId = x.CustomerId,
                Code = x.Code,
                Name = x.Name,
                Id = x.Id,
                Status = x.Status,
                CreateDate = x.CreateDate != null ? ((DateTime)x.CreateDate).ToString("dd/MM/yyyy") : "",
                CreateTime = x.CreateDate != null ? ((DateTime)x.CreateDate).ToString("HH:mm") : "",
                CustomerCode = x.Customer.Code,
                CustomerName = x.Customer.Name,
                TotalDeposit = x.TotalDeposit,
                TotalMoney = x.TotalMoney,
                TotalQuantity = x.TotalQuantity
            }).ToList();
            var paginationSet = new PagedResult<MasterOrderResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public PagedResult<PIsResultModel> GetAllPisPagingAsync(string keyword, int status, int page, int pageSize)
        {
            var query = _unitOfWork.PIRepository.GetAll();

            query = query.OrderByDescending(x => x.CreateDate).Where(x => (string.IsNullOrEmpty(keyword) || (!string.IsNullOrEmpty(x.Code) && x.Code.Contains(keyword))
            || (!string.IsNullOrEmpty(x.Customer.Name) && x.Customer.Name.Contains(keyword))));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(x => new PIsResultModel()
            {
                CustomerId = x.CustomerId,
                PIId = x.Id,
                POCode = x.Code,
                //POId = x.POId,
                CreateDate = x.CreateDate != null ? ((DateTime)x.CreateDate).ToString("dd/MM/yyyy") : "",
                CreateTime = x.CreateDate != null ? ((DateTime)x.CreateDate).ToString("HH:mm") : "",
                //PiDate = x.PIDate != null ? ((DateTime)x.PIDate).ToString("dd/MM/yyyy") : "",
                //PiTime = x.PIDate != null ? ((DateTime)x.PIDate).ToString("HH:mm") : "",
                CustomerCode = x.Customer.Code,
                CustomerName = x.Customer.Name,
                TotalDeposit = x.TotalDeposit,
                TotalMoney = x.TotalMoney,
                TotalQuantity = x.TotalQuantity
            }).ToList();
            var paginationSet = new PagedResult<PIsResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public List<Models.NgoChauModel.PO> GetListPOByIds(Guid[] ids)
        {
            return _unitOfWork.PORepository.GetAll().Where(x => ids.Contains(x.Id)).ToList();
        }

        public List<Ngochau.Models.NgoChauModel.PI> GetListPIByIds(Guid[] ids)
        {
            return _unitOfWork.PIRepository.GetAll().Where(x => ids.Contains(x.Id)).ToList();
        }

        public async Task<MasterOrderItemResultModel> GetById(Guid Id)
        {
            MasterOrderItemResultModel mode = new MasterOrderItemResultModel();
            mode.PODetails = new List<MasterOrderDetailResultModel>();
            var res = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Id == Id);
            mode.Id = res.Id;
            mode.CustomerId = res.CustomerId;
            mode.Code = res.Code;
            mode.Name = res.Name;
            mode.TotalDeposit = res.TotalDeposit;
            mode.TotalMoney = res.TotalMoney;
            mode.TotalQuantity = res.TotalQuantity;
            mode.CreateDate = res.CreateDate;
            mode.Customer = res.Customer;
            mode.Status = res.Status;

            foreach (var item in res.PODetails)
            {
                MasterOrderDetailResultModel modt = new MasterOrderDetailResultModel();
                modt.Id = item.Id;
                //modt.POId = item.POId;
                modt.ProductId = item.ProductId;
                modt.ProductSKU = item.Product == null ? "" : item.Product.Code;
                modt.ProductName = item.Product == null ? "" : item.Product.Name;
                modt.ItemId = item.ItemId;
                modt.ItemCode = item.Item == null ? "" : item.Item.Code;
                modt.ItemName = item.Item == null ? "" : item.Item.Name;
                modt.ColorId = item.ColorId;
                modt.ColorCode = item.Color == null ? "" : item.Color.Code;
                modt.ColorName = item.Color == null ? "" : item.Color.Name;
                modt.UnitPrice = item.UnitPrice;
                modt.Deposit = item.Deposit;
                modt.CBM = item.CBM;
                modt.ColorFail = item.DescCodeColor;
                modt.DescriptionColorFail = item.DescColorFails; ;
                modt.TotalMoney = item.UnitPrice * item.Quantity;
                modt.Quantity = item.Quantity;
                //modt.Description = item.Description;
                //modt.ExtraCharge = item.ExtraCharge;
                //modt.BatchVolume = item.BatchVolume;
                modt.CreateDate = item.CreateDate;
                //modt.Status = item.Status;
                mode.PODetails.Add(modt);
            }
            return mode;
        }

        public async Task<PIResultModel> GetByPIId(Guid Id)
        {
            PIResultModel mode = new PIResultModel();
            mode.PIDetails = new List<PIDetailResultModel>();
            var res = _unitOfWork.PIRepository.GetFirstOrDefault(x => x.Id == Id);
            mode.PIId = res.Id;
            mode.POId = res.POId;
            mode.CustomerId = res.CustomerId;
            mode.TotalDeposit = res.TotalDeposit;
            mode.TotalMoney = res.TotalMoney;
            mode.TotalQuantity = res.TotalQuantity;
            //mode.PIDate = res.PIDate != null ? ((DateTime)res.PIDate).ToString("dd-MM-yyyy") : "";
            foreach (var item in res.PIDetails)
            {
                PIDetailResultModel modt = new PIDetailResultModel();

                modt.PIDetailId = item.Id;
                modt.ProductId = item.ProductId;
                modt.ProductSKU = item.Product.Code;
                modt.ProductName = item.Product.Name;
                modt.ItemId = item.ItemId;
                modt.ItemCode = item.Item.Code;
                modt.ItemName = item.Item.Name;
                modt.ColorId = item.ColorId;
                modt.ColorCode = item.Color.Code;
                modt.UnitPrice = item.UnitPrice;
                modt.Deposit = item.Deposit;
                modt.TotalMoney = item.UnitPrice * item.Quantity;
                modt.Quantity = item.Quantity;
                mode.PIDetails.Add(modt);
            }
            return mode;
        }

        public List<MasterOrderItemResultModel> GetByIds(Guid[] Ids)
        {
            List<MasterOrderItemResultModel> resultModel = new List<MasterOrderItemResultModel>();

            foreach (var id in Ids)
            {
                MasterOrderItemResultModel mode = new MasterOrderItemResultModel();
                mode.PODetails = new List<MasterOrderDetailResultModel>();
                var res = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Id == id);
                mode.Id = res.Id;
                mode.CustomerId = res.CustomerId;
                mode.Code = res.Code;
                mode.Name = res.Name;
                mode.TotalDeposit = res.TotalDeposit;
                mode.TotalMoney = res.TotalMoney;
                mode.TotalQuantity = res.TotalQuantity;
                mode.CreateDate = res.CreateDate;
                mode.Customer = res.Customer;
                mode.Status = res.Status;
                foreach (var item in res.PODetails)
                {
                    MasterOrderDetailResultModel modt = new MasterOrderDetailResultModel();

                    modt.Id = item.Id;
                    //modt.POId = item.POId;
                    modt.ProductId = item.ProductId;
                    modt.ProductSKU = item.Product == null ? "" : item.Product.Code;
                    modt.ProductName = item.Product == null ? "" : item.Product.Name;
                    //modt.ContainerId = item.ContainerId;
                    //modt.ContainerCode = item.Container.Code;
                    //modt.ContainerName = item.Container.Name;
                    modt.ItemId = item.ItemId;
                    modt.ItemCode = item.Item == null ? "" : item.Item.Code;
                    modt.ItemName = item.Item == null ? "" : item.Item.Name;
                    modt.ColorId = item.ColorId;
                    modt.ColorCode = item.Color == null ? "" : item.Color.Code;
                    modt.ColorName = item.Color == null ? "" : item.Color.Name;
                    modt.UnitPrice = item.UnitPrice;
                    modt.Deposit = item.Deposit;
                    modt.CBM = item.CBM;
                    modt.ColorFail = item.DescCodeColor;
                    modt.DescriptionColorFail = item.DescColorFails;
                    //modt.TotalMoney = item.TotalMoney;
                    modt.Quantity = item.Quantity;
                    //modt.Description = item.Description;
                    //modt.ExtraCharge = item.ExtraCharge;
                    //modt.BatchVolume = item.BatchVolume;
                    modt.CreateDate = item.CreateDate;
                    //modt.Status = item.Status;
                    mode.PODetails.Add(modt);
                }
                resultModel.Add(mode);
            }
            return resultModel;
        }

        public List<Models.NgoChauModel.PO> GetPOs(string keyword = null)
        {
            return _unitOfWork.PORepository.GetAll().Where(x => (keyword == null || x.Code == keyword || x.Name.Contains(keyword))).ToList();
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(MasterOrderParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultMasterOrder = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    //Lưu thông tin master order
                    Models.NgoChauModel.PO mso = new Models.NgoChauModel.PO();
                    mso.Id = Guid.NewGuid();
                    mso.Code = model.Code;
                    mso.Name = model.Name;
                    mso.Status = model.Status;
                    mso.CustomerId = model.CustomerId;
                    mso.CreateDate = DateTime.Now;
                    mso.TotalDeposit = model.MasterOrderDetailParamModel.Sum(x => x.Deposit);
                    mso.TotalQuantity = model.MasterOrderDetailParamModel.Sum(x => x.Quantity);
                    mso.TotalMoney = model.MasterOrderDetailParamModel.Sum(x => x.TotalMoney);
                    mso.CreateDate = DateTime.Now;

                    _unitOfWork.PORepository.Add(mso);

                    foreach (var item in model.MasterOrderDetailParamModel)
                    {
                        PODetail msod = new PODetail();
                        msod.Id = Guid.NewGuid();
                        msod.POId = mso.Id;
                        //Tìm kiếm Item
                        //var ItemCode = !string.IsNullOrEmpty(item.StrItem) ? SplipStringCode(item.StrItem) : "";
                        Item itemResFind = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Code == item.StrItem);
                        if (itemResFind == null)
                        {
                            idResult.ResultType = 500;
                            _unitOfWork.RollbackTransaction();
                            return idResult;
                        }
                        //Tìm kiếm Product
                        var productCode = !string.IsNullOrEmpty(item.StrProduct) ? SplipStringCode(item.StrProduct, "|") : "";
                        Product prodResFind = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Code == productCode);
                        if (prodResFind == null)
                        {
                            idResult.ResultType = 500;
                            _unitOfWork.RollbackTransaction();
                            return idResult;
                        }
                        //Tìm kiếm màu
                        var colorCode = !string.IsNullOrEmpty(item.StrColor) ? SplipStringCode(item.StrColor, "|") : "";
                        Models.NgoChauModel.Color colorResFind = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Code == colorCode);
                        if (colorResFind == null)
                        {
                            idResult.ResultType = 500;
                            _unitOfWork.RollbackTransaction();
                            return idResult;
                        }

                        msod.ItemId = itemResFind.Id;
                        msod.ProductId = prodResFind.Id;
                        msod.ColorId = colorResFind.Id;
                        msod.Quantity = item.Quantity;
                        msod.UnitPrice = (decimal)item.UnitPrice;
                        msod.CBM = item.CBM != null ? decimal.Parse(item.CBM) : itemResFind.CBM;
                        msod.Deposit = item.Deposit;
                        //msod.TotalMoney = item.TotalMoney;
                        msod.CreateDate = DateTime.Now;
                        _unitOfWork.PODetailRepository.Add(msod);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = mso.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> InsertPI(PIParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultPI = _unitOfWork.PIRepository.GetFirstOrDefault(x => x.POId == model.POId);
                if (resultPI != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    //Lưu thông tin master order
                    PI mso = new PI();
                    mso.Id = Guid.NewGuid();
                    var po = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Id == model.POId);
                    if (po != null)
                    {
                        mso.Code = po.Code;
                        mso.POId = po.Id;
                        mso.Name = po.Name;
                    }

                    mso.CustomerId = model.CustomerId;
                    mso.PIDate = DateTime.Now;
                    mso.TotalDeposit = model.PIDetailParamModel.Sum(x => x.Deposit);
                    mso.TotalQuantity = model.PIDetailParamModel.Sum(x => x.Quantity);
                    mso.TotalMoney = model.PIDetailParamModel.Sum(x => x.TotalMoney);
                    mso.CreateDate = DateTime.Now;

                    _unitOfWork.PIRepository.Add(mso);

                    foreach (var item in model.PIDetailParamModel)
                    {
                        Models.NgoChauModel.PIDetail piD = new Models.NgoChauModel.PIDetail();
                        piD.Id = Guid.NewGuid();
                        piD.PIId = mso.Id;
                        PODetail PODetail = _unitOfWork.PODetailRepository.GetFirstOrDefault(x => x.Id == item.MasterOrderDetailId);
                        if (PODetail != null)
                        {
                            piD.CBM = PODetail.CBM;
                            piD.ProductId = PODetail.Product.Id;
                            piD.ColorId = PODetail.ColorId;
                            piD.ItemId = PODetail.ItemId;
                        }
                        piD.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        piD.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        piD.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        piD.Quantity = item.Quantity;
                        piD.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        piD.Deposit = item.Deposit;
                        _unitOfWork.PIDetailRepository.Add(piD);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = mso.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        private string SplipStringCode(string strParam, string split)
        {
            string result = string.Empty;

            string[] strItemRes = strParam.Split(split);

            if (strItemRes.Length > 0)
                result = strItemRes[0].Trim();
            return result;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(MasterOrderParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                if (model.Id == Guid.Empty || model.Id == null)
                {
                    idResult.ResultType = 404;

                }
                var reMasterOrder = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Id == model.Id);
                if (reMasterOrder != null)
                {
                    reMasterOrder.Name = model.Name;
                    reMasterOrder.Code = model.Code;
                    reMasterOrder.CustomerId = model.CustomerId;
                    reMasterOrder.TotalQuantity = model.MasterOrderDetailParamModel.Sum(x => x.Quantity);
                    reMasterOrder.TotalDeposit = model.MasterOrderDetailParamModel.Sum(x => x.Deposit);
                    reMasterOrder.TotalMoney = model.MasterOrderDetailParamModel.Sum(x => x.TotalMoney);
                    reMasterOrder.Status = model.Status;
                    _unitOfWork.PORepository.Update(reMasterOrder);


                    foreach (var item in model.MasterOrderDetailParamModel)
                    {
                        PODetail msod = new PODetail();
                        msod = _unitOfWork.PODetailRepository.GetFirstOrDefault(x => x.Id == item.MasterOrderDetailId);
                        if (msod != null) // Update
                        {
                            //Tìm kiếm Product
                            var productCode = !string.IsNullOrEmpty(item.StrProduct) ? SplipStringCode(item.StrProduct, "|") : "";
                            Product prodResFind = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Code == productCode);
                            if (prodResFind == null)
                            {
                                idResult.ResultType = 500;
                                _unitOfWork.RollbackTransaction();
                                return idResult;
                            }
                            //Tìm kiếm color
                            var colorCode = !string.IsNullOrEmpty(item.StrColor) ? SplipStringCode(item.StrColor, "|") : "";
                            Models.NgoChauModel.Color colorResFind = _unitOfWork.ColorRepository.GetFirstOrDefault(x => x.Code == colorCode);
                            if (colorResFind == null)
                            {
                                idResult.ResultType = 500;
                                _unitOfWork.RollbackTransaction();
                                return idResult;
                            }
                            //Tìm kiếm Item
                            Item itemResFind = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.Code == item.StrItem);
                            if (itemResFind == null)
                            {
                                idResult.ResultType = 500;
                                _unitOfWork.RollbackTransaction();
                                return idResult;
                            }
                            msod.ItemId = itemResFind.Id;
                            msod.ProductId = prodResFind.Id;
                            msod.ColorId = colorResFind.Id;
                            msod.Quantity = item.Quantity;
                            msod.UnitPrice = (decimal)item.UnitPrice;
                            msod.CBM = item.CBM != null ? decimal.Parse(item.CBM) : itemResFind.CBM;
                            msod.Deposit = item.Deposit;
                            //msod.TotalMoney = item.TotalMoney;
                            _unitOfWork.PODetailRepository.Update(msod);
                        }
                        else // Thêm mới
                        {

                        }
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                    return idResult;
                }
                else
                {
                    idResult.ResultType = 404;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> InsertPDFExcel(PurchaseOrderParamModel.PO model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultMasterOrder = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Code == model.PurchaseOrder);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    //Lưu thông tin master order
                    Models.NgoChauModel.PO mso = new Models.NgoChauModel.PO();
                    mso.Id = Guid.NewGuid();
                    mso.Code = model.PurchaseOrder;
                    mso.Name = "Purchase Order " + model.PurchaseOrder;
                    mso.Status = 1;
                    mso.CustomerId = model.CustomerId;
                    Customer customer = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Id == model.CustomerId);
                    //Xử lý lấy % customer
                    if (customer != null)
                    {
                        decimal deposit = (decimal)(model.Products.Sum(x => x.TotalAmonut) * customer.Deposit) / 100;
                        mso.TotalDeposit = deposit;
                    }

                    mso.TotalQuantity = model.Products.Sum(x => x.UnitsOrdered);
                    mso.TotalMoney = model.Products.Sum(x => x.TotalAmonut);
                    mso.CreateDate = DateTime.Now;
                    _unitOfWork.PORepository.Add(mso);

                    foreach (var item in model.Products)
                    {
                        PODetail msod = new PODetail();
                        msod.Id = Guid.NewGuid();
                        msod.POId = mso.Id;

                        msod.ProductId = InsertUpdateProduct(item);
                        // Kiểm tra xem giá có giống không?
                        Tuple<string, string, Guid, decimal?> tup = CheckItemPrice(item, msod.ProductId);
                        msod.ItemId = tup.Item3;
                        msod.DescCodeColor = tup.Item1;
                        msod.DescColorFails = tup.Item2;
                        msod.Quantity = item.UnitsOrdered;
                        msod.UnitPrice = item.UnitCost;
                        msod.UnitPriceOld = tup.Item4;
                        msod.CBM = item.UnitCBM;
                        msod.Deposit = (item.UnitCost * item.UnitsOrdered * customer.Deposit) / 100;
                        //msod.TotalMoney = item.UnitCost * item.UnitsOrdered;
                        msod.CreateDate = DateTime.Now;
                        _unitOfWork.PODetailRepository.Add(msod);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = mso.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        //public async Task<CreateUpdateDeleteResultModel<Guid>> InsertPDFExcel(Models.NgoChauModel.PO model)
        //{
        //    CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
        //    _unitOfWork.BeginTransaction();
        //    try
        //    {
        //        var resultMasterOrder = _unitOfWork.PORepository.GetFirstOrDefault(x => x.Code == model.Code);
        //        if (resultMasterOrder != null)
        //        {
        //            idResult.ResultType = 416;
        //            _unitOfWork.RollbackTransaction();
        //        }
        //        else
        //        {
        //            //Lưu thông tin master order
        //            Models.NgoChauModel.PO mso = new Models.NgoChauModel.PO();
        //            mso.Id = Guid.NewGuid();
        //            mso.Code = model.Code;
        //            mso.Name = "Purchase Order " + model.Code;
        //            mso.Status = 1;
        //            mso.CustomerId = model.CustomerId;
        //            Customer customer = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Id == model.CustomerId);
        //            //Xử lý lấy % customer
        //            if (customer != null)
        //            {
        //                decimal deposit = (decimal)(model.PODetails.Sum(x => x.Money) * customer.Deposit) / 100;
        //                mso.TotalDeposit = deposit;
        //            }

        //            mso.TotalQuantity = model.PODetails.Sum(x => x.Quantity);
        //            mso.TotalMoney = model.PODetails.Sum(x => x.Money);
        //            mso.CreateDate = DateTime.Now;
        //            _unitOfWork.PORepository.Add(mso);

        //            foreach (var item in model.PODetails)
        //            {
        //                PODetail msod = new PODetail();
        //                msod.Id = Guid.NewGuid();
        //                msod.POId = mso.Id;

        //                msod.ProductId = InsertUpdateProduct(item.Product);
        //                // Kiểm tra xem giá có giống không?
        //                //Tuple<string, string, Guid, decimal?> tup = CheckItemPrice(model.PODetails, msod.ProductId);
        //                //msod.ItemId = tup.Item3;
        //                //msod.DescColorFails = tup.Item1;
        //                //msod.DescColorFails = tup.Item2;
        //                //msod.Quantity = item.UnitsOrdered;
        //                //msod.UnitPrice = item.UnitCost;
        //                //msod.UnitPriceOld = tup.Item4;
        //                //msod.CBM = item.UnitCBM;
        //                //msod.Deposit = (item.UnitCost * item.UnitsOrdered * customer.Deposit) / 100;
        //                //msod.TotalMoney = item.UnitCost * item.UnitsOrdered;
        //                msod.CreateDate = DateTime.Now;
        //                _unitOfWork.PODetailRepository.Add(msod);
        //            }
        //            _unitOfWork.Save();
        //            _unitOfWork.CommitTransaction();
        //            idResult.Id = mso.Id;
        //            idResult.ResultType = 200;
        //        }
        //    }
        //    catch
        //    {
        //        idResult.ResultType = 500;
        //        _unitOfWork.RollbackTransaction();
        //    }
        //    return idResult;
        //}
        private Guid InsertUpdateProduct(ProductItem model)
        {
            Product product = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Code == model.ProductBarCode);
            if (product == null)
            {
                Product prod = new Product();
                prod.Id = Guid.NewGuid();
                prod.Code = model.ProductBarCode;
                prod.Desc = model.Description;
                prod.Name = model.ProductName;
                prod.DateCreated = DateTime.Now;
                _unitOfWork.ProductRepository.Add(prod);
                return prod.Id;
            }
            else
            {
                return product.Id;
            }
        }

        private Tuple<string, string, Guid, decimal?> CheckItemPrice(ProductItem model, Guid productId)
        {
            Item item = _unitOfWork.ItemRepository.GetFirstOrDefault(x => x.ProductId == productId);
            if (item != null)
            {
                if (item.PriceNew != model.UnitCost)
                {
                    return new Tuple<string, string, Guid, decimal?>("FF0000", "Giá không giống với hệ thống", item.Id, item.PriceNew);
                }
                else
                {
                    return new Tuple<string, string, Guid, decimal?>("DFDFDF", "Không có gì thay đổi", item.Id, item.PriceNew);
                }
            }
            else
            {
                Item itemNew = new Item();
                itemNew.Id = Guid.NewGuid();
                itemNew.ProductId = productId;
                itemNew.Code = model.ItemCode;
                itemNew.Name = model.ItemCode + " - " + model.ProductCode;
                itemNew.PriceNew = model.UnitCost;
                itemNew.CBM = model.UnitCBM;
                itemNew.MPN = model.MPN;
                _unitOfWork.ItemRepository.Add(itemNew);
                // Thêm mới Item và giá vào
                return new Tuple<string, string, Guid, decimal?>("Blue", "Giá không có trong hệ thống được thêm mới vào", itemNew.Id, 0);
            }

        }

        public Task<CreateUpdateDeleteResultModel<Guid>> UpdatePI(PIParamModel model)
        {
            throw new NotImplementedException();
        }


        public List<Ngochau.Models.NgoChauModel.PO> GetListPOByCustomerId(Guid customerId)
        {
            return _unitOfWork.PORepository.GetAll().Where(mo => mo.CustomerId == customerId).ToList();
        }
    }
}
