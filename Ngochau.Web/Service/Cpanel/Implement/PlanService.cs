﻿using DocumentFormat.OpenXml.VariantTypes;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.Enum;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Org.BouncyCastle.Asn1.Ocsp;
using static Ngochau.Web.Model.ResultModels.PlanResultModel;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class PlanService : IPlanService
    {
        private IUnitOfWork _unitOfWork;

        public PlanService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<PlanResultModel.GetPOForPlanResultModel> GetPOForPlan(Guid poId)
        {
            PlanResultModel.GetPOForPlanResultModel result = new PlanResultModel.GetPOForPlanResultModel();

            var po = _unitOfWork.PORepository.GetFirstOrDefault(po => po.Id == poId);

            if (po != null)
            {
                result.POId = po.Id;
                result.Code = po.Code;
                result.Name = po.Name;
                result.Desc = po.Desc;
                result.CustomerId = po.CustomerId;
                result.TotalDeposit = po.TotalDeposit;
                result.TotalMoney = po.TotalMoney;
                result.TotalQuantity = po.TotalQuantity;
                result.CreateDate = po.CreateDate;
                result.Status = po.Status;

                foreach (var item in po.PODetails.OrderBy(po => po.CreateDate))
                {
                    int? productQuantity = _unitOfWork.PODetailRepository.GetFirstOrDefault(mo => mo.ProductId == item.ProductId).Quantity;
                    int? quantityOldPlan = _unitOfWork.PlanDetailRepository.GetAll().Where(p => p.ProductId == item.ProductId)?.Sum(p => p.Quantity);

                    int remainQuantity = (productQuantity ?? 0) - (quantityOldPlan ?? 0);

                    if (remainQuantity == 0) continue;

                    var product = _unitOfWork.ProductRepository.GetFirstOrDefault(mo => mo.Id == item.ProductId);
                    var color = _unitOfWork.ColorRepository.GetFirstOrDefault(mo => mo.Id == item.ColorId);
                    var itemData = _unitOfWork.ItemRepository.GetFirstOrDefault(mo => mo.Id == item.ItemId);

                    GetPOForPlanDetailResultModel poDetail = new GetPOForPlanDetailResultModel();
                    poDetail.PODetailId = item.Id;
                    poDetail.POId = item.POId;
                    poDetail.ProductId = item.ProductId;
                    poDetail.ProductCode = product.Code;
                    poDetail.ProductName = product.Name;
                    poDetail.ItemId = item.ItemId;
                    poDetail.ItemCode = itemData.Code;
                    poDetail.ItemName = itemData.Name;
                    poDetail.ColorId = item.ColorId;
                    poDetail.ColorCode = color.Code;
                    poDetail.ColorName = color.Name;

                    poDetail.Quantity = item.Quantity;
                    poDetail.RemainQuantity = remainQuantity;

                    poDetail.CBM = item.CBM;

                    poDetail.Desc = item.Desc;
                    poDetail.Deposit = item.Deposit;
                    poDetail.Money = item.Money;
                    poDetail.UnitPrice = item.UnitPrice;
                    poDetail.UnitPriceOld = item.UnitPriceOld;

                    poDetail.CreateDate = item.CreateDate;

                    poDetail.DescCodeColor = item.DescCodeColor;
                    poDetail.DescColorFails = item.DescColorFails;

                    result.PODetails.Add(poDetail);
                }
            }

            return result;
        }

        public List<PlanResultModel.PlanGetAllResultModel> GetAll()
        {
            var query = _unitOfWork.PlanRepository.GetAll();

            return query
                .Select(p => new PlanResultModel.PlanGetAllResultModel()
                {
                    Id = p.Id,
                    Code = p.Code,
                    Name = p.Name
                }).ToList();
        }

        public PagedResult<PlanResultModel.PlanGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            var query = _unitOfWork.PlanRepository.GetAll();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(q => q.Name.Contains(keyword) || q.Code.Contains(keyword));
            }

            if (status != null)
            {
                query = query;
            }

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(q => new PlanResultModel.PlanGetAllPagingResultModel()
            {
                Id = q.Id,
                Code = q.Code,
                Name = q.Name,
                //PlanDate = q.PlanDate != null ? ((DateTime)q.PlanDate).ToString("dd/MM/yyyy") : "",
                CreateDate = q.CreateDate != null ? ((DateTime)q.CreateDate).ToString("dd/MM/yyyy") : "",
                TotalDeposit = q.TotalDeposit,
                TotalMoney = q.TotalMoney,
                TotalQuantity = q.TotalQuantity,
                //Status = q.Status,
                //StatusName = q.Status == 0 ? "Mới" : "Đóng",
            }).ToList();

            var paginationSet = new PagedResult<PlanResultModel.PlanGetAllPagingResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public async Task<PlanResultModel.PlanGetByIdResultModel> GetById(Guid Id)
        {
            PlanResultModel.PlanGetByIdResultModel planResult = new PlanResultModel.PlanGetByIdResultModel();
            var plan = _unitOfWork.PlanRepository.GetFirstOrDefault(x => x.Id == Id);
            if (plan != null)
            {
                planResult.PlanId = plan.Id;
                planResult.Code = plan.Code;
                planResult.Name = plan.Name;
                planResult.CustomerId = plan.CustomerId;
                planResult.TotalDeposit = plan.TotalDeposit;
                planResult.TotalMoney = plan.TotalMoney;
                planResult.TotalQuantity = plan.TotalQuantity;
                planResult.CreateDate = plan.CreateDate;

                var pos = _unitOfWork.PORepository.GetAll().Select(mo => new { mo.Id, mo.Code }).ToList();
                foreach (var item in plan.PlanDetails.OrderBy(p => p.CreateDate))
                {
                    int? productQuantity = _unitOfWork.PODetailRepository.GetFirstOrDefault(mo => mo.ProductId == item.ProductId).Quantity;
                    int? quantityOldPlan = _unitOfWork.PlanDetailRepository.GetAll().Where(p => p.ProductId == item.ProductId)?.Sum(p => p.Quantity);

                    int remainQuantityPO = (productQuantity ?? 0) - (quantityOldPlan ?? 0);
                    int remainQuantityPlan = (item.Quantity ?? 0) + remainQuantityPO;

                    var product = _unitOfWork.ProductRepository.GetFirstOrDefault(mo => mo.Id == item.ProductId);
                    var color = _unitOfWork.ColorRepository.GetFirstOrDefault(mo => mo.Id == item.ColorId);
                    var itemData = _unitOfWork.ItemRepository.GetFirstOrDefault(mo => mo.Id == item.ItemId);

                    PlanResultModel.PlanDetailGetByIdResultModel planDetailResult = new PlanResultModel.PlanDetailGetByIdResultModel();
                    planDetailResult.PlanDetailId = item.Id;
                    planDetailResult.PlanId = item.PlanId;

                    planDetailResult.POId = item.POId;
                    planDetailResult.POCode = pos.Where(mo => mo.Id == item.POId).FirstOrDefault()?.Code;

                    planDetailResult.ProductId = item.ProductId;
                    planDetailResult.ProductCode = product.Code;
                    planDetailResult.ProductName = product.Name;

                    planDetailResult.ColorId = item.ColorId;
                    planDetailResult.ColorCode = color.Code;
                    planDetailResult.ColorName = color.Name;

                    planDetailResult.ItemId = item.ItemId;
                    planDetailResult.ItemCode = itemData.Code;
                    planDetailResult.ItemName = itemData.Name;

                    planDetailResult.Quantity = item.Quantity;
                    planDetailResult.RemainQuantity = remainQuantityPlan;

                    planDetailResult.Quantity = item.Quantity;
                    planDetailResult.Deposit = item.Deposit;
                    planDetailResult.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                    planDetailResult.Money = item.Money;

                    planDetailResult.CBM = item.CBM;

                    planDetailResult.DescCodeColor = item.DescCodeColor;
                    planDetailResult.DescColorFails = item.DescColorFails;

                    planResult.PlanDetails.Add(planDetailResult);
                }
            }

            return planResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> InsertPlan(PlanParamModel.PlanInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultPlan = _unitOfWork.PlanRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultPlan != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    //Lưu thông tin master order
                    Plan plan = new Plan();
                    plan.Id = Guid.NewGuid();
                    plan.Code = model.Code;
                    plan.Name = model.Name;
                    plan.CustomerId = model.CustomerId;
                    //plan.PlanDate = model.PlanDate != null ? model.PlanDate : DateTime.Now;
                    //plan.DueDate = model.DueDate != null ? model.DueDate : DateTime.Now;
                    plan.TotalDeposit = model.PlanDetails.Sum(x => x.Deposit);
                    plan.TotalQuantity = model.PlanDetails.Sum(x => x.Quantity);
                    plan.TotalMoney = model.PlanDetails.Sum(x => x.TotalMoney);
                    plan.CreateDate = DateTime.Now;
                    //plan.Status = (int)CommonStatus.New;

                    _unitOfWork.PlanRepository.Add(plan);

                    foreach (var item in model.PlanDetails)
                    {
                        PlanDetail planDetail = new PlanDetail();
                        planDetail.Id = Guid.NewGuid();
                        planDetail.PlanId = plan.Id;
                        planDetail.POId = item.POId;
                        PODetail PODetail = _unitOfWork.PODetailRepository.GetFirstOrDefault(x => x.Id == item.PODetailId);
                        if (PODetail != null)
                        {
                            planDetail.CBM = PODetail.CBM;
                            planDetail.DescCodeColor = PODetail.DescCodeColor;
                            planDetail.DescColorFails = PODetail.DescColorFails;
                        }
                        planDetail.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        planDetail.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        planDetail.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        planDetail.Quantity = item.Quantity;
                        planDetail.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        planDetail.Deposit = item.Deposit;
                        planDetail.Money = item.TotalMoney;
                        planDetail.CreateDate = DateTime.Now;
                        _unitOfWork.PlanDetailRepository.Add(planDetail);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = plan.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(PlanParamModel.PlanInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                if (model.PlanId == Guid.Empty || model.PlanId == null)
                {
                    idResult.ResultType = 404;
                    _unitOfWork.RollbackTransaction();
                }
                var plan = _unitOfWork.PlanRepository.GetFirstOrDefault(x => x.Id == model.PlanId);
                if (plan != null)
                {
                    plan.Name = model.Name;
                    plan.Code = model.Code;
                    plan.TotalQuantity = model.PlanDetails.Sum(x => x.Quantity);
                    plan.TotalDeposit = model.PlanDetails.Sum(x => x.Deposit);
                    plan.TotalMoney = model.PlanDetails.Sum(x => x.TotalMoney);

                    _unitOfWork.PlanRepository.Update(plan);

                    //Remove all before insert new list plan detail
                    var listPlanDetail = _unitOfWork.PlanDetailRepository.GetAll().Where(pd => pd.PlanId == model.PlanId).ToList();
                    _unitOfWork.PlanDetailRepository.RemoveRange(listPlanDetail);

                    foreach (var item in model.PlanDetails)
                    {
                        PlanDetail planDetailInsert = new PlanDetail();
                        planDetailInsert.Id = Guid.NewGuid();
                        planDetailInsert.PlanId = plan.Id;
                        planDetailInsert.POId = item.POId;
                        //planDetailInsert.CBM = item.CBM;
                        planDetailInsert.DescCodeColor = item.DescCodeColor;
                        planDetailInsert.DescColorFails = item.DescColorFails;
                        planDetailInsert.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        planDetailInsert.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        planDetailInsert.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        planDetailInsert.Quantity = item.Quantity;
                        planDetailInsert.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        planDetailInsert.Deposit = item.Deposit;
                        planDetailInsert.Money = item.TotalMoney;
                        _unitOfWork.PlanDetailRepository.Add(planDetailInsert);
                    }

                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = plan.Id;
                    idResult.ResultType = 200;
                    return idResult;
                }
                else
                {
                    idResult.ResultType = 404;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        private string SplipStringCode(string strParam, string split)
        {
            string result = string.Empty;

            string[] strItemRes = strParam.Split(split);

            if (strItemRes.Length > 0)
                result = strItemRes[0].Trim();
            return result;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> DeletePlan(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var existCIPL = _unitOfWork.CIPLDetailRepository.GetAll().Where(x => x.PlanId == id).Any();
                if (existCIPL == false)
                {
                    var plan = _unitOfWork.PlanRepository.GetFirstOrDefault(x => x.Id == id);
                    var planDetails = _unitOfWork.PlanDetailRepository.GetAll().Where(x => x.PlanId == id).ToList();
                    if (plan != null && planDetails.Count > 0)
                    {
                        _unitOfWork.PlanDetailRepository.RemoveRange(planDetails);
                        _unitOfWork.PlanRepository.Removed(plan);
                        _unitOfWork.Save();
                        _unitOfWork.CommitTransaction();
                        idResult.ResultType = 200;
                        idResult.Id = id;
                    }
                    else
                    {
                        idResult.ResultType = 404;
                        idResult.Id = id;
                        _unitOfWork.RollbackTransaction();
                    }
                }
                else
                {
                    idResult.ResultType = null;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
                _unitOfWork.RollbackTransaction();
            }

            return idResult;
        }

        public List<Plan> GetListPlanByIds(Guid[] ids)
        {
            return _unitOfWork.PlanRepository.GetAll().Where(x => ids.Contains(x.Id)).ToList();
        }

        public List<Ngochau.Models.NgoChauModel.Plan> GetListPlanByCustomerId(Guid customerId)
        {
            return _unitOfWork.PlanRepository.GetAll().Where(mo => mo.CustomerId == customerId).ToList();
        }


        public async Task<ExcelFileData> ExportPlan(Guid planId)
        {
            var plan = await GetById(planId);

            ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("FEB");

            // Set the font to Times New Roman for the entire worksheet
            ws.Cells.Style.Font.Name = "Times New Roman";

            // Define the title and address information
            var title = "NGO CHAU TRADING COMPANY LIMITED";
            var address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
            var contact = "Tel: (+84 8) 3730 9465 | Fax: (+84 8) 3730 8465";
            var title2 = "Plan";

            SetTextCell(ws, "A2:L2", title, 12, false, true);
            SetTextCell(ws, "A3:L3", address, 12, false, true);
            SetTextCell(ws, "A4:L4", contact, 12, false, true);
            SetTextCell(ws, "A6:L6", title2, 12, true, true);

            SetTextCell(ws, "B8", "PO#", 11, true, false, true);
            SetTextCell(ws, "M8", "PO ETD:", 11, true, false, true);

            SetTextCell(ws, "F7", "Kiểm cuối", 11, false, false, false);
            SetTextCell(ws, "F8", "Date: " + plan.CreateDate?.ToString("dd/MM/yyyy"), 12, true);

            var customer = GetCustomer(plan.CustomerId.Value);
            SetTextCell(ws, "A10", "Customer:", 11, false, false, true);
            SetTextCell(ws, "A11:B11", customer.Name, 11, false, false, false);
            SetTextCell(ws, "A12:B12", customer.CustomerAddress, 11, false, false, false);
            SetTextCell(ws, "A13:B13", "P: " + customer.Phone + " | F: " + customer.Fax, 11, false, false, false);

            SetTextCell(ws, "F10", "PO#", 11, true, false, false);
            SetTextCell(ws, "F11", "", 11, false, false, false); //*
            SetTextCell(ws, "F12", "PO Date", 11, true, false, true);
            SetTextCell(ws, "F13", "", 11, false, false, false); //*

            //SetTextCell(ws, "G10", "CONT# KOCU4088423", 11, true, false, false);
            //SetTextCell(ws, "G11", "SEAL# 23 0136605", 11, true, false, false);
            //SetTextCell(ws, "G12:G13", "MGB34923", 11, true, true, false);

            SetTextCell(ws, "J10", "Contact", 11, true, false, true);
            SetTextCell(ws, "J11:L11", " SIAMKARAT VIETNAM REP OFFICE", 11, true, false, false);
            SetTextCell(ws, "J12:L12", "Ms. Helen - Merchandiser", 11, false, false, false);

            // Write the headers to the header table
            SetHeaderTableCell(ws);

            int startRowTable = 14;
            int startRowTableData = 14;

            var listGroupCodes = plan.PlanDetails.GroupBy(pd => pd.POCode).Select(pd => pd.Key).ToList();
            foreach (var code in listGroupCodes)
            {
                startRowTableData++;
                var groupCode = ws.Cells[string.Format("A{0}:S{1}", startRowTableData, startRowTableData)];
                groupCode.Merge = true;
                groupCode.Value = code;
                groupCode.Style.Font.Size = 11;
                groupCode.Style.Font.Bold = true;
                groupCode.Style.Fill.PatternType = ExcelFillStyle.Solid;
                groupCode.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                var listPlanDetails = plan.PlanDetails.Where(p => p.POCode == code).ToList();
                foreach (var planDetail in listPlanDetails)
                {
                    startRowTableData++;
                    SetDataTableCell(ws, startRowTableData, planDetail);
                }
            }
            int endRowTable = startRowTableData;

            //var tableRange = ws.Cells[startRowTable, 1, endRowTable, 19];
            var tableRange = ws.Cells[10, 1, endRowTable, 19];
            var borders = tableRange.Style.Border;
            borders.Left.Style = borders.Right.Style = borders.Top.Style = borders.Bottom.Style = ExcelBorderStyle.Thin;

            endRowTable++;
            ws.Cells[endRowTable, 11].Value = plan.PlanDetails.Sum(pd => pd.Quantity);
            ws.Cells[endRowTable, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 11].Value = "PCS";
            ws.Cells[endRowTable + 1, 11].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 12].Value = 0;
            ws.Cells[endRowTable, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 12].Value = "PKGS";
            ws.Cells[endRowTable + 1, 12].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 13].Value = "$  " + plan.PlanDetails.Sum(pd => pd.Money);
            ws.Cells[endRowTable, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 13].Value = "USD";
            ws.Cells[endRowTable + 1, 13].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 14].Value = 0;
            ws.Cells[endRowTable, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 14].Value = "KGS";
            ws.Cells[endRowTable + 1, 14].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable + 1, 1].Value = "Country of Origin : Made in VN";
            ws.Cells[endRowTable + 1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            ws.Cells[endRowTable + 2, 1].Value = "I certify that all chemical substances in this shipment comply with all applicable rules or orders under TSCA and that I am not offering a chemical substance for entry in violation of TSCA or any applicable rule or order there under";
            ws.Cells[endRowTable + 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 2, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            return new ExcelFileData
            {
                Name = string.Format(@"plan-{0}.xlsx", plan.CreateDate?.ToString("dd-MM-yyyy-hh-mm-ss-fff")),
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                Bytes = excelPackage.GetAsByteArray()
            };
        }

        void SetTextCell(ExcelWorksheet worksheet, int row, int cell, string text, int fontSize, bool isBold = false)
        {
            var titleCell = worksheet.Cells[row, cell];
            titleCell.Value = text;
            titleCell.Style.Font.Size = fontSize;
            titleCell.Style.Font.Bold = isBold;
            titleCell.AutoFitColumns();
        }

        //void SetTextCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isUnderlined = false)
        //{
        //    var titleCell = worksheet.Cells[cellRange];
        //    titleCell.Merge = true;
        //    titleCell.Value = text;
        //    titleCell.Style.Font.Size = fontSize;
        //    titleCell.Style.Font.Bold = isBold;
        //    titleCell.Style.Font.UnderLine = isUnderlined;
        //    titleCell.AutoFitColumns();
        //}

        // Helper method to set merged cell with text and formatting
        void SetTextCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isCenter = false, bool isUnderlined = false)
        {
            var titleCell = worksheet.Cells[cellRange];
            titleCell.Merge = true;
            titleCell.Value = text;
            titleCell.Style.Font.Size = fontSize;
            titleCell.Style.Font.Bold = isBold;
            titleCell.Style.Font.UnderLine = isUnderlined;
            titleCell.Style.HorizontalAlignment = isCenter ? ExcelHorizontalAlignment.Center : ExcelHorizontalAlignment.Left;
            titleCell.Style.VerticalAlignment = isCenter ? ExcelVerticalAlignment.Center : ExcelVerticalAlignment.Bottom;
            titleCell.AutoFitColumns();
        }


        void SetHeaderTableCell(ExcelWorksheet worksheet)
        {
            var headers = new[]
            {
                "Seq", "Product ID ", "Item code", "Item #", "Product Description", "Finish", "Box Dimension",
                "Box Volume", "GW", "Unit Price", "Qty", "PKGS", "Total amount (FOB)",
                "Total Vol. (m3)", "Total GW (kgs)", "Total GW (lbs)", "Mirror Cm2", "Material", "HTS code"
            };

            var columnWidths = new[]
            {
                13,   // Seq
                33,  // Product ID
                20,  // Item code
                20,   // Item #
                60,  // Product Description
                17,  // Finish
                27,  // Box Dimension
                13,  // Box Volume
                13,   // GW
                13,  // Unit Price
                13,   // Qty
                15,   // PKGS
                17,  // Total amount (FOB)
                17,  // Total Vol. (m3)
                17,  // Total GW (kgs)
                17,  // Total GW (lbs)
                13,  // Mirror Cm2
                13,  // Material
                13   // HTS code
            };


            for (int i = 0; i < headers.Length; i++)
            {
                var cell = worksheet.Cells[14, i + 1];
                cell.Value = headers[i];
                cell.Style.Font.Bold = i == 0 ? false : true;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Center the text
                cell.AutoFitColumns();

                // Set column width (adjust the value as needed)
                worksheet.Column(i + 1).Width = columnWidths[i];
            }
        }

        void SetDataTableCell(ExcelWorksheet worksheet, int row, PlanResultModel.PlanDetailGetByIdResultModel planDetail)
        {
            worksheet.Cells[row, 2].Value = planDetail?.ProductId.ToString() ?? ""; //Product ID
            worksheet.Cells[row, 3].Value = planDetail?.ItemCode ?? ""; //Item code
            worksheet.Cells[row, 4].Value = planDetail.ItemId.ToString() ?? ""; //Item #
            worksheet.Cells[row, 5].Value = planDetail?.ProductName ?? ""; //Product Description
            worksheet.Cells[row, 6].Value = ""; //Finish
            worksheet.Cells[row, 7].Value = ""; //Box Dimension (inch)
            worksheet.Cells[row, 8].Value = ""; //Box Volume
            worksheet.Cells[row, 9].Value = ""; //GW (lbs)
            worksheet.Cells[row, 10].Value = planDetail?.UnitPrice.ToString() ?? ""; //Unit Price
            worksheet.Cells[row, 11].Value = planDetail?.Quantity.ToString() ?? ""; //Qty
            worksheet.Cells[row, 11].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[row, 12].Value = ""; //PKGS
            worksheet.Cells[row, 13].Value = "$  " + planDetail?.Money ?? ""; //Total amount (FOB)
            worksheet.Cells[row, 13].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[row, 14].Value = ""; //Total Vol. (m3)
            worksheet.Cells[row, 15].Value = ""; //Total GW (kgs)
            worksheet.Cells[row, 16].Value = ""; //Total GW (lbs)
            worksheet.Cells[row, 17].Value = ""; // Mirror Cm2
            worksheet.Cells[row, 18].Value = ""; //Material
            worksheet.Cells[row, 19].Value = ""; //HTS code


            var rowRange = worksheet.Cells[row, 1, row, worksheet.Dimension.End.Column];
            //rowRange.AutoFitColumns();
            worksheet.Row(row).Style.Font.Size = 11;
        }

        private Item GetItem(Guid itemId)
        {
            return _unitOfWork.ItemRepository.GetFirstOrDefault(i => i.Id == itemId);
        }

        private Customer GetCustomer(Guid customerId)
        {
            return _unitOfWork.CustomerRepository.GetFirstOrDefault(i => i.Id == customerId);
        }
    }

    public class ExcelFileData
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Bytes { get; set; }
    }
}
