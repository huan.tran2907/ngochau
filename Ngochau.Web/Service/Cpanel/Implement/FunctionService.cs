﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.Enums;
using Ngochau.Web.Areas.Admin.Service.IService;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class FunctionService : IFunctionService
    {
        private IUnitOfWork _unitOfWork;

        public FunctionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<string>> Delete(string id)
        {
            CreateUpdateDeleteResultModel<string> idResult = new CreateUpdateDeleteResultModel<string>();
            try
            {
                var function = _unitOfWork.FunctionRepository.GetFirstOrDefault(x => x.Id == id);
                if (function != null)
                {
                    _unitOfWork.FunctionRepository.Removed(function);
                    _unitOfWork.Save();
                    idResult.ResultType = 200;
                    idResult.Id = id;
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
            }
            
            return idResult;
        }

        public async Task<IEnumerable<Function>> GetAll(string filter)
        {
            var query = _unitOfWork.FunctionRepository.GetAll().ToList();
            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.Name.Contains(filter) || x.Id == filter).ToList();
            return query;
        }

        public async Task<Function> GetById(string filter)
        {
            var query = _unitOfWork.FunctionRepository.GetFirstOrDefault(x => x.Id == filter);
            return query;
        }

        public async Task<List<Function>> GetFunctionWithRoles(string userId)
        {
            try
            {
                string sql = $"select f.* from AppUser as u " +
               $"inner join AppUserRoles as ur on u.Id = ur.UserId " +
               $"inner join AppRole as r on ur.RoleId = r.Id " +
               $"inner join[Permissions] as p on p.RoleId = r.Id " +
               $"inner join Functions as f on f.Id = p.FunctionId " +
               $"where u.Id = '" + userId + "' and p.CanRead = 1 and f.status = 1; ";
                var result = _unitOfWork.FunctionRepository.FromSql(sql).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<CreateUpdateDeleteResultModel<string>> Insert(Function model)
        {
            CreateUpdateDeleteResultModel<string> idResult = new CreateUpdateDeleteResultModel<string>();
            try
            {
                model.Id = model.Code;
                var resultFunc = _unitOfWork.FunctionRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultFunc != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    _unitOfWork.FunctionRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<string>> Update(Function model)
        {
            CreateUpdateDeleteResultModel<string> idResult = new CreateUpdateDeleteResultModel<string>();
            try
            {
                if (model.Id == string.Empty)
                {
                    idResult.ResultType = 404;

                }
                var resultFunc = _unitOfWork.FunctionRepository.GetFirstOrDefault(x => x.Id == model.Id);
                if (resultFunc != null)
                {
                    resultFunc.Name = model.Name;
                    resultFunc.IconCss = model.IconCss;
                    resultFunc.SortOrder = model.SortOrder;
                    resultFunc.Status = model.Status;
                    resultFunc.URL = model.URL;
                    resultFunc.ParentId = model.ParentId;
                    _unitOfWork.FunctionRepository.Update(resultFunc);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                    return idResult;
                }
                else
                {
                    idResult.ResultType = 404;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }
    }
}
