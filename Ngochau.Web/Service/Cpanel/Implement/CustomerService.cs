﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class CustomerService : ICustomerService
    {
        IUnitOfWork _unitOfWork;
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var function = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Id == id);
                if (function != null)
                {
                    _unitOfWork.CustomerRepository.Removed(function);
                    _unitOfWork.Save();
                    idResult.ResultType = 200;
                    idResult.Id = id;
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = id;
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = id;
            }

            return idResult;
        }

        [HttpGet]
        public async Task<Customer> GetById(Guid id)
        {
            var model = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Id == id);

            return model;
        }

        public List<Customer> GetCustomer(string keyword = null)
        {
            return _unitOfWork.CustomerRepository.GetAll().Where(x => (keyword == null || x.Code == keyword || x.Name.Contains(keyword))).ToList();
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(Customer model)
        {

            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var resultMasterOrder = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultMasterOrder != null)
                {
                    idResult.ResultType = 416;
                }
                else
                {
                    model.Id = Guid.NewGuid();
                    _unitOfWork.CustomerRepository.Add(model);
                    _unitOfWork.Save();
                    idResult.Id = model.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(Customer model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            try
            {
                var customer = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Id == model.Id);
                customer.Name = model.Name;
                customer.Mail = model.Mail;
                customer.Phone = model.Phone;
                customer.Fax = model.Fax;
                customer.CustomerAddress = model.CustomerAddress;
                customer.Avatar = model.Avatar;
                customer.Deposit = model.Deposit;
                customer.IsRule = model.IsRule;
                _unitOfWork.CustomerRepository.Update(customer);
                _unitOfWork.Save();
                idResult.Id = customer.Id;
                idResult.ResultType = 200;
                return idResult;
            }
            catch (Exception ex)
            {
                idResult.Id = Guid.Empty;
                idResult.ResultType = 500;
                return idResult;
            }
        }

        public PagedResult<Customer> GetAllPagingAsync(string keyword, int page, int pageSize)
        {
            var query = _unitOfWork.CustomerRepository.GetAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword)
                || x.Code.Contains(keyword)
                || x.Phone.Contains(keyword));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);
            var paginationSet = new PagedResult<Customer>()
            {
                Results = query.ToList(),
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

    }
}
