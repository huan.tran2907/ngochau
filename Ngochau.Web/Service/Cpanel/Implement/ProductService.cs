﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class ProductService : IProductService
    {
        IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Delete(Guid id)
        {
            var pro = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == id);
            if (pro != null)
            {
                _unitOfWork.ProductRepository.Removed(pro);
                _unitOfWork.Save();
                return 200;
            }
            else
                return 404;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Insert(Product model)
        {
            CreateUpdateDeleteResultModel<Guid> result = new CreateUpdateDeleteResultModel<Guid>();
            if (model.Id == Guid.Empty || model.Id == null)
            {
                var entity = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (entity == null) // Thêm mới
                {
                    model.Id = Guid.NewGuid();
                    model.DateCreated = DateTime.Now;
                    //model.DateModified = DateTime.Now;
                    _unitOfWork.ProductRepository.Add(model);
                    _unitOfWork.Save();
                }
                else
                {
                    result.ResultType = 416;
                    return result; //SEO Alias already exists
                }
            }
            result.ResultType = 200;
            result.Id = model.Id;
            return result;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(Product model)
        {
            CreateUpdateDeleteResultModel<Guid> result = new CreateUpdateDeleteResultModel<Guid>();

            var itemCate = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == model.Id);
            if (itemCate != null)
            {
                //var alias = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id != model.Id && x.SKU == model.SKU);
                //if (alias == null)
                //{
                //    itemCate.Name = model.Name;
                //    //itemCate.Description = model.Description;
                //    //itemCate.Content = model.Content;
                //    itemCate.Image = model.Image;
                //    //itemCate.DateModified = model.DateModified;
                //    //itemCate.SortOrder = model.SortOrder;
                //    //itemCate.Status = model.Status;
                //    //itemCate.DateModified = DateTime.Now;
                //    _unitOfWork.ProductRepository.Update(itemCate);
                //    _unitOfWork.Save();
                //}
                //else
                //{
                //    result.ResultType = 416;
                //    return result;
                //}
            }
            else
            {
                result.ResultType = 404;
                return result;
            }

           
            result.ResultType = 200;
            result.Id = model.Id;
            return result;
        }
        public ProductResultModel GetById(Guid id)
        {
            ProductResultModel prod = new ProductResultModel();
            var product = _unitOfWork.ProductRepository.GetFirstOrDefault(x => x.Id == id);
            prod.Id = product.Id;
            prod.Name = product.Name;
            //prod.SKU = product.SKU;
            //prod.BarCode = product.BarCode;
            //prod.Image = product.Image;
            //prod.Description = product.Description;
            //prod.Content = product.Content;
            //prod.Status = product.Status;
            return prod;
        }

        public PagedResult<ProductResultModel> GetProducts(string keyword = null, int take = 0)
        {
            var query = _unitOfWork.ProductRepository.GetAll();

            query = query.Where(x => (string.IsNullOrEmpty(keyword) || x.Code.ToLower().Trim().Contains(keyword.ToLower().Trim())
            || x.Name.ToLower().Trim().Contains(keyword.ToLower().Trim())));

            var data = query.Select(x => new ProductResultModel()
            {
                BarCode = x.Code,
                Name = x.Name,
                Id = x.Id,
                CreateDate = x.DateCreated != null ? ((DateTime)x.DateCreated).ToString("dd/MM/yyyy") : "",
                CreateTime = x.DateCreated != null ? ((DateTime)x.DateCreated).ToString("HH:mm") : ""
            }).ToList();
            var paginationSet = new PagedResult<ProductResultModel>()
            {
                Results = take == 0 ? data : data.Take(take).ToList()
            };
            return paginationSet;
        }

        public PagedResult<ProductResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize)
        {
            var query = _unitOfWork.ProductRepository.GetAll();

            query = query.Where(x => (string.IsNullOrEmpty(keyword) 
            || x.Name.Contains(keyword) || x.Code.Contains(keyword)));

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(x => new ProductResultModel()
            {
                BarCode = x.Code,
                Name = x.Name,
                Id = x.Id,
                Image = x.Image,
                CreateDate = x.DateCreated != null ? ((DateTime)x.DateCreated).ToString("dd/MM/yyyy") : "",
                CreateTime = x.DateCreated != null ? ((DateTime)x.DateCreated).ToString("HH:mm") : ""
            }).ToList();
            var paginationSet = new PagedResult<ProductResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public List<Product> GetAlls(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
