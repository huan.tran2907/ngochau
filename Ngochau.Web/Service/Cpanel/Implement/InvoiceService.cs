﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.Enum;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Numerics;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class InvoiceService : IInvoiceService
    {
        private IUnitOfWork _unitOfWork;

        public InvoiceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public PagedResult<InvoiceResultModel.InvoiceGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize)
        {
            var query = _unitOfWork.InvoiceRepository.GetAll();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(q => q.Name.Contains(keyword) || q.Code.Contains(keyword));
            }

            if (status != null)
            {
                query = query.Where(q => q.Status == status.Value);
            }

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize)
               .Take(pageSize);

            var data = query.Select(q => new InvoiceResultModel.InvoiceGetAllPagingResultModel()
            {
                Id = q.Id,
                Code = q.Code,
                Name = q.Name,
                ContainerName = _unitOfWork.ContainerRepository.GetFirstOrDefault(c => c.Id == q.ContainerId)?.Name,
                StatusName = q.Status == (int)CommonStatus.New ? "Mới" : "Đóng",
                Status = q.Status,
                TotalDeposit = q.TotalDeposit,
                TotalMoney = q.TotalMoney,
                TotalQuantity = q.TotalQuantity,
                CreateDate = q.CreateDate != null ? ((DateTime)q.CreateDate).ToString("dd/MM/yyyy") : "",
            }).ToList();

            var paginationSet = new PagedResult<InvoiceResultModel.InvoiceGetAllPagingResultModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public async Task<InvoiceResultModel.InvoiceGetByIdResultModel> GetById(Guid Id)
        {
            InvoiceResultModel.InvoiceGetByIdResultModel invoiceResult = new InvoiceResultModel.InvoiceGetByIdResultModel();
            var invoice = _unitOfWork.InvoiceRepository.GetFirstOrDefault(x => x.Id == Id);
            invoiceResult.InvoiceId = invoice.Id;
            invoiceResult.Code = invoice.Code;
            invoiceResult.Name = invoice.Name;
            invoiceResult.CustomerId = invoice.CustomerId;
            invoiceResult.ContainerId = invoice.ContainerId;
            invoiceResult.TotalDeposit = invoice.TotalDeposit;
            invoiceResult.TotalMoney = invoice.TotalMoney;
            invoiceResult.TotalQuantity = invoice.TotalQuantity;
            invoiceResult.Status = invoice.Status;
            invoiceResult.CreateDate = invoice.CreateDate;

            var cipls = _unitOfWork.CIPLRepository.GetAll().Select(mo => new { mo.Id, mo.Code }).ToList();
            foreach (var item in invoice.InvoiceDetails)
            {
                var po = _unitOfWork.PORepository.GetFirstOrDefault(po => po.PODetails.Where(poD => poD.ProductId == item.ProductId).Any() == true);

                var product = _unitOfWork.ProductRepository.GetFirstOrDefault(mo => mo.Id == item.ProductId);
                var color = _unitOfWork.ColorRepository.GetFirstOrDefault(mo => mo.Id == item.ColorId);
                var itemData = _unitOfWork.ItemRepository.GetFirstOrDefault(mo => mo.Id == item.ItemId);

                InvoiceResultModel.InvoiceDetailGetByIdResultModel invoiceDetailResult = new InvoiceResultModel.InvoiceDetailGetByIdResultModel();
                invoiceDetailResult.InvoiceDetailId = item.Id;
                invoiceDetailResult.InvoiceId = invoice.Id;

                invoiceDetailResult.POCode = po.Code;

                invoiceDetailResult.CIPLId = item.CIPLId;
                invoiceDetailResult.CIPLCode = cipls.Where(mo => mo.Id == item.CIPLId).FirstOrDefault()?.Code;

                invoiceDetailResult.ProductId = item.ProductId;
                invoiceDetailResult.ProductCode = product.Code;
                invoiceDetailResult.ProductName = product.Name;

                invoiceDetailResult.ColorId = item.ColorId;
                invoiceDetailResult.ColorCode = color.Code;
                invoiceDetailResult.ColorName = color.Name;

                invoiceDetailResult.ItemId = item.ItemId;
                invoiceDetailResult.ItemCode = itemData.Code;
                invoiceDetailResult.ItemName = itemData.Name;

                invoiceDetailResult.Quantity = item.Quantity;
                invoiceDetailResult.Deposit = item.Deposit;
                invoiceDetailResult.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                invoiceDetailResult.Money = item.Money;

                invoiceResult.InvoiceDetails.Add(invoiceDetailResult);
            }
            return invoiceResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> InsertInvoice(InvoiceParamModel.InvoiceInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var resultInvoice = _unitOfWork.InvoiceRepository.GetFirstOrDefault(x => x.Code == model.Code);
                if (resultInvoice != null)
                {
                    idResult.ResultType = 416;
                    _unitOfWork.RollbackTransaction();
                }
                else
                {
                    var cipl = _unitOfWork.CIPLRepository.GetFirstOrDefault(cipl => cipl.Id == model.CIPLId);
                    if (cipl == null)
                    {
                        idResult.ResultType = 500;
                        return idResult;
                    }

                    //Lưu thông tin master order
                    Invoice invoice = new Invoice();
                    invoice.Id = Guid.NewGuid();
                    invoice.CustomerId = cipl.CustomerId;
                    invoice.ContainerId = cipl.ContainerId;
                    invoice.Code = model.Code;
                    invoice.Name = model.Name;
                    invoice.TotalDeposit = model.InvoiceDetails.Sum(x => x.Deposit);
                    invoice.TotalQuantity = model.InvoiceDetails.Sum(x => x.Quantity);
                    invoice.TotalMoney = model.InvoiceDetails.Sum(x => x.TotalMoney);
                    invoice.CreateDate = DateTime.Now;
                    invoice.Status = (int)CommonStatus.New;

                    _unitOfWork.InvoiceRepository.Add(invoice);

                    foreach (var item in model.InvoiceDetails)
                    {
                        InvoiceDetail invoiceDetail = new InvoiceDetail();
                        invoiceDetail.Id = Guid.NewGuid();
                        invoiceDetail.InvoiceId = invoice.Id;
                        invoiceDetail.CIPLId = model.CIPLId;

                        CIPLDetail ciplDetail = _unitOfWork.CIPLDetailRepository.GetFirstOrDefault(x => x.Id == item.CIPLDetailId);
                        if (ciplDetail != null)
                        {
                            invoiceDetail.CBM = ciplDetail.CBM;
                            invoiceDetail.DescCodeColor = ciplDetail.DescCodeColor;
                            invoiceDetail.DescColorFails = ciplDetail.DescColorFails;
                        }

                        invoiceDetail.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        invoiceDetail.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        invoiceDetail.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        invoiceDetail.Quantity = item.Quantity;
                        invoiceDetail.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        invoiceDetail.Deposit = item.Deposit;
                        invoiceDetail.Money = item.TotalMoney;
                        invoiceDetail.CreateDate = DateTime.Now;
                        _unitOfWork.InvoiceDetailRepository.Add(invoiceDetail);
                    }
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = invoice.Id;
                    idResult.ResultType = 200;
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }

        public async Task<CreateUpdateDeleteResultModel<Guid>> Update(InvoiceParamModel.InvoiceInsertParamModel model)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                if (model.InvoiceId == Guid.Empty || model.InvoiceId == null)
                {
                    idResult.ResultType = 404;

                }
                var invoice = _unitOfWork.InvoiceRepository.GetFirstOrDefault(x => x.Id == model.InvoiceId);
                if (invoice != null)
                {
                    invoice.Name = model.Name;
                    invoice.Code = model.Code;
                    invoice.CreateDate = model.CreateDate;
                    invoice.TotalQuantity = model.InvoiceDetails.Sum(x => x.Quantity);
                    invoice.TotalDeposit = model.InvoiceDetails.Sum(x => x.Deposit);
                    invoice.TotalMoney = model.InvoiceDetails.Sum(x => x.TotalMoney);

                    _unitOfWork.InvoiceRepository.Update(invoice);

                    //Remove all before insert new list payment detail
                    var listInvoiceDetail = _unitOfWork.InvoiceDetailRepository.GetAll().Where(pd => pd.InvoiceId == model.InvoiceId).ToList();
                    _unitOfWork.InvoiceDetailRepository.RemoveRange(listInvoiceDetail);

                    foreach (var item in model.InvoiceDetails)
                    {
                        InvoiceDetail invoiceDetailInsert = new InvoiceDetail();
                        invoiceDetailInsert.Id = Guid.NewGuid();
                        invoiceDetailInsert.CIPLId = item.CIPLId;
                        invoiceDetailInsert.InvoiceId = invoice.Id;

                        CIPLDetail ciplDetail = _unitOfWork.CIPLDetailRepository.GetFirstOrDefault(x => x.Id == item.CIPLDetailId);
                        if (ciplDetail != null)
                        {
                            invoiceDetailInsert.CBM = ciplDetail.CBM;
                            invoiceDetailInsert.DescCodeColor = ciplDetail.DescCodeColor;
                            invoiceDetailInsert.DescColorFails = ciplDetail.DescColorFails;
                        }

                        invoiceDetailInsert.ColorId = !string.IsNullOrEmpty(item.StrColor) ? Guid.Parse(item.StrColor) : null;
                        invoiceDetailInsert.ItemId = !string.IsNullOrEmpty(item.StrItem) ? Guid.Parse(item.StrItem) : null;
                        invoiceDetailInsert.ProductId = !string.IsNullOrEmpty(item.StrProduct) ? Guid.Parse(item.StrProduct) : null;
                        invoiceDetailInsert.Quantity = item.Quantity;
                        invoiceDetailInsert.UnitPrice = item.UnitPrice != 0 ? (decimal)item.UnitPrice : 0;
                        invoiceDetailInsert.Deposit = item.Deposit;
                        invoiceDetailInsert.Money = item.TotalMoney;
                        _unitOfWork.InvoiceDetailRepository.Add(invoiceDetailInsert);
                    }

                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.Id = invoice.Id;
                    idResult.ResultType = 200;
                    return idResult;
                }
                else
                {
                    idResult.ResultType = 404;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch
            {
                idResult.ResultType = 500;
                _unitOfWork.RollbackTransaction();
            }
            return idResult;
        }


        public async Task<CreateUpdateDeleteResultModel<Guid>> DeleteInvoice(Guid invoiceId)
        {
            CreateUpdateDeleteResultModel<Guid> idResult = new CreateUpdateDeleteResultModel<Guid>();
            _unitOfWork.BeginTransaction();
            try
            {
                var invoice = _unitOfWork.InvoiceRepository.GetFirstOrDefault(x => x.Id == invoiceId);
                if (invoice != null)
                {
                    _unitOfWork.InvoiceDetailRepository.RemoveRange(invoice.InvoiceDetails);
                    _unitOfWork.InvoiceRepository.Removed(invoice);
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    idResult.ResultType = 200;
                    idResult.Id = invoiceId;
                }
                else
                {
                    idResult.ResultType = 404;
                    idResult.Id = invoiceId;
                    _unitOfWork.RollbackTransaction();
                }
            }
            catch (Exception ex)
            {
                idResult.ResultType = 500;
                idResult.Id = invoiceId;
                _unitOfWork.RollbackTransaction();
            }

            return idResult;
        }

        public async Task<bool> ApplyPayment(Guid invoiceId)
        {
            bool result = false;
            _unitOfWork.BeginTransaction();
            try
            {
                var invoice = _unitOfWork.InvoiceRepository.GetFirstOrDefault(x => x.Id == invoiceId);
                var invoiceDetails = _unitOfWork.InvoiceDetailRepository.GetAll().Where(x => x.InvoiceId == invoiceId).ToList();
                if (invoiceDetails != null && invoiceDetails.Count > 0)
                {
                    foreach (var item in invoiceDetails)
                    {
                        item.QuantityExport = item.Quantity;
                        _unitOfWork.InvoiceDetailRepository.Update(item);

                        var poProduct = _unitOfWork.PODetailRepository.GetFirstOrDefault(po => po.ProductId == item.ProductId);
                        if (poProduct != null)
                        {
                            poProduct.QuantityExport = (poProduct.QuantityExport ?? 0) + item.Quantity;
                            _unitOfWork.PODetailRepository.Update(poProduct);
                        }
                    }

                    invoice.Status = (int)CommonStatus.Close;
                    _unitOfWork.InvoiceRepository.Update(invoice);

                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    result = true;
                }
            }
            catch
            {
                _unitOfWork.RollbackTransaction();
            }
            return result;
        }


        public async Task<ExcelFileData> ExportInvoice(Guid invoiceId)
        {
            var invoice = await GetById(invoiceId);

            ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("PAYMENT");

            // Set the font to Times New Roman for the entire worksheet
            ws.Cells.Style.Font.Name = "Times New Roman";

            // Define the title and address information
            var title = "NGO CHAU TRADING COMPANY LIMITED";
            var address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
            var contact = "Tel: (+84 8) 3730 9465 | Fax: (+84 8) 3730 8465";
            var title2 = "INVOICE";

            SetTextCell(ws, "A2:L2", title, 12, false, true);
            SetTextCell(ws, "A3:L3", address, 12, false, true);
            SetTextCell(ws, "A4:L4", contact, 12, false, true);
            SetTextCell(ws, "A6:L6", title2, 12, true, true);

            SetTextCell(ws, "B8", "PO#", 11, true, false, true);
            SetTextCell(ws, "M8", "PO ETD:", 11, true, false, true);

            SetTextCell(ws, "F7", "Kiểm cuối", 11, false, false, false);
            SetTextCell(ws, "F8", "Date: " + invoice.CreateDate?.ToString("dd/MM/yyyy"), 12, true);

            var customer = GetCustomer(invoice.CustomerId.Value);
            SetTextCell(ws, "A10", "Customer:", 11, false, false, true);
            SetTextCell(ws, "A11:B11", customer.Name, 11, false, false, false);
            SetTextCell(ws, "A12:B12", customer.CustomerAddress, 11, false, false, false);
            SetTextCell(ws, "A13:B13", "P: " + customer.Phone + " | F: " + customer.Fax, 11, false, false, false);

            SetTextCell(ws, "F10", "PO#", 11, true, false, false);
            SetTextCell(ws, "F11", "", 11, false, false, false);
            SetTextCell(ws, "F12", "PO Date", 11, true, false, true);
            SetTextCell(ws, "F13", "", 11, false, false, false);

            SetTextCell(ws, "G10:H10", "CONT# KOCU4088423", 11, true, false, false);
            SetTextCell(ws, "G11:H11", "SEAL# 23 0136605", 11, true, false, false);
            SetTextCell(ws, "G12:G13", "MGB34923", 11, true, true, false);

            SetTextCell(ws, "J10", "Contact", 11, true, false, true);
            SetTextCell(ws, "J11:L11", " SIAMKARAT VIETNAM REP OFFICE", 11, true, false, false);
            SetTextCell(ws, "J12:L12", "Ms. Helen - Merchandiser", 11, false, false, false);

            //// Write the headers to the header table
            SetHeaderTableCell(ws);

            int startRowTable = 14;
            int startRowTableData = 14;

            var listGroupCodes = invoice.InvoiceDetails.GroupBy(pd => pd.POCode).Select(pd => pd.Key).ToList();
            foreach (var code in listGroupCodes)
            {
                startRowTableData++;
                var groupCode = ws.Cells[string.Format("A{0}:R{1}", startRowTableData, startRowTableData)];
                groupCode.Merge = true;
                groupCode.Value = code;
                groupCode.Style.Font.Size = 12;
                groupCode.Style.Font.Bold = true;
                groupCode.Style.Fill.PatternType = ExcelFillStyle.Solid;
                groupCode.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                var listInvoiceDetails = invoice.InvoiceDetails.Where(p => p.POCode == code).ToList();
                foreach (var invoiceDetail in listInvoiceDetails)
                {
                    startRowTableData++;
                    SetDataTableCell(ws, startRowTableData, invoiceDetail);
                }
            }
            int endRowTable = startRowTableData;

            //var tableRange = ws.Cells[startRowTable, 1, endRowTable, 19];
            var tableRange = ws.Cells[10, 1, endRowTable, 14];
            var borders = tableRange.Style.Border;
            borders.Left.Style = borders.Right.Style = borders.Top.Style = borders.Bottom.Style = ExcelBorderStyle.Thin;
            endRowTable++;
            ws.Cells[endRowTable, 10].Value = invoice.InvoiceDetails.Sum(pd => pd.Quantity);
            ws.Cells[endRowTable, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 10].Value = "PCS";
            ws.Cells[endRowTable + 1, 10].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 11].Value = 0;
            ws.Cells[endRowTable, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 11].Value = "PKGS";
            ws.Cells[endRowTable + 1, 11].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 12].Value = "$  " + invoice.InvoiceDetails.Sum(pd => pd.Money);
            ws.Cells[endRowTable, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 12].Value = "USD";
            ws.Cells[endRowTable + 1, 12].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 13].Value = "$  ";
            ws.Cells[endRowTable, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable, 14].Value = "$  ";
            ws.Cells[endRowTable, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells[endRowTable + 1, 1].Value = "Country of Origin : Made in VN";
            ws.Cells[endRowTable + 1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 1, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 1, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            ws.Cells[endRowTable + 2, 1].Value = "I certify that all chemical substances in this shipment comply with all applicable rules or orders under TSCA and that I am not offering a chemical substance for entry in violation of TSCA or any applicable rule or order there under";
            ws.Cells[endRowTable + 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[endRowTable + 2, 1].Style.Font.Bold = true;
            ws.Cells[endRowTable + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(31, 78, 120));

            return new ExcelFileData
            {
                Name = string.Format(@"invoice-{0}.xlsx", invoice.CreateDate?.ToString("dd-MM-yyyy-hh-mm-ss-fff")),
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                Bytes = excelPackage.GetAsByteArray()
            };
        }

        //void SetTextCell(ExcelWorksheet worksheet, int row, int cell, string text, int fontSize, bool isBold = false)
        //{
        //    var titleCell = worksheet.Cells[row, cell];
        //    titleCell.Value = text;
        //    titleCell.Style.Font.Size = fontSize;
        //    titleCell.Style.Font.Bold = isBold;
        //    titleCell.AutoFitColumns();
        //}

        void SetTextCell(ExcelWorksheet worksheet, string cellRange, string text, int fontSize, bool isBold = false, bool isCenter = false, bool isUnderlined = false)
        {
            var titleCell = worksheet.Cells[cellRange];
            titleCell.Merge = true;
            titleCell.Value = text;
            titleCell.Style.Font.Size = fontSize;
            titleCell.Style.Font.Bold = isBold;
            titleCell.Style.Font.UnderLine = isUnderlined;
            titleCell.Style.HorizontalAlignment = isCenter ? ExcelHorizontalAlignment.Center : ExcelHorizontalAlignment.Left;
            titleCell.Style.VerticalAlignment = isCenter ? ExcelVerticalAlignment.Center : ExcelVerticalAlignment.Bottom;
            titleCell.AutoFitColumns();
        }

        void SetHeaderTableCell(ExcelWorksheet worksheet)
        {
            var headers = new[]
            {
                "Seq", "Product ID ", "Item code", "Product Description", "Finish",
                "Box Dimension", "Box Volume", "GW", "Unit Price", "Qty", "PKGS", "Total amount", "Deposit amnt", "Balance amnt"
            };

            var columnWidths = new[]
{
                13,  // Seq
                33,  // Product ID
                20,  // Item code
                60,  // Product Description
                17,  // Finish
                27,  // Box Dimension
                13,  // Box Volume
                13,  // GW
                13,  // Unit Price
                13,  // Qty
                15,  // PKGS
                17,  // Total amount
                17,  // Deposit amnt
                17,  // Balance amnt
            };

            for (int i = 0; i < headers.Length; i++)
            {
                var cell = worksheet.Cells[14, i + 1];
                cell.Value = headers[i];
                cell.Style.Font.Bold = i == 0 ? false : true;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; // Center the text
                cell.AutoFitColumns();

                // Set column width (adjust the value as needed)
                worksheet.Column(i + 1).Width = columnWidths[i];
            }
        }

        void SetDataTableCell(ExcelWorksheet worksheet, int row, InvoiceResultModel.InvoiceDetailGetByIdResultModel invoiceDetail)
        {
            var item = GetItem(invoiceDetail.ItemId);

            worksheet.Cells[row, 2].Value = invoiceDetail.ItemCode ?? ""; //Item code
            worksheet.Cells[row, 3].Value = invoiceDetail?.ProductId.ToString() ?? ""; //Product ID
            worksheet.Cells[row, 4].Value = invoiceDetail?.ProductName ?? ""; //Product Description
            worksheet.Cells[row, 5].Value = ""; //invoiceDetail?.Finish ?? ""; //Finish
            worksheet.Cells[row, 6].Value = item != null ? item.BoxDimension : ""; //Box Dimension (inch)
            worksheet.Cells[row, 7].Value = item != null ? item.BoxWeight : ""; //Box Volume
            worksheet.Cells[row, 8].Value = ""; //GW (lbs)
            worksheet.Cells[row, 9].Value = invoiceDetail?.UnitPrice.ToString() ?? ""; //Unit Price
            worksheet.Cells[row, 10].Value = invoiceDetail?.Quantity.ToString() ?? ""; //Qty
            worksheet.Cells[row, 10].Style.Font.Bold = true;
            worksheet.Cells[row, 11].Value = "$  " + invoiceDetail?.Money ?? ""; //Total amount
            worksheet.Cells[row, 11].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[row, 12].Value = ""; //PKGS
            worksheet.Cells[row, 13].Value = invoiceDetail?.Deposit; //Deposit amnt
            worksheet.Cells[row, 14].Value = ""; //Balance amnt

            var rowRange = worksheet.Cells[row, 1, row, worksheet.Dimension.End.Column];
            //rowRange.AutoFitColumns();
            worksheet.Row(row).Style.Font.Size = 11;
        }

        private Customer GetCustomer(Guid customerId)
        {
            return _unitOfWork.CustomerRepository.GetFirstOrDefault(i => i.Id == customerId);
        }

        private Item GetItem(Guid? itemId)
        {
            return _unitOfWork.ItemRepository.GetFirstOrDefault(i => i.Id == itemId);
        }
    }
}
