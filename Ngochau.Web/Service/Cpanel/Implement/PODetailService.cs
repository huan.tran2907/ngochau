﻿using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Ngochau.Models.Enums;
using Ngochau.Web.Areas.Admin.Service.IService;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Interface;
using Microsoft.AspNetCore.Identity;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Service.Cpanel.Implement
{
    public class PODetailService : IPODetailService
    {
        private IUnitOfWork _unitOfWork;

        public PODetailService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public PagedResult<MasterOrderResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<CreateUpdateDeleteResultModel<Guid>> Insert(PODetail model)
        {
            throw new NotImplementedException();
        }

        public Task<CreateUpdateDeleteResultModel<Guid>> Update(PODetail model)
        {
            throw new NotImplementedException();
        }
    }
}
