﻿using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IInvoiceService
    {
        PagedResult<InvoiceResultModel.InvoiceGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize);
        Task<InvoiceResultModel.InvoiceGetByIdResultModel> GetById(Guid Id);
        Task<CreateUpdateDeleteResultModel<Guid>> InsertInvoice(InvoiceParamModel.InvoiceInsertParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(InvoiceParamModel.InvoiceInsertParamModel model);
        Task<bool> ApplyPayment(Guid paymentId);
        Task<ExcelFileData> ExportInvoice(Guid invoiceId);
        Task<CreateUpdateDeleteResultModel<Guid>> DeleteInvoice(Guid invoiceId);
    }
}
