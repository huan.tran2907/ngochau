﻿using Ngochau.Models;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IUserService
    {
        Task<CreateUpdateDeleteResultModel<Guid>> AddAsync(UserParamModel userVm);
        Task<int> InsertOrUpdate(AppUser model);

        Task DeleteAsync(Guid id);

        Task<List<UserParamModel>> GetAllAsync();

        PagedResult<UserParamModel> GetAllPagingAsync(string keyword, int page, int pageSize);

        Task<UserParamModel> GetById(Guid id);


        Task<CreateUpdateDeleteResultModel<Guid>> UpdateAsync(UserParamModel userVm);
    }
}
