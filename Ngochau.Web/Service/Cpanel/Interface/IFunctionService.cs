﻿using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IFunctionService
    {
        Task<IEnumerable<Function>> GetAll(string filter);
        Task<List<Function>> GetFunctionWithRoles(string userId);
        Task<Function> GetById(string filter);
        Task<CreateUpdateDeleteResultModel<string>> Insert(Function model);
        Task<CreateUpdateDeleteResultModel<string>> Update(Function model);
        Task<CreateUpdateDeleteResultModel<string>> Delete(string id);
    }
}
