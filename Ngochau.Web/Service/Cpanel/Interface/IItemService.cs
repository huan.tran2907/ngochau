﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IItemService
    {
        PagedResult<Item> GetAllPagingAsync(string keyword, int page, int pageSize);
        Item GetById(Guid id);
        List<Item> GetItems(string keyword = null, int take = 0);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(Item model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Item model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);
    }
}
