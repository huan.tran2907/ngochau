﻿using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IRoleService
    {
        //Task<List<AppRole>> GetAll(string keyword);
        Task<bool> AddAsync(AppRole userVm);

        Task DeleteAsync(Guid id);

        Task<List<AppRole>> GetAllAsync();

        PagedResult<AppRole> GetAllPagingAsync(string keyword, int page, int pageSize);

        Task<AppRole> GetById(Guid id);


        Task UpdateAsync(AppRole userVm);

        List<Permission> GetListFunctionWithRole(Guid roleId);

        void SavePermission(List<Permission> permissions, Guid roleId);

        Task<bool> CheckPermission(string functionId, string action, string[] roles);
    }
}
