﻿using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IColorService
    {
        PagedResult<Color> GetAllPagingAsync(string keyword, int page, int pageSize);
        Color GetById(Guid id);
        List<Color> GetColors(string keyword = null, int take = 0);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(Color model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Color model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);
        Task InsertUpdateColor(Dictionary<string, string> models);
    }
}
