﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IPODetailService
    {
        PagedResult<MasterOrderResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(PODetail model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(PODetail model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);

    }
}
