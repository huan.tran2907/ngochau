﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IContainerService
    {
        Container GetById(Guid id);
        List<Container> GetContainers(string keyword = null);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(Container model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Container model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);
        PagedResult<ContainerResultModel> GetAllPagingAsync(string keyword, int page, int pageSize);
    }
}
