﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IPlanService
    {
        List<PlanResultModel.PlanGetAllResultModel> GetAll();
        PagedResult<PlanResultModel.PlanGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize);
        Task<PlanResultModel.GetPOForPlanResultModel> GetPOForPlan(Guid poId);
        Task<PlanResultModel.PlanGetByIdResultModel> GetById(Guid Id);
        Task<CreateUpdateDeleteResultModel<Guid>> InsertPlan(PlanParamModel.PlanInsertParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(PlanParamModel.PlanInsertParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> DeletePlan(Guid id);
        List<Plan> GetListPlanByIds(Guid[] ids);
        List<Ngochau.Models.NgoChauModel.Plan> GetListPlanByCustomerId(Guid customerId);
        Task<ExcelFileData> ExportPlan(Guid planId);
    }
}
