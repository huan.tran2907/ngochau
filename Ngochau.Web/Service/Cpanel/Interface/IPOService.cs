﻿using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ParamModels;
using Ngochau.Web.Model.ResultModels;
using static Ngochau.Web.Model.Cpanel.ParamModels.PurchaseOrderParamModel;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IPOService
    {
        List<Ngochau.Models.NgoChauModel.PO> GetListPOByIds(Guid[] ids);
        List<PI> GetListPIByIds(Guid[] ids);
        public List<Ngochau.Models.NgoChauModel.PO> GetPOs(string keyword = null);
        Task<MasterOrderItemResultModel> GetById(Guid Id);
        Task<PIResultModel> GetByPIId(Guid Id);
        List<MasterOrderItemResultModel> GetByIds(Guid[] Id);
        PagedResult<MasterOrderResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize);
        PagedResult<PIsResultModel> GetAllPisPagingAsync(string keyword, int status, int page, int pageSize);

        Task<CreateUpdateDeleteResultModel<Guid>> Insert(MasterOrderParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> InsertPI(PIParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> UpdatePI(PIParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(MasterOrderParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);

        //// Thêm vào từ file PDF & Excel
        Task<CreateUpdateDeleteResultModel<Guid>> InsertPDFExcel(PurchaseOrderParamModel.PO model);


        List<Ngochau.Models.NgoChauModel.PO> GetListPOByCustomerId(Guid customerId);
    }
}
