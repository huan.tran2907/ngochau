﻿using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface ICustomerService
    {
        PagedResult<Customer> GetAllPagingAsync(string keyword, int page, int pageSize);
        Task<Customer> GetById(Guid id);
        List<Customer> GetCustomer(string keyword = null);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(Customer model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Customer model);
        Task<CreateUpdateDeleteResultModel<Guid>> Delete(Guid id);
    }
}
