﻿using Ngochau.Web.Model.Cpanel.ParamModels;
using Ngochau.Web.Model.ResultModels;
using Ngochau.Web.Service.Cpanel.Implement;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface ICIPLService
    {
        Task<CIPLResultModel.GetPlanForCIPLResultModel> GetPlanForCIPL(Guid planId);
        List<CIPLResultModel.CIPLGetAllResultModel> GetAll();
        Task<CIPLResultModel.CIPLGetByIdResultModel> GetById(Guid Id);
        PagedResult<CIPLResultModel.CIPLGetAllPagingResultModel> GetAllPaging(string keyword, int? status, int page, int pageSize);
        Task<CreateUpdateDeleteResultModel<Guid>> InsertCIPL(CIPLParamModel.CIPLInsertParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(CIPLParamModel.CIPLInsertParamModel model);
        Task<CreateUpdateDeleteResultModel<Guid>> DeleteCIPL(Guid ciplId);
        Task<ExcelFileData> ExportCIPL(Guid ciplId);
    }
}
