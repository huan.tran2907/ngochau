﻿using Ngochau.Models;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IStatusService
    {
        Status GetById(Guid id);
        List<Status> GetStatus(string keyword = null);
        Task<CreateUpdateDeleteResultModel<int>> Insert(Status model);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Status model);
        Task<Guid> Delete(Guid id);
    }
}
