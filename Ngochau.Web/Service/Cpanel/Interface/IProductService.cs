﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using Ngochau.Web.Model.ResultModels;

namespace Ngochau.Web.Service.Cpanel.Interface
{
    public interface IProductService
    {
        PagedResult<ProductResultModel> GetProducts(string keyword = null, int take = 0);
        PagedResult<ProductResultModel> GetAllPagingAsync(string keyword, int status, int page, int pageSize);
        List<Product> GetAlls(Guid id);
        ProductResultModel GetById(Guid id);
        Task<CreateUpdateDeleteResultModel<Guid>> Update(Product model);
        Task<CreateUpdateDeleteResultModel<Guid>> Insert(Product model);
        Task<int> Delete(Guid id);
    }
}
