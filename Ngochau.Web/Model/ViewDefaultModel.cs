﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.Web.Model
{
    public class ViewDefaultModel
    {
        public int? Type { get; set; }
        public string? Alias { get; set; }
        public Guid Id { get; set; }
        public int? pageSize { get; set; }
        public string? SortBy { get; set; }
        public int? Page { get; set; }
        public string? Title { get; set; }
        public string? SeoKeyword { get; set; }
        public string? SeoDescription { get; set; }
        public string? Logo { get; set; }
        public string? LogoFooter { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? FooterContent { get; set; }
    }

    public class DetailDefaultModel
    {
        public Product Product { get; set; }
        public List<Product> Products { get; set; }
        public List<Product> News { get; set; }
        public string? SeoTitle { get; set; }
        public string? SeoMetaKeyword { get; set; }
        public string? SeoMetaDescription { get; set; }
    }
}
