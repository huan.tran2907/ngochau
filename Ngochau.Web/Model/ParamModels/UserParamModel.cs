﻿namespace Ngochau.Web.Model.ParamModels
{
    public class UserParamModel
    {
        public UserParamModel()
        {
            Roles = new List<string>();
        }
        public Guid? Id { set; get; }
        public string FullName { set; get; }
        public string BirthDay { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string UserName { set; get; }
        public string Address { get; set; }
        public string PhoneNumber { set; get; }
        public string Avatar { get; set; }
        public int? Status { get; set; }
        public string CreatedDate { get; set; }
        public List<string> Roles { get; set; }
    }
}
