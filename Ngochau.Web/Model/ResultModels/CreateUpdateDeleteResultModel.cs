﻿namespace Ngochau.Web.Model.ResultModels
{
    public class CreateUpdateDeleteResultModel<T>
    {
        public T Id { get; set; }
        public int ? ResultType { get; set; }
    }
}
