﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ngochau.Web.Model.ResultModels
{
    public class MasterOrderResultModel
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string? CustomerCode { get; set; }
        public string? CustomerName { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? POType { get; set; }
        public string? ShipVIA { get; set; }
        public string? Vendor { get; set; }
        public string? MGExFactory { get; set; }
        public string? VendorExFactory { get; set; }
        public string? ShippingTerms { get; set; }
        public string? VendorAHD { get; set; }
        public DateTime? VendorETAUSPort { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public int? Status { get; set; }
        public virtual Customer Customer { set; get; }
    }

    public class PIsResultModel
    {
        public Guid PIId { get; set; }
        public Guid? POId { get; set; }
        public string? POCode { get; set; }
        public string? POName { get; set; }
        public Guid?CustomerId { get; set; }
        public string? CustomerCode { get; set; }
        public string? CustomerName { get; set; }
        public string? PiDate { get; set; }
        public string? PiTime{ get; set; }
        public string? CreateDate{ get; set; }
        public string? CreateTime{ get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public decimal? TotalQuantity { get; set; }
    }
}
