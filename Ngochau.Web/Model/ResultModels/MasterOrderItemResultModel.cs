﻿using Ngochau.Models;
using Ngochau.Models.NgoChauModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ngochau.Web.Model.ResultModels
{
    public class MasterOrderItemResultModel
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? POType { get; set; }
        public string? ShipVIA { get; set; }
        public string? Vendor { get; set; }
        public string? MGExFactory { get; set; }
        public string? VendorExFactory { get; set; }
        public string? ShippingTerms { get; set; }
        public string? VendorAHD { get; set; }
        public DateTime? VendorETAUSPort { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public Customer Customer { set; get; }
        public List<MasterOrderDetailResultModel> PODetails { set; get; }
    }

    public class MasterOrderDetailResultModel
    {
        public Guid Id { get; set; }
        public Guid MasterOrderId { get; set; }
        public Guid ProductId { get; set; }
        public string? ProductSKU { get; set; }
        public string? ProductName { get; set; }
        public Guid? ContainerId { get; set; }
        public string? ContainerCode { get; set; }
        public string? ContainerName { get; set; }
        public Guid? ItemId { get; set; }
        public string? ItemCode { get; set; }
        public string? ItemName { get; set; }
        public Guid? ColorId { get; set; }
        public string? ColorCode { get; set; }
        public string? ColorName { get; set; }
        public decimal? UnitPrice { get; set; }
        public string? Finish { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? CBM { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? Quantity { get; set; }
        public string? Description { get; set; }
        public string? ExtraCharge { get; set; }
        public string? BatchVolume { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public string? ColorFail { get; set; } // Khi insert vào khác giá trị thì là fails
        public string? DescriptionColorFail { get; set; } // Khi insert vào khác giá trị thì là fails
    }


    public class PIResultModel
    {
        public Guid? PIId { get; set; }
        public Guid? POId { get; set; }
        public Guid? CustomerId { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public string? PIDate { get; set; }

        public List<PIDetailResultModel> PIDetails { get; set; }
    }

    public class PIDetailResultModel
    {
        public Guid? PIDetailId { get; set; }
        public Guid? ProductId { get; set; }
        public string? ProductSKU { get; set; }
        public string? ProductName { get; set; }
        public Guid? ItemId { get; set; }
        public string? ItemCode { get; set; }
        public string? ItemName { get; set; }
        public Guid? ColorId { get; set; }
        public string? ColorCode { get; set; }
        public string? ColorName { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? Quantity { get; set; }

    }
}
