﻿using static Ngochau.Web.Model.ResultModels.PlanResultModel;

namespace Ngochau.Web.Model.ResultModels
{
    public class CIPLResultModel
    {
        public class CIPLGetAllResultModel
        {
            public Guid Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
        }

        public class CIPLGetAllPagingResultModel
        {
            public Guid? Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? Container { get; set; }
            public string? CustomerName { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public string? CreateDate { get; set; }
        }

        public class CIPLGetByIdResultModel
        {
            public Guid? CIPLId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public Guid? CustomerId { get; set; }
            public Guid? ContainerId { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public DateTime? CreateDate { get; set; }
            public bool? IsHasInvoice { get; set; }
            public List<CIPLDetailGetByIdResultModel> CIPLDetails { get; set; }
            public List<PlanDetailPOResultModel> ListPlans { set; get; } = new List<PlanDetailPOResultModel>();
        }

        public class CIPLDetailGetByIdResultModel
        {
            public Guid? CIPLDetailId { get; set; }
            public Guid? CIPLId { get; set; }

            public string? POCode { get; set; }

            public Guid? PlanId { get; set; }
            public string? PlanCode { get; set; }

            public Guid? ProductId { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductName { get; set; }

            public Guid? ItemId { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemName { get; set; }

            public Guid? ColorId { get; set; }
            public string? ColorCode { get; set; }
            public string? ColorName { get; set; }

            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }

            public decimal UnitPrice { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? Money { get; set; }
            public int? Quantity { get; set; }
            public int? RemainQuantity { get; set; }
        }

        public class GetPlanForCIPLResultModel
        {
            public Guid? PlanId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? Desc { get; set; }
            public Guid? CustomerId { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public DateTime? CreateDate { get; set; }
            public List<GetPlanForCIPLDetailResultModel> PlanDetails { get; set; } = new List<GetPlanForCIPLDetailResultModel>();
        }

        public class GetPlanForCIPLDetailResultModel
        {
            public Guid? PlanDetailId { get; set; }
            public Guid? PlanId { get; set; }

            public string? POCode { get; set; }

            public Guid? ProductId { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductName { get; set; }

            public Guid? ItemId { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemName { get; set; }

            public Guid? ColorId { get; set; }
            public string? ColorCode { get; set; }
            public string? ColorName { get; set; }

            public int? Quantity { get; set; }
            public int? RemainQuantity { get; set; }

            public decimal? CBM { get; set; }

            public string? Desc { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? Money { get; set; }
            public decimal? UnitPrice { get; set; }
            public decimal? UnitPriceOld { get; set; }

            public DateTime? CreateDate { get; set; }

            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
        }
    }
}
