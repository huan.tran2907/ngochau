﻿namespace Ngochau.Web.Model.ResultModels
{
    public class SelectItem<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }
}
