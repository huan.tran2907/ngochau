﻿namespace Ngochau.Web.Model.ResultModels
{
    public class InvoiceResultModel
    {
        public class InvoiceGetAllPagingResultModel
        {
            public Guid? Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? ContainerName { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public int? Status { get; set; }
            public string? StatusName { get; set; }
            public string? CreateDate { get; set; }
        }

        public class InvoiceGetByIdResultModel
        {
            public Guid? InvoiceId { get; set; }
            public Guid? CustomerId { get; set; }
            public Guid? ContainerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? Desc { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public DateTime? CreateDate { get; set; }
            public int? Status { get; set; }
            public List<InvoiceDetailGetByIdResultModel> InvoiceDetails { get; set; } = new List<InvoiceResultModel.InvoiceDetailGetByIdResultModel>();
            public List<InvoiceDetailMasterOrderResultModel> ListPOs { set; get; } = new List<InvoiceDetailMasterOrderResultModel>();
        }

        public class InvoiceDetailMasterOrderResultModel
        {
            public Guid Id { get; set; }
            public Guid CustomerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
        }

        public class InvoiceDetailGetByIdResultModel
        {
            public Guid? InvoiceDetailId { get; set; }
            public Guid? InvoiceId { get; set; }

            public string? POCode { get; set; }

            public Guid? CIPLId { get; set; }
            public string? CIPLCode { get; set; }

            public Guid? ProductId { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductName { get; set; }

            public Guid? ItemId { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemName { get; set; }

            public Guid? ColorId { get; set; }
            public string? ColorCode { get; set; }
            public string? ColorName { get; set; }

            public int? Quantity { get; set; }
            public int? QuantityExport { get; set; }

            public DateTime? CreateDate { get; set; }

            public decimal UnitPrice { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? Money { get; set; }

            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
        }
    }
}
