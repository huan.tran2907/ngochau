﻿using Ngochau.Models;

namespace Ngochau.Web.Model.ResultModels
{
    public class PlanResultModel
    {
        public class PlanGetAllResultModel
        {
            public Guid Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
        }

        public class PlanGetAllPagingResultModel
        {
            public Guid? Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public string? PlanDate { get; set; }
            public string? CreateDate { get; set; }
            public int? Status { get; set; }
            public string? StatusName { get; set; }
        }

        public class PlanGetByIdResultModel
        {
            public Guid? PlanId { get; set; }
            public Guid? CustomerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public string? Desc { get; set; }
            public DateTime? CreateDate { get; set; }
            public List<PlanDetailGetByIdResultModel> PlanDetails { get; set; } = new List<PlanResultModel.PlanDetailGetByIdResultModel>();
            public List<PlanDetailPOResultModel> ListPOs { set; get; } = new List<PlanDetailPOResultModel>();
        }

        public class PlanDetailGetByIdResultModel
        {
            public Guid? PlanDetailId { get; set; }
            public Guid? PlanId { get; set; }

            public Guid? POId { get; set; }
            //public Guid? PODetailId { get; set; }
            public string? POCode { get; set; }

            public Guid? ProductId { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductName { get; set; }

            public Guid? ColorId { get; set; }
            public string? ColorCode { get; set; }
            public string? ColorName { get; set; }

            public Guid? ItemId { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemName { get; set; }

            public int? Quantity { get; set; }
            public int? RemainQuantity { get; set; }

            public decimal? CBM { get; set; }

            public decimal? Deposit { get; set; }
            public decimal UnitPrice { get; set; }
            public decimal? Money { get; set; }

            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
        }

        public class PlanDetailPOResultModel
        {
            public Guid Id { get; set; }
            public Guid CustomerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
        }

        public class GetPOForPlanResultModel
        {
            public Guid? POId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? Desc { get; set; }
            public Guid? CustomerId { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public DateTime? CreateDate { get; set; }
            public int? Status { get; set; }
            public List<GetPOForPlanDetailResultModel> PODetails { get; set; } = new List<GetPOForPlanDetailResultModel>();
        }

        public class GetPOForPlanDetailResultModel
        {
            public Guid? PODetailId { get; set; }
            public Guid? POId { get; set; }

            public Guid? ProductId { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductName { get; set; }

            public Guid? ItemId { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemName { get; set; }

            public Guid? ColorId { get; set; }
            public string? ColorCode { get; set; }
            public string? ColorName { get; set; }

            public int? Quantity { get; set; }
            public int? RemainQuantity { get; set; }

            public decimal? CBM { get; set; }

            public string? Desc { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? Money { get; set; }
            public decimal? UnitPrice { get; set; }
            public decimal? UnitPriceOld { get; set; }

            public DateTime? CreateDate { get; set; }

            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
        }
    }
}
