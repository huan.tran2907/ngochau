﻿namespace Ngochau.Web.Model.ResultModels
{
    public class ContainerResultModel
    {
        public Guid Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public int? IsActive { get; set; }
    }
}
