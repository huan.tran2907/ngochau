﻿using System.ComponentModel.DataAnnotations;

namespace Ngochau.Web.Model.ResultModels
{
    public class ProductResultModel
    {
        public Guid Id { get; set; }

        
        public string? Name { get; set; }
        public string? SKU { get; set; }
        public string? BarCode { get; set; }
        public string? Image { get; set; }
        public string? Description { get; set; }

        public string? Content { get; set; }

        public string? CreateDate { set; get; }
        public string? CreateTime { set; get; }
        public int? Status { set; get; }

        public int? SortOrder { set; get; }
    }
}
