﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ngochau.Web.Model.Cpanel.ParamModels
{
    public class MasterOrderParamModel
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public int? Status { get; set; }
        public List<MasterOrderDetailParamModel> MasterOrderDetailParamModel { get; set; }
    }

    public class MasterOrderDetailParamModel
    {
        public Guid MasterOrderDetailId { get; set; } = Guid.Empty;
        public string? StrProduct { get; set; }
        public string? StrItem { get; set; }
        public string? StrColor { get; set; }
        public decimal? UnitPrice { get; set; }
        public string? CBM { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? Quantity { get; set; }
    }

    public class POExport
    {
        public List<POExportId> POIds { get; set; }

    }

    public class POExportId
    {
        public Guid Id { get; set; } = Guid.Empty;
    }

    public class PIParamModel
    {
        public Guid PIId { get; set; }
        public Guid POId { get; set; }
        public Guid CustomerId { get; set; }
        public string? POCode { get; set; }
        public string? Name { get; set; }
        public decimal? TotalDeposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? TotalQuantity { get; set; }
        public int? Status { get; set; }
        public List<PIDetailParamModel> PIDetailParamModel { get; set; }
    }

    public class PIDetailParamModel
    {
        public Guid MasterOrderDetailId { get; set; } = Guid.Empty;
        public string? StrProduct { get; set; }
        public string? StrItem { get; set; }
        public string? StrColor { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? TotalMoney { get; set; }
        public int? Quantity { get; set; }
    }
}
