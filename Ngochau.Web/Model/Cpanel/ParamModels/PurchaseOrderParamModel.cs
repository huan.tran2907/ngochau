﻿namespace Ngochau.Web.Model.Cpanel.ParamModels
{
    public class PurchaseOrderParamModel
    {
        public class ProductItem
        {
            public string ProductBarCode { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public string MPN { get; set; }
            public string? CBM { get; set; }
            public int UnitsOrdered { get; set; } = 0;
            public decimal UnitCost { get; set; } = 0;
            public decimal TotalCBM { get; set; } = 0;
            public int PacksOrdered { get; set; } = 0;
            public decimal PackCost { get; set; } = 0;
            public decimal UnitCBM { get; set; } = 0;
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public string Finish { get; set; }
            public string Description { get; set; }
            public string Dimension { get; set; }
            public decimal TotalAmonut { get; set; } = 0;
            public string SupplierCode { get; set; }
            public string SupplierName { get; set; }
            public string SupplierFinish { get; set; }
            public string SupplierMaterial { get; set; }
            public string   BoxWeight { get; set; }
        }

        public class PO
        {
            
            public Guid CustomerId { get; set; }
            public string PurchaseOrder { get; set; }
            public string Vendor { get; set; }
            public string ShipTo { get; set; }
            public string OrderDate { get; set; }
            public string ShipVIA { get; set; }
            public string POType { get; set; }
            public string MGExFactory { get; set; }
            public string MGETAUSPort { get; set; }
            public string MGAHD { get; set; }
            public string ShippingTerms { get; set; }
            public string PaymentTerms { get; set; }
            public List<ProductItem> Products { get; set; } = new List<ProductItem>();
        }
    }
}
