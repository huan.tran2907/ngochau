﻿namespace Ngochau.Web.Model.Cpanel.ParamModels
{
    public class PlanParamModel
    {
        public class PlanInsertParamModel
        {
            public Guid? PlanId { get; set; }
            public Guid CustomerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public DateTime? PlanDate { get; set; }
            public DateTime? DueDate { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public int? Status { get; set; }
            public List<PlanDetailInsertParamModel> PlanDetails { get; set; }
        }

        public class PlanDetailInsertParamModel
        {
            public Guid POId { get; set; } = Guid.Empty;
            public Guid PODetailId { get; set; } = Guid.Empty;
            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
            public string? StrProduct { get; set; }
            public string? StrItem { get; set; }
            public string? StrColor { get; set; }
            public decimal? UnitPrice { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? Quantity { get; set; }
        }
    }
}
