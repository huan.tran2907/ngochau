﻿namespace Ngochau.Web.Model.Cpanel.ParamModels
{
    public class CIPLParamModel
    {
        public class CIPLInsertParamModel
        {
            public Guid? CIPLId { get; set; }
            public Guid CustomerId { get; set; }
            public Guid ContainerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public List<CIPLDetailInsertParamModel> CIPLDetails { get; set; }
        }

        public class CIPLDetailInsertParamModel
        {
            public Guid PlanId { get; set; } = Guid.Empty;
            public Guid PlanDetailId { get; set; } = Guid.Empty;
            public string? DescCodeColor { get; set; }
            public string? DescColorFails { get; set; }
            public string? StrProduct { get; set; }
            public string? StrItem { get; set; }
            public string? StrColor { get; set; }
            public decimal? UnitPrice { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? Quantity { get; set; }
        }
    }
}
