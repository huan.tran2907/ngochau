﻿namespace Ngochau.Web.Model.Cpanel.ParamModels
{
    public class InvoiceParamModel
    {
        public class InvoiceInsertParamModel
        {
            public Guid? InvoiceId { get; set; }
            public Guid? CIPLId { get; set; }
            public Guid CustomerId { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public decimal? TotalDeposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? TotalQuantity { get; set; }
            public int? Status { get; set; }
            public DateTime? CreateDate { get; set; }
            public List<InvoiceDetailInsertParamModel> InvoiceDetails { get; set; }
        }

        public class InvoiceDetailInsertParamModel
        {
            public Guid CIPLId { get; set; } = Guid.Empty;
            public Guid CIPLDetailId { get; set; } = Guid.Empty;
            public string? StrProduct { get; set; }
            public string? StrItem { get; set; }
            public string? StrItemName { get; set; }
            public string? StrColor { get; set; }
            public string? StrColorName { get; set; }
            public decimal? UnitPrice { get; set; }
            public decimal? Deposit { get; set; }
            public decimal? TotalMoney { get; set; }
            public int? Quantity { get; set; }
        }
    }
}
