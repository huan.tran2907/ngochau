﻿using Ngochau.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Identity Config

            builder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims").HasKey(x => x.Id);

            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims")
                .HasKey(x => x.Id);

            builder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);

            builder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles")
                .HasKey(x => new { x.RoleId, x.UserId });

            builder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens")
               .HasKey(x => new { x.UserId });

            #endregion Identity Config
            //base.OnModelCreating(builder);
        }
        public DbSet<Ngochau.Models.NgoChauModel.Product> Products { get; set; }
        public DbSet<Function> Functions { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<AppRole> AppRoles { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<PO> PO { get; set; }
        public DbSet<PODetail> PODetail { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Color> Color { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Container> Containers { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Item> Item { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Customer> Customer { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.PI> PI { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.PIDetail> PIDetail { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Plan> Plan { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.PlanDetail> PlanDetail { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.Invoice> Invoice { get; set; }
        public DbSet<Ngochau.Models.NgoChauModel.InvoiceDetail> InvoiceDetail { get; set; }
      

    }
}

/// Add table --- add-migration tablename
/// Update --- update-database
