﻿namespace Ngochau.DataAccess.Repository.IRepository
{
    public  interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }
        IProductRepository ProductRepository { get; }
        IFunctionRepository FunctionRepository { get; }
        ICompanyRepository CompanyRepository { get; }
        IRoleRepository RoleRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IPORepository PORepository { get; }
        IPODetailRepository PODetailRepository { get; }
        IItemRepository ItemRepository { get; }
        IColorRepository ColorRepository { get; }
        IContainerRepository ContainerRepository { get; }
        IStatusRepository StatusRepository { get; }
        IPIRepository PIRepository { get; }
        IPIDetailRepository PIDetailRepository { get; }
        IPlanRepository PlanRepository { get; }
        IPlanDetailRepository PlanDetailRepository { get; }
        IPaymentRepository PaymentRepository { get; }
        IPaymentDetailRepository PaymentDetailRepository { get; }
        ICIPLRepository CIPLRepository { get; }
        ICIPLDetailRepository CIPLDetailRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        IInvoiceDetailRepository InvoiceDetailRepository { get; }
        void Save();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}
