﻿using Ngochau.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository.IRepository
{
    public interface IPaymentDetailRepository : IRepository<PaymentDetail>
    {
    }
}
