﻿using Ngochau.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository.IRepository
{
    public interface IUserRoleRepository : IRepository<IdentityUserRole<Guid>>
    {
    }
}
