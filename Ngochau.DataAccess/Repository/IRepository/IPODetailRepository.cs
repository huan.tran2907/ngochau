﻿using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository.IRepository
{
    public interface IPODetailRepository : IRepository<PODetail>
    {
    }
}
