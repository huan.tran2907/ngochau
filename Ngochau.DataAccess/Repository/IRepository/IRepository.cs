﻿using System.Linq.Expressions;

namespace Ngochau.DataAccess.Repository.IRepository
{
    public interface IRepository <T> where T : class
    {
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        T GetFirstOrDefault(Expression<Func<T, bool>> filter);
        IQueryable<T> FindAll(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        void Removed(T entity);
        void RemoveRange(IEnumerable<T> entity);
        IQueryable<T> FromSql(string sql, params object[] parameters);
    }
}
