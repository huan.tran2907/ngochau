﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;

using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class ContainerRepository : Repository<Container>, IContainerRepository
    {

        private readonly ApplicationDbContext _db;
        public ContainerRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
