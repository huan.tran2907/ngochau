﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore.Storage;

namespace Ngochau.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _db;
        private IDbContextTransaction _transaction;
        public IUserRepository UserRepository { get; private set; }
        public IFunctionRepository FunctionRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }
        public ICompanyRepository CompanyRepository { get; private set; }
        public IRoleRepository RoleRepository { get; private set; }
        public IUserRoleRepository UserRoleRepository { get; private set; }
        public ICustomerRepository CustomerRepository { get; private set; }
        public IPORepository PORepository { get; private set; }
        public IItemRepository ItemRepository { get; private set; }
        public IPODetailRepository PODetailRepository { get; private set; }
        public IColorRepository ColorRepository { get; private set; }
        public IContainerRepository ContainerRepository { get; private set; }
        public IStatusRepository StatusRepository { get; private set; }
        public IPIRepository PIRepository { get; private set; }
        public IPIDetailRepository PIDetailRepository { get; private set; }
        public IPlanRepository PlanRepository { get; private set; }
        public IPlanDetailRepository PlanDetailRepository { get; private set; }
        public IPaymentRepository PaymentRepository { get; private set; }
        public IPaymentDetailRepository PaymentDetailRepository { get; private set; }
        public ICIPLRepository CIPLRepository { get; private set; }
        public ICIPLDetailRepository CIPLDetailRepository { get; private set; }
        public IInvoiceRepository InvoiceRepository { get; private set; }
        public IInvoiceDetailRepository InvoiceDetailRepository { get; private set; }


        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            FunctionRepository = new FunctionRepository(_db);
            ProductRepository = new ProductRepository(_db);
            UserRepository = new UserRepository(_db);
            CompanyRepository = new CompanyRepository(_db);
            RoleRepository = new RoleRepository(_db);   
            UserRoleRepository = new UserRoleRepository(_db);   
            CustomerRepository = new CustomerRepository(_db);
            PORepository = new MasterOrderRepository(_db);
            ItemRepository = new ItemRepository(_db);
            PODetailRepository = new PODetailRepository(_db);
            ColorRepository = new ColorRepository(_db);
            ContainerRepository = new ContainerRepository(_db);
            StatusRepository = new StatusRepository(_db);
            PIRepository = new PIRepository(_db);
            PIDetailRepository = new PIDetailRepository(_db);   
            PlanRepository = new PlanRepository(_db);   
            PlanDetailRepository = new PlanDetailRepository(_db);
            PaymentRepository = new PaymentRepository(_db);
            PaymentDetailRepository = new PaymentDetailRepository(_db);
            CIPLDetailRepository = new CIPLDetailRepository(_db);
            CIPLRepository = new CIPLRepository(_db);
            InvoiceRepository = new InvoiceRepository(_db);
            InvoiceDetailRepository = new InvoiceDetailRepository(_db);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void BeginTransaction()
        {
            _transaction = _db.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }
    }
}
