﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class CIPLDetailRepository : Repository<CIPLDetail>, ICIPLDetailRepository
    {
        private readonly ApplicationDbContext _db;
        public CIPLDetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
