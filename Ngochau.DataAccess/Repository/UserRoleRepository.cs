﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository
{
    public class UserRoleRepository : Repository<IdentityUserRole<Guid>>, IUserRoleRepository
    {

        private readonly ApplicationDbContext _db;
        public UserRoleRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}

