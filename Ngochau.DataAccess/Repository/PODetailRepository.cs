﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class PODetailRepository : Repository<PODetail>, IPODetailRepository
    {
        private readonly ApplicationDbContext _db;
        public PODetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
