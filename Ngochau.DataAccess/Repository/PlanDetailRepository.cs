﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class PlanDetailRepository : Repository<PlanDetail>, IPlanDetailRepository
    {
        private readonly ApplicationDbContext _db;
        public PlanDetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
