﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository
{
    public class PIRepository : Repository<PI>, IPIRepository
    {
        private readonly ApplicationDbContext _db;
        public PIRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
