﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class PIDetailRepository : Repository<PIDetail>, IPIDetailRepository
    {
        private readonly ApplicationDbContext _db;
        public PIDetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
