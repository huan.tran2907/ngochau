﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository
{
    public class PermissionRepository : Repository<Permission>, IPermissionRepository
    {

        private readonly ApplicationDbContext _db;
        public PermissionRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}