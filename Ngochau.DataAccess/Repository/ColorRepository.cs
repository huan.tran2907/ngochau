﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class ColorRepository : Repository<Color>, IColorRepository
    {

        private readonly ApplicationDbContext _db;
        public ColorRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
