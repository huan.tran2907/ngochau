﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class CIPLRepository : Repository<CIPL>, ICIPLRepository
    {
        private readonly ApplicationDbContext _db;
        public CIPLRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
