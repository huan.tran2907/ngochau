﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.Repository
{
    public class MasterOrderRepository : Repository<PO>, IPORepository
    {
        private readonly ApplicationDbContext _db;
        public MasterOrderRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
