﻿using Ngochau.DataAccess.Data;
using Ngochau.DataAccess.Repository.IRepository;
using Ngochau.Models.NgoChauModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ngochau.DataAccess.Repository
{
    public class InvoiceDetailRepository : Repository<InvoiceDetail>, IInvoiceDetailRepository
    {
        private readonly ApplicationDbContext _db;
        public InvoiceDetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
