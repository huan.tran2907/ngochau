﻿using Ngochau.DataAccess.Data;
using Ngochau.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Ngochau.Models.NgoChauModel;

namespace Ngochau.DataAccess.DbInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationDbContext _db;
        private UserManager<AppUser> _userManager; private RoleManager<AppRole> _roleManager;
        public DbInitializer(ApplicationDbContext db, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            AppUser user = _db.AppUsers.FirstOrDefault(x => x.UserName == "admin@gmail.com");
            if (user == null)
            {
                AppUser mode = new AppUser();
                mode.Id = Guid.NewGuid();
                mode.Discriminator = "AppUser";
                mode.FullName = "Administrator";
                mode.BirthDay = DateTime.Now;
                mode.Balance = 0;
                mode.Avatar = "";
                mode.SecurityStamp = Guid.NewGuid().ToString();
                mode.CreateDate = DateTime.Now;
                mode.ModifyDate = DateTime.Now;
                mode.Status = 1;
                mode.UserName = "admin@gmail.com";
                mode.NormalizedUserName = "admin@gmail.com";
                mode.Email = "admin@gmail.com";
                mode.NormalizedEmail = "admin@gmail.com";
                mode.EmailConfirmed = true;
                mode.ConcurrencyStamp = "";
                mode.PhoneNumber = "";
                mode.PhoneNumberConfirmed = false;
                mode.TwoFactorEnabled = false;
                mode.LockoutEnd = null;
                mode.LockoutEnabled = false;
                mode.AccessFailedCount = 0;
                var hasher = new PasswordHasher<AppUser>();
                mode.PasswordHash = hasher.HashPassword(mode, "Adminhuan1@");
                _db.AppUsers.Add(mode);
                _db.SaveChanges();

                AppRole findRole = _db.AppRoles.FirstOrDefault(x => x.Name == "Admin");
                if (findRole == null)
                {
                    var role = new AppRole();
                    role.Id = Guid.NewGuid();
                    role.Name = "Admin";
                    role.NormalizedName = "Admin";
                    role.Description = "Administrator";
                    _db.AppRoles.Add(role);
                    _db.SaveChanges();
                }
            }

            if (_db.Functions.Count() == 0)
            {
                try
                {
                    _db.Functions.AddRange(new List<Function>()
                    {
                        // Quản lý đơn hàng
                        new Function() {Id = "ORDER",Code = "ORDER", Name = "Quản lý đơn hàng",ParentId = null,SortOrder = 1,Status = 1,URL = "/",IconCss = "uil-invoice"  },
                        new Function() {Id = "MASTER",Code = "MASTER", Name = "Quản lý đơn hàng Master",ParentId = "ORDER",SortOrder = 5,Status = 1,URL = "/cpanel/masterorder/index",IconCss = "fa-desktop"  },

                        // Báo cáo
                        new Function() {Id = "REPORT",Code = "REPORT", Name = "Quản lý báo cáo",ParentId = null,SortOrder = 1,Status = 1,URL = "/cpanel/report/index",IconCss = "uil-invoice"  },
                        //new Function() {Id = "MASTER",Code = "MASTER", Name = "Quản lý đơn hàng Master",ParentId = "ORDER",SortOrder = 5,Status = 1,URL = "/cpanel/masterorder/index",IconCss = "fa-desktop"  },
                       
                        // Quản lý kế hoạch
                         new Function() {Id = "PLAN",Code = "PLAN", Name = "Quản lý kế hoạch",ParentId = null,SortOrder = 1,Status = 1,URL = "/",IconCss = "uil-invoice"  },
                        new Function() {Id = "PLANMASTER",Code = "PLANMASTER", Name = "Kế hoạch",ParentId = "PLAN",SortOrder = 5,Status = 1,URL = "/cpanel/masterorder/index",IconCss = "fa-desktop"  },


                        // Quản lý sản phẩm
                        new Function() {Id = "PROMASTER",Code = "PROMASTER", Name = "Quản lý sản phẩm",ParentId = null, SortOrder = 2,Status = 1,URL = "/",IconCss = "uil-home-alt"  },
                        new Function() {Id = "PRODUCTPRODUCT",Code = "PRODUCT", Name = "Quản lý sản phẩm",ParentId = "PROMASTER",SortOrder = 1,Status = 1,URL = "/cpanel/product/index",IconCss = "fa-desktop"  },
                        new Function() {Id = "PRODUCTCOLOR",Code = "COLOR", Name = "Quản lý màu",ParentId = "PROMASTER",SortOrder = 1,Status = 1,URL = "/cpanel/color/index",IconCss = "fa-desktop"  },
                        new Function() {Id = "PRODUCTITEM",Code = "ITEM", Name = "Quản lý item (Giá)",ParentId = "PROMASTER",SortOrder = 1,Status = 1,URL = "/cpanel/item/index",IconCss = "fa-desktop"  },

                        //Quản lý kho

                        new Function() {Id = "INVOICE",Code = "INVOICE", Name = "Quản lý Invoice",ParentId = null,SortOrder = 3,Status = 1,URL = "/cpanel/invoice/index",IconCss = "uil-shop"  },

                        //Quản lý hệ thống
                        new Function() {Id = "SYSTEM",Code = "SYSTEM", Name = "Quản lý hệ thống",ParentId = null,SortOrder = 4,Status = 1,URL = "/",IconCss = "uil-cog"  },
                        new Function() {Id = "SYSTEM_USER",Code = "SYSTEM_USER", Name = "Quản lý người dùng",ParentId = "SYSTEM",SortOrder = 1,Status = 1,URL = "/cpanel/user/users",IconCss = "fa-desktop"  },
                        new Function() {Id = "SYSTEM_ROLE",Code = "SYSTEM_ROLE", Name = "Quản lý vai trò",ParentId = "SYSTEM",SortOrder = 3,Status = 1,URL = "/Cpanel/role/roles",IconCss = "fa-home"  },
                        new Function() {Id = "SYSTEM_FUNCTION",Code = "FUNCTION", Name = "Quản lý chức năng (Menu)",ParentId = "SYSTEM",SortOrder = 3,Status = 1,URL = "/Cpanel/function/functions",IconCss = "fa-home"  },
                    });
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {

                    // throw;
                }


            }
            if(_db.Companies.Count() == 0)
            {
                Company company = new Company();
                company.Address = "74/5 Le Van Duyet St., Ward 1, Binh Thanh District, HCMC, Viet Nam";
                company.Name = "NGO CHAU TRADING COMPANY LIMITED";
                company.Website = "qlsx.ngochau.com";
                company.Email = "ngochau@gmail.com";
                company.Phone1 = "(+84 8) 3733 0419";
                company.Phone2 = "(+84 8) 3733 0410";
                company.TimeWork = "8h00 – 22h00 từ thứ 2 đến Chủ nhật";
                _db.Companies.Add(company);
                _db.SaveChanges();
            }
            

            if(_db.Containers.Count() == 0)
            {
                Ngochau.Models.NgoChauModel.Container con = new Ngochau.Models.NgoChauModel.Container();
                con.Id = Guid.NewGuid();
                con.Code = "Container01";
                con.Name = "Container 01";
                _db.Containers.Add(con);
                _db.SaveChanges();
            }

        }

        public void Initialize()
        {
            try
            {
                if (_db.Database.GetPendingMigrations().Count() > 0)
                {
                    _db.Database.Migrate();
                }
            }
            catch (Exception ex)
            {
            }

            Seed();
        }
    }
}
