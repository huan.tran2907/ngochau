﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ngochau.DataAccess.Migrations
{
    public partial class initdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoxDimension",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "BoxVol",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "BoxWeight",
                table: "Products");

            migrationBuilder.AddColumn<string>(
                name: "BoxDimension",
                table: "Item",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BoxWeight",
                table: "Item",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoxDimension",
                table: "Item");

            migrationBuilder.DropColumn(
                name: "BoxWeight",
                table: "Item");

            migrationBuilder.AddColumn<string>(
                name: "BoxDimension",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BoxVol",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BoxWeight",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
