﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ngochau.DataAccess.Migrations
{
    public partial class dataInt3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "POId",
                table: "PIDetail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "POId",
                table: "PIDetail",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
