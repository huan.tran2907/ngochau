﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ngochau.DataAccess.Migrations
{
    public partial class dataInt5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PIDetail_Products_ProductId",
                table: "PIDetail");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "PIDetail",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<DateTime>(
                name: "PIDate",
                table: "PI",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PIDetail_Products_ProductId",
                table: "PIDetail",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PIDetail_Products_ProductId",
                table: "PIDetail");

            migrationBuilder.DropColumn(
                name: "PIDate",
                table: "PI");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "PIDetail",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PIDetail_Products_ProductId",
                table: "PIDetail",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
